from .parser import RegisterAssignmentDefinition, MemoryActionDefinition

def output_as_x64dbg(definition):
  return '\n'.join(outputDefinitions(definition, x64dbg_outputter))

def outputDefinitions(definition, outputter, userdata=None):
  yield from outputMockedReturns(definition.actions, outputter, userdata)
  yield from outputMemoryAllocations(definition.memories.keys(), outputter, userdata)
  yield from outputActions(definition.actions, outputter, userdata)

def outputMockedReturns(actions, outputter, userdata=None):
  for action in actions:
    if isinstance(action, RegisterAssignmentDefinition):
      if action.value.startswith('RET.'):
        yield outputter.get_mocked_return_as_output(action.value[4:], userdata)

def outputMemoryAllocations(names, outputter, userdata=None):
  for name in sorted(names):
    yield outputter.get_allocation_as_output(name, userdata)

def outputActions(actions, outputter, userdata=None):
  for action in actions:
    if isinstance(action, MemoryActionDefinition):
      yield outputter.get_memory_action_as_output(action.address, action.action, action.size, userdata)

  for action in actions:
    if isinstance(action, RegisterAssignmentDefinition):
      yield outputter.get_assignment_as_output(action.name, action.value, userdata)
    elif isinstance(action, MemoryActionDefinition):
      pass
    else:
      raise NotImplementedException

class x64dbg_outputter:
  SIZES = {'B': 'byte', 'W': 'word', 'D': 'dword', 'Q': 'qword'}

  @staticmethod
  def get_mocked_return_as_output(number, userdata):
    return 'mov byte:[$rip_for_ret + {0}], 0xC3\nbp $rip_for_ret + {0}\n'.format(hex(int(number)))

  @staticmethod
  def get_allocation_as_output(name, userdata):
    return 'alloc\nmov $' + name + ', $lastalloc\ncall put_lastalloc_under_test\n'

  @staticmethod
  def get_assignment_as_output(target, source, userdata):
    if source.startswith('RET.'):
      number = source[4:]
      source = '$rip_for_ret + ' + hex(int(number))
    elif source == 'RET':
      source = '$rip_for_ret'
    if not source.startswith('0x') and source[0] in '0123456789':
      source = hex(int(source))
    if source.startswith('m'):
      if '.' in source:
        size = x64dbg_outputter.SIZES[source[-1]]
        source = size + '($' + source.split('.')[0] + ' + 0x' + source.split('.')[1] + ')'
      else:
        source = '$' + source
    if target.startswith('m'):
      size = x64dbg_outputter.SIZES[target[-1]]
      target = size + ':[$' + target.split('.')[0] + ' + 0x' + target.split('.')[1] + ']'
    if target == 'cip':
      return 'mov $rip_under_test, {1}\n'.format(target, source)
    return 'mov {0}, {1}'.format(target, source)

  @staticmethod
  def get_memory_action_as_output(address, action, size, userdata):
    return 'setpagerights 0x' + ('%X' % address) + ', ReadWrite'
