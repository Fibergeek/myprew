class simple_value_assignments:
  pass

simple_value_assignments.input = """
dl = 'A'
dh = 1
ecx = 0xAA
r8d = ecx
r9 = RET
"""
simple_value_assignments.expected = """
mov dl, 'A'
mov dh, 0
mov ecx, 0xAA
mov r8d, ecx
mov r9, $rip_for_ret
"""

class simple_memory_assignments:
  pass

simple_memory_assignments.input = """
ecx = 0xAA
rax = mA
r8 = mB
r9 = RET.1
"""

simple_memory_assignments.expected = """
mov byte:[$rip_for_ret + 1], 0xC3
bp $rip_for_ret + 1

alloc
mov $mA, $lastalloc

alloc
mov $mB, $lastalloc

mov ecx, 0xAA
mov rax, $mA
mov r8, $mB
mov r9, $rip_for_ret + 1
"""

class complex_memory_assignments:
  pass

complex_memory_assignments.input = """
mov rcx, 0
mov dx, 1

mA.0.Q = mB
mB.0.Q = mB

mB.0.D = 1
mB.4.B = '4'
mB.5.B = 0
mB.48.Q = RET.1

mZ.00.Q = mA
mZ.10.D =  ecx
mZ.14.D =  ecx
mZ.18.W =  dx
mZ.1A.W =  dx
mZ.1C.W =  dx
mZ.1E.W =  dx
mZ.08.Q = RET.2

r8 = mZ
"""

class simple_cip_assignment:
  pass

simple_cip_assignment.input = """
cip = 0x401000
"""
simple_cip_assignment.input.expected = """
mov $rip_under_test, 0x401000
"""

complex_memory_assignments.expected = """
mov byte:[$rip_for_ret + 1], 0xC3
bp $rip_for_ret + 1

mov byte:[$rip_for_ret + 2], 0xC3
bp $rip_for_ret + 2

alloc
mov $mA, $lastalloc

alloc
mov $mB, $lastalloc

alloc
mov $mZ, $lastalloc

mov rcx, 0
mov dx, 1

mov qword:[$mA+0x0], $mB
mov qword:[$mB+0x0], $mB

mov dword:[$mB+0x0], 1
mov byte:[$mB+0x4], '4'
mov byte:[$mB+0x5], 0
mov qword:[$mB+0x48], $rip_for_ret + 1

mov qword:[$mZ+0x0], $mA
mov dword:[$mZ+0x10], ecx
mov dword:[$mZ+0x14], ecx
mov word:[$mZ+0x18], dx
mov word:[$mZ+0x1A], dx
mov word:[$mZ+0x1C], dx
mov word:[$mZ+0x1E], dx
mov qword:[$mZ+0x8], $rip_for_ret + 2

mov r8, $mZ
"""

