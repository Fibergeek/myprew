CODE_AS_SIZES = {'B': 1, 'W': 2, 'D': 4, 'Q': 8}

class MockDefinition:
  def __init__(self):
    self.memories = dict()
    self.actions = list()

class RegisterAssignmentDefinition:
  def __init__(self, name, value):
    self.name = name
    self.value = value

class MemoryActionDefinition:
  def __init__(self, address, action, size=0):
    self.address = int(address[2:], 16)
    self.action = action
    self.size = size

def parse_string(text, printer=None):
  if printer == None:
    printer = print

  definition = MockDefinition()
  for line in text.split('\n'):
    line = line.strip()
    if not line:
      continue

    to_add = parse_add_expression(line, printer)
    if not to_add:
      to_add = parse_assignment_expression(line, definition.memories, printer)
      if not to_add:
        printer('Warning! Invalid expression is ignored: {0}'.format(line))
        continue

    definition.actions.append(to_add)

  return definition

def parse_add_expression(line, printer):
    left, right = split_add_expression(line)
    if not left or not right:
      return None

    if not is_constant(left):
      printer('Warning! Memory address is invalid: {0}'.format(line))
      return None

    if not right in ['R', 'W', 'RW', 'WR']:
      printer('Warning! Action is invalid due to the value: {0}'.format(line))
      return None

    return MemoryActionDefinition(left, right)

def parse_assignment_expression(line, memories, printer):
    left, right = split_assignment_expression(line)
    if not left or not right:
      return None

    if is_memory_expression(left):
      if not get_memory_name(left) in memories:
        memories[get_memory_name(left)] = {}
    elif is_memory_name(left):
      printer('Warning! Memory assignment is invalid: {0}'.format(line))
      return None
    elif not is_register_name(left):
      printer('Warning! Assignment is invalid: {0}'.format(line))
      return None

    if is_memory_expression(right):
      if not get_memory_name(right) in memories:
        memories[get_memory_name(right)] = {}
    elif not is_register_name(right) and not is_constant(right) and not is_mocked_return(right):
      printer('Warning! Assignment is invalid due to the value: {0}'.format(line))
      return None

    return RegisterAssignmentDefinition(left, right)

def is_register_name(name):
  if name in ['ip', 'eip', 'rip']:
    return True
  if name in ['al', 'ah', 'ax', 'eax', 'rax']:
    return True
  if name in ['cl', 'ch', 'cx', 'ecx', 'rcx']:
    return True
  if name in ['dl', 'dh', 'dx', 'edx', 'rdx']:
    return True
  if name in ['bl', 'bh', 'bx', 'ebx', 'rbx']:
    return True
  if name in ['r8b', 'r8w', 'r8d', 'r8']:
    return True
  if name in ['r9b', 'r9w', 'r9d', 'r9']:
    return True
  return False

def is_memory_expression(value):
  name = value.split('.')[0] if '.' in value else value
  return is_memory_name(name) or name == 'TLS'

def get_memory_name(value):
  if is_memory_expression(value):
    return value[:2]
  return ''

def is_memory_name(name):
  return len(name) == 2 and name[0] == 'm' and name[1] in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def is_valid_name(name):
  return is_register_name(name) or is_memory_name(name)

def is_constant(value):
  if len(value) == 3 and value.startswith('\'') and value.endswith('\''):
    return True
  if len(value) >= 3 and value.startswith('0x'):
    return True
  if len(value) >= 1 and value[0] in '0123456789':
    return True
  return False

def is_mocked_return(value):
  return value == 'RET' or value.startswith('RET.')

def is_add_expression(expression):
  return expression.count('+=') == 1

def is_assignment_expression(expression):
  return expression.count('=') == 1

def split_add_expression(expression):
  if not is_add_expression(expression):
    return '', ''
  left, _, right = expression.partition('+=')
  return left.strip(), right.strip()

def split_assignment_expression(expression):
  if not is_assignment_expression(expression):
    return '', ''
  left, _, right = expression.partition('=')
  return left.strip(), right.strip()
