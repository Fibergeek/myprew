try:
  from idaapi import BADADDR
  IDA_AVAILABLE = True
except ImportError:
  IDA_AVAILABLE = False

from .decoder.registers import *
from .decoder.instructions import *
from .decoder.analysis import *
from .decoder import zydis
from .assembler import nasm

if IDA_AVAILABLE:
    from .ida.bridge import *
    from .ida.bridge_workflow import *
    from .ida.bridge_server import run_bridge_server, close_bridge_server
    from .ida.memory import *
    from .ida.utils import *

    from .ida.segments import *
    
    from .ida.items import *
    from .ida.item_analysis import *
    from .ida.item_coloring import *
    from .ida.item_workflow import *

    from .ida.functions import *
    from .ida.function_analysis import *
    from .ida.function_coloring import *
    from .ida.function_grouping import *
    from .ida.function_matching import *
    from .ida.function_workflow import *
    from .ida.function_restoration import *

    try:
        from .ida.forms import *
    except Exception as ex:
        print('Forms are not loaded! {0}'.format(ex))
    
    from .ida.gui import *

from .x64dbg.x64dbg_remote_runner import *

from .func_input_mocker import func_input_mocker

def reload(extra_to_reload=None):
  from imp import reload as reload_module
  
  if IDA_AVAILABLE:
    reload_module(ida.bridge)
    reload_module(ida.bridge_workflow)
    reload_module(ida.bridge_server)
    reload_module(ida.memory)
    reload_module(ida.utils)
    
    reload_module(ida.items)
    reload_module(ida.item_analysis)
    reload_module(ida.item_coloring)
    reload_module(ida.item_workflow)
    
    reload_module(ida.functions)
    reload_module(ida.function_analysis)
    reload_module(ida.function_coloring)
    reload_module(ida.function_grouping)
    reload_module(ida.function_matching)
    reload_module(ida.function_workflow)
    reload_module(ida.function_restoration)
    
    reload_module(ida.gui)
  
    reload_module(ida.x64dbg_remote_runner)

  reload_module(func_input_mocker)
  
  if extra_to_reload != None:
    reload_module(extra_to_reload)
