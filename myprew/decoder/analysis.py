from io import BytesIO
from .zydis import decode_block, decode_function
from .registers import REGISTERS_16, REGISTERS_32, REGISTERS_64
from .functions import is_branching_instruction

def ends_with_branching(instructions):
  if not instructions:
     return False
  last_instr = instructions[-1]
  mnem = last_instr.Mnem
  return is_branching_instruction(last_instr)

def contains_privileged_instruction(instructions):
  for instr in instructions:
    if instr.IsPrivileged:
      return True
    mnem = instr.Mnem
    if mnem.startswith('IRET'):
      return True
    elif mnem == 'CLI' or mnem == 'STI':
      return True
    elif mnem.startswith('IN'):
      if mnem == 'INT':
        if instr.Operands[0].Value != [1, 3, 0x2E]:
          return True
      elif mnem in ['IN', 'INS', 'INSB', 'INSW', 'INSD', 'INSD']:
        return True
    elif mnem.startswith('OUT'):
      if mnem in ['OUT', 'OUTS', 'OUTSB', 'OUTSW', 'OUTSD', 'OUTSQ']:
        return True
  return False

def contains_debugger_instruction(instructions):
  for instr in instructions:
    mnem = instr.Mnem
    if mnem in ['INT1', 'INT3', 'INTO']:
      return True
    elif mnem == 'INT':
      if instr.Operands[0].Value in [1, 3]:
        return True
  return False

def contains_pop_without_push(instructions):
  for i, instr in enumerate(instructions):
    if instr.Mnem.startswith('POP'):
      if i == 0:
        return True
      for prev_instr in instructions[:i]:
        if instr.Mnem == 'POP' and prev_instr.Mnem == 'PUSH':
          return False
        if instr.Mnem.startswith('POPF') and prev_instr.Mnem.startswith('PUSHF'):
          return False
        if instr.Mnem.startswith('POPA'):
          return False
      return True
  return False

def contains_pop_without_stack_register_write(instructions):
  for i, instr in enumerate(instructions):
    if instr.Mnem.startswith('POP'):
      if i == 0:
        return True
      for prev_instr in instructions[:i]:
        if prev_instr.Mnem.startswith('PUSH'):
          return False
        if prev_instr.Mnem == 'XCHG':
          ops = prev_instr.Text.split(' ')[1:]
        else:
          ops = prev_instr.Text.split(',')[0].split(' ')[1:]
        for op in ops:
          if 'rsp' in op:
            if not '[rsp' in op:
              return False
          if prev_instr.Bitness == 64 and 'esp' in op: # TODO: RSP is destroyed!
            if not '[esp' in op:
              return False # RSP is destroyed!
      return True
  return False

def modifies_stack_register(instructions):
  for instr in instructions:
    for op in instr.Operands:
      if op.IsReg and 'Write' in op.ActionText and op.Reg.Name.endswith('sp'):
        return True
  return False

def reads_full_register(instructions, reg):
  minimum_read_size = reg.GrandParent.Bitness
  for instr in instructions:
    for op in instr.Operands:
      if op.IsReg and 'Read' in op.ActionText and op.Bitness >= minimum_read_size and op.Reg.GrandParent == reg.GrandParent:
        return True
  return False

def modifies_full_register(instructions, reg):
  for instr in instructions:
    minimum_write_size = 32 if instr.Bitness == 64 else instr.Bitness
    for op in instr.Operands:
      if op.IsReg and 'Write' in op.ActionText and op.Bitness >= minimum_write_size and op.Reg.GrandParent == reg.GrandParent:
        return True
  return False

def modifies_any_of_grandparent_register(instructions, grandparent_reg):
  for instr in instructions:
    for op in instr.Operands:
      if op.IsReg and 'Write' in op.ActionText and op.Reg.GrandParent == grandparent_reg.GrandParent:
        return True
  return False

def modifies_any_of_register(instructions, reg):
  for instr in instructions:
    for op in instr.Operands:
      if op.IsReg and 'Write' in op.ActionText and (op.Reg.Name == reg.Name or op.Reg.Parent.Name == reg.Name or op.Reg.Parent.Parent.Name == reg.Name):
        return True
  return False

def accesses_any_of_register(instructions, reg):
  for instr in instructions:
    for op in instr.Operands:
      if op.IsReg and ('Read' in op.ActionText or 'Write' in op.ActionText) and op.Reg.GrandParent == reg.GrandParent:
        return True
  return False
  
def destroys_any_protected_register(instructions):
  # TODO: ensure registers are updated for 64-bit only
  protected_registers = ['ebx', 'ebp', 'esi', 'edi']
  protected_registers.extend(['r12d', 'r13d', 'r14d', 'r15d'])

  for before_count, instr in enumerate(instructions):
    for op in instr.Operands:
      if op.IsReg and 'Write' in op.ActionText:
        for reg in protected_registers:
          grandparent_reg = REGISTERS_64[reg].GrandParent
          if modifies_any_of_grandparent_register([instr], grandparent_reg):
            if before_count == 0 or not reads_full_register(instructions[:before_count], grandparent_reg):
              return True
  return False

def contains_unusual_instruction(instructions):
  if instructions[0].Mnem == 'CALL':
    return True
  for instr in instructions:
    if instr.Mnem == 'RETF':
      return True
    if instr.Mnem == 'RET' and instr.Operands[0].IsImm and instr.Operands[0].Value != 0:
      return True
  return False	  

def contains_loop_without_rcx_write(instructions):
  for count_before, instr in enumerate(instructions):
    if instr.Mnem.startswith('LOOP'):
      for op in instr.Operands:
        if count_before == 0:
          return True
        if op.IsReg and op.Text.endswith('cx'):
          if not modifies_full_register(instructions[:count_before], op.Reg):
            return True
      break
  return False	  

def uses_dirty_register_in_memory_access_without_write_first(instructions):
  # TODO: ensure registers are updated for 64-bit only
  dirty_registers = ['eax', 'ebx', 'ebp', 'esi', 'edi']
  dirty_registers.extend(['rax', 'rbx', 'rbp', 'rsi', 'rdi', 'r10', 'r10d', 'r11', 'r11d', 'r12', 'r12d', 'r13', 'r13d', 'r14', 'r14d', 'r15', 'r15d'])
  
  for before_count, instr in enumerate(instructions):
    for op in instr.Operands:
      if op.IsMem and instr.Mnem != 'NOP' and instr.Mnem != 'LEA':
        if op.HasBaseReg and op.BaseReg.Name in dirty_registers:
          if before_count == 0:
            return True
          if not modifies_full_register(instructions[:before_count], op.BaseReg):
            return True
        if op.HasIndexReg and op.IndexReg.Name in dirty_registers:
          if before_count == 0:
            return True
          if not modifies_full_register(instructions[:before_count], op.IndexReg):
            return True
  
  return False

def has_invalid_branch_target(instructions, is_executable_ea):
  for instr in instructions:
    # TODO: support CALL FAR immseg:immofs
    if is_branching_instruction(instr) and not instr.Mnem.startswith('RET') and instr.Operands[0].IsImm:
      target_ea = int(' '.join(instr.Text.split(' ')[1:])[2:], 16)
      if not is_executable_ea(target_ea):
        return True
  return False

def contains_flag_test_without_write(instructions):
  mnem = instructions[0].Mnem
  if mnem.startswith('J') and mnem != 'JMP' and mnem != 'JMPF':
    return True
  return False

def startswith_known_jmp(instructions):
  i = 0
  while True:
    instr = instructions[i]
    if instr.Text == 'lea rsp, [rsp]' or instr.Mnem == 'NOP':
      i += 1
    else:
      break
  
  mnem = instr.Mnem
  if mnem == 'JMP' and instr.Operands[0].IsImm:
    return True
  return False

def contains_invalid_abs_mem_access(instructions):
  for instr in instructions:
    if instr.Mnem != 'LEA':
      for op in instr.Operands:
        if op.IsMem and not op.HasBaseReg and not op.HasIndexReg:
          if 'Write' in op.ActionText and not 'CondWrite' in op.ActionText:
            return True
          if op.Disp < 0x7FFE000 or op.Disp >= 0x7FFF000:
            if op.Disp >= 0x1000 or op.SegmentReg.Name != 'gs': # TODO: support FS for 32-bit systems, should we support Write?
              return True
  return False

def analyze_function_prologue(ea_or_block, is_executable_ea=None):
  import idaapi # WARNING ! TODO: fix this dependency on IDA
  if isinstance(ea_or_block, int):
    ea = ea_or_block
    ea_bytes = idaapi.get_bytes(ea, 0x10000)
    block = list(decode_block(ea_bytes, None, ea))
  elif isinstance(ea_or_block, list):
    block = ea_or_block
    ea = block[0].Addr if block else 0
  else:
    block = list(ea_or_block)
    ea = block[0].Addr if block else 0
  
  if False:
    print([hex(i.Addr) + ' ' + i.Text for i in block])
    print(hex(ea), 'PrivilegedInstruction:', contains_privileged_instruction(block), 'DebuggerInstruction:', contains_debugger_instruction(block))
    print(hex(ea), 'EndsWithBranch:', ends_with_branching(block), 'PopWithoutPush:', contains_pop_without_push(block))
  if not ends_with_branching(block):
    return 'Invalid instruction'
  if contains_privileged_instruction(block):
    return 'Privileged instruction'
  if contains_debugger_instruction(block):
    return 'Debugger instruction'
  if contains_pop_without_push(block):
    if contains_pop_without_stack_register_write(block):
      return 'POP without PUSH'
  if uses_dirty_register_in_memory_access_without_write_first(block):
    return 'Reserved register used in Memory Access'
  if destroys_any_protected_register(block):
    return 'Protected register destroyed'
  if is_executable_ea != None and has_invalid_branch_target(block, is_executable_ea):
    return 'Invalid branch target'
  if startswith_known_jmp(block):
    # NOTE: We know the target is Valid!
    target_ea = int(' '.join(block[-1].Text.split(' ')[1:])[2:], 16)
    if len(block) == 1:
      return 'Followed JMP: '+ analyze_function_prologue(target_ea, is_executable_ea)
    return 'Followed Obfuscated JMP: '+ analyze_function_prologue(target_ea, is_executable_ea)
  if contains_unusual_instruction(block):
    return 'Unusual instruction'
  if contains_loop_without_rcx_write(block):
    return 'Loop without write'
  if contains_invalid_abs_mem_access(block):
    return 'Absolute Memory Access'
  if contains_flag_test_without_write(block):
    result = analyze_function_prologue(block[1:], is_executable_ea)
    if result == 'Valid':
      target_ea = int(' '.join(instr.Text.split(' ')[1:])[2:], 16)
      result = analyze_function_prologue(target_ea, is_executable_ea)
      if result != 'Valid':
        return 'Followed Jcc: ' + result
    else:
      return 'Ignored Jcc: ' + result
  
  #print_function(ea, ea_bytes)
  
  return 'Valid'

def tests():
 def print_function(ea, ea_bytes):
  func = list(decode_function(ea_bytes, None, ea))
  print([hex(i.Addr) + ' ' + i.Text for i in func])
 
 print(hex(get_screen_ea()), 'IsValid:', analyze_function_prologue(get_screen_ea()))
 print(ZydisInstruction(b'\x91', None).Mnem, [op.Text + ':' + op.ActionText + ',' + op.VisibilityText for op in ZydisInstruction(b'\x91', None).Operands])
 
 print([op.Text + ':' + op.ActionText + ',' + op.VisibilityText for op in ZydisInstruction(idaapi.get_bytes(get_screen_ea(), 15), None, get_screen_ea()).Operands])
 
 #print(modifies_stack_register([decode_instruction(idaapi.get_bytes(get_screen_ea(), 15), None, get_screen_ea())]))
 pass
