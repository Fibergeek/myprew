class FunctionContext:
    def __init__(self, address):
        self.address = address
        self.known_funcs = set([address])
        self.known_jmp_targets = set()

def is_branching_instruction(last_instr, context=None):
    if last_instr == None:
        return False
    if is_function_exit_instruction(last_instr, context):
        return True
    mnem = last_instr.Mnem
    return mnem.startswith('J') or mnem.startswith('LOOP') or mnem.startswith('CALL')

def is_function_exit_instruction(last_instr, context):
    if last_instr == None:
        return False
    
    mnem = last_instr.Mnem
    if isinstance(context, FunctionContext):
        if mnem == 'CALL':
            call_target = ' '.join(last_instr.Text.split(' ')[1:])
            if call_target.startswith('0x'):
                call_target = int(call_target[2:], 16)
                context.known_funcs.add(call_target)
        if mnem.startswith('J') or mnem.startswith('LOOP'):
            jump_target = ' '.join(last_instr.Text.split(' ')[1:])
            if jump_target.startswith('0x'):
                jump_target = int(jump_target[2:], 16)
                if jump_target > context.address and not jump_target in context.known_funcs:
                    if mnem != 'JMP': # TODO: optimize this, review this!
                        context.known_jmp_targets.add(jump_target)
        
        addr = last_instr.Addr + last_instr.Length
        if addr in context.known_jmp_targets:
            return False
        
        if context.known_jmp_targets:
            #print('max known', hex(max(context.known_jmp_targets)), 'addr', hex(addr))
            if addr - max(context.known_jmp_targets) < 0x10:
                return False
    
    return mnem.startswith('JMP') or mnem.startswith('RET') or mnem.startswith('IRET')
