import pydis
from io import BytesIO

from .functions import FunctionContext, is_branching_instruction, is_function_exit_instruction
from .registers import REGISTERS_16, REGISTERS_32, REGISTERS_64
from .instructions import OPVISIBILITY_HIDDEN, OPVISIBILITY_IMPLICIT, OPVISIBILITY_EXPLICIT
from .instructions import OPACTION_NONE, OPACTION_READ, OPACTION_CREAD, OPACTION_WRITE, OPACTION_CWRITE
from .instructions import Instruction, RegisterOperand, ImmediateOperand, MemoryOperand

X16_DECODER = pydis.Decoder(mode=pydis.MachineMode.Real16, address_width=pydis.AddressWidth.Width16)
X32_DECODER = pydis.Decoder(mode=pydis.MachineMode.LongCompat32, address_width=pydis.AddressWidth.Width32)
X64_DECODER = pydis.Decoder(mode=pydis.MachineMode.Long64, address_width=pydis.AddressWidth.Width64)

def get_action_and_visibility(op):
        if op.visibility == pydis.OperandVisibility.Hidden:
          visibility = OPVISIBILITY_HIDDEN
        elif op.visibility == pydis.OperandVisibility.Implicit:
          visibility = OPVISIBILITY_IMPLICIT
        elif op.visibility == pydis.OperandVisibility.Explicit:
          visibility = OPVISIBILITY_EXPLICIT
        else:
          raise NotImplementedError('Unsupported visibility: {0}'.format(op.visibility))
        
        action = OPACTION_NONE
        if op.action == pydis.OperandAction.Read or op.action == pydis.OperandAction.ReadWrite or op.action == pydis.OperandAction.Read_Cond_Write:
          action += OPACTION_READ
        elif op.action == pydis.OperandAction.Cond_Read or op.action == pydis.OperandAction.Write_Cond_Read:
          action += OPACTION_READ + OPACTION_CREAD
        if op.action == pydis.OperandAction.Write or op.action == pydis.OperandAction.ReadWrite or op.action == pydis.OperandAction.Write_Cond_Read:
          action += OPACTION_WRITE
        elif op.action == pydis.OperandAction.Cond_Write or op.action == pydis.OperandAction.Read_Cond_Write:
          action += OPACTION_WRITE + OPACTION_CWRITE
    
        return action, visibility  

class ZydisRegisterOperand(RegisterOperand):
    def __init__(self, op, reg_db):
        regname = str(op)
        if not regname in reg_db:
            raise NotImplementedError('The register {0} does not exist in the DB!'.format(regname))
        
        action, visibility = get_action_and_visibility(op)
        RegisterOperand.__init__(self, reg_db[regname], action=action, visibility=visibility)

class ZydisImmediateOperand(ImmediateOperand):
    def __init__(self, op):
        action, visibility = get_action_and_visibility(op)
        ImmediateOperand.__init__(self, op.size, op.element_size, op.immediate.value, action=action, visibility=visibility)

class ZydisMemoryOperand(MemoryOperand):
    def __init__(self, op, reg_db):
        seg_regname = str(op.memory.segment)
        if not seg_regname in reg_db:
            raise NotImplementedError('The segment register {0} does not exist in the DB!'.format(seg_regname))
        
        base_regname = str(op.memory.base) if op.memory.base.value else None
        if base_regname != None and not base_regname in reg_db:
            raise NotImplementedError('The base register {0} does not exist in the DB!'.format(base_regname))
        
        index_regname = str(op.memory.index) if op.memory.index.value else None
        if index_regname != None and not index_regname in reg_db:
            raise NotImplementedError('The index register {0} does not exist in the DB!'.format(index_regname))
        
        segment_reg = reg_db.get(seg_regname)
        base_reg    = reg_db.get(base_regname)
        index_reg   = reg_db.get(index_regname)
        
        action, visibility = get_action_and_visibility(op)
        MemoryOperand.__init__(self, op.size, segment_reg, base_reg, index_reg, op.memory.scale, op.memory.displacement, action=action, visibility=visibility)

class ZydisInstruction(Instruction):
    def __init__(self, data, platform, address=0):
        Instruction.__init__(self, address=address)
        
		# TODO: choose the decoder based on the platform
        reg_db, instr = REGISTERS_64, X64_DECODER.decode_instruction(data, address=address)
        if instr.length > len(data):
            raise Exception('Not enough data to decode correctly!')
        
        self._text = str(instr)
        self._bytes = instr.bytes
        self._mnem = instr.mnemonic.upper()
        if 'ret far' in self._text:
             self._mnem = 'RETF'
        self._length = instr.length
        self._is_privileged = bool(instr.attributes & pydis.InstructionAttribute.Is_Priviledged)
        try:
            for op in instr.operands:
                if op.type == pydis.OperandType.Unused:
                    raise NotImplementedError('.Unused')#self._operands.append(ZydisOperand(op))
                elif op.type == pydis.OperandType.Immediate:
                    self._operands.append(ZydisImmediateOperand(op))
                elif op.type == pydis.OperandType.Memory:
                    self._operands.append(ZydisMemoryOperand(op, reg_db))
                elif op.type == pydis.OperandType.Register:
                    self._operands.append(ZydisRegisterOperand(op, reg_db))
                else:
                    raise NotImplementedError('Unknown Operand type')
        except Exception as ex:
            raise Exception('{0}: {1}'.format(hex(address), ex))

def decode_instruction(bytes_or_reader, platform, address=0):
    if bytes_or_reader == None:
        return None

    if isinstance(bytes_or_reader, bytes):
        instr = ZydisInstruction(bytes_or_reader[:15], platform, address)
    else:
        raise NotImplementedError('Decode an instruction requires bytes')
        data_reader = bytes_or_reader
        data_pos = data_reader.seek(0, 1)
        data = data_reader.read(15)
        instr = ZydisInstruction(data or b'', platform)
        data_reader.seek(data_pos + instr.Length, 0)
    
    return instr

def decode_block(bytes_or_reader, platform, address=0, is_branch_target=None):
    context = FunctionContext(address)
    if is_branch_target != None:
        should_exit = lambda last_instr, context: (is_branching_instruction(last_instr) or is_branch_target(last_instr))
    else:
        should_exit = is_branching_instruction
    for instr in decode_instructions(bytes_or_reader, platform, address, should_exit):
        yield instr

def decode_function(bytes_or_reader, platform, address=0):
    context = FunctionContext(address)
    for instr in decode_instructions(bytes_or_reader, platform, address, is_function_exit_instruction, context):
        yield instr

def decode_instructions(bytes_or_reader, platform, address=0, should_exit=None, should_exit_context=None):
    if bytes_or_reader == None:
        return None
    
    if should_exit == None:
        should_exit = is_branching_instruction
    
    if isinstance(bytes_or_reader, bytes):
        data_pos, data_reader = 0, BytesIO(bytes_or_reader)
    else:
        data_pos, data_reader = 0, bytes_or_reader
    
    instr = None
    while not should_exit(instr, should_exit_context):
        data = data_reader.read(15)
        if data == None or len(data) == 0:
            break
        try:
            instr = decode_instruction(data, platform, address)
        except Exception as ex:
            sex = str(ex)
            if 'BadRegister' in sex or 'DecodingError' in sex or 'IllegalLock' in sex or 'IllegalMap' in sex or 'IllegalRex' in sex or 'MalformexEvex' in sex:
                break
            raise Exception('{0}: {1}'.format(hex(address), ex))
        yield instr
        address += instr.Length
        data_pos += instr.Length
        data_reader.seek(data_pos, 0)
