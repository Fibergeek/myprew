REG_PC    = 0
REG_FLAGS = 1
REG_SEG   = 2
REG_GPR   = 3
REG_FP    = 4
REG_MMX   = 5
REG_SSE   = 6
REG_SSE_MASK = 7

REGISTERS_16 = {}
REGISTERS_32 = {}
REGISTERS_64 = {}

class Register:
  def __init__(self, name, size):
    if not (name or '').strip():
      raise ValueError('Invalid register name: {0}'.format(name))
    self._name = name
    self._size = size
  
  @property
  def Name(self):
    return self._name
  
  @property
  def Bitness(self):
    return self._size * 8
  
  @property
  def Size(self):
    return self._size
  
  @property
  def GrandParent(self):
    return self

class Subregister:
  def __init__(self, parent_reg):
    if not isinstance(parent_reg, Register):
      raise ValueError('Expected a parent register')
    self._parent = parent_reg
  
  @property
  def Parent(self):
    return self._parent

class SystemRegister(Register):
  pass

class SystemSubregister(SystemRegister, Subregister):
  def __init__(self, name, size, parent_reg):
    SystemRegister.__init__(self, name, size)
    Subregister.__init__(self, parent_reg)
  
  @property
  def GrandParent(self):
    return self._parent.GrandParent

class SegmentRegister(Register):
  pass

class MaskRegister(Register):
  pass

class GP_Register(Register):
  pass

class GP_Subregister(GP_Register, Subregister):
  def __init__(self, name, size, parent_reg):
    GP_Register.__init__(self, name, size)
    Subregister.__init__(self, parent_reg)
  
  @property
  def GrandParent(self):
    return self._parent.GrandParent

class FP_Register(Register):
  pass

class MMX_Register(Register):
  pass

class XMM_Register(Register):
  pass

class XMM_Subregister(XMM_Register, Subregister):
  def __init__(self, name, size, parent_reg):
    XMM_Register.__init__(self, name, size)
    Subregister.__init__(self, parent_reg)
  
  @property
  def GrandParent(self):
    return self._parent.GrandParent

def RegisterRegister(dbs, type, name, size, index, parent_name=None):
  for db in dbs:
    if parent_name == None:
      db[name] = type_to_func(type, False)(name, size)
    elif parent_name in db:
      db[name] = type_to_func(type, True)(name, size, db[parent_name])
    elif type in [REG_PC, REG_FLAGS, REG_GPR]:
      # To keep out register definitions readable, we will downgrade Subregister to Register here!
      db[name] = type_to_func(type, False)(name, size)
    else:
      raise ValueError('Parent does not exist and is not supported for the register type!')

def type_to_func(type, is_subreg=None):
  if type == REG_PC:
    if is_subreg:
      return SystemSubregister
    else:
      return SystemRegister
  elif type == REG_FLAGS:
    if is_subreg:
      return SystemSubregister
    else:
      return SystemRegister
  elif type == REG_SEG:
    if is_subreg:
      raise ValueError('Subregisters are not supported for register type: {0}'.format(type))
    return SegmentRegister
  elif type == REG_GPR:
    if is_subreg:
      return GP_Subregister
    else:
      return GP_Register
  elif type == REG_FP:
    if is_subreg:
      raise ValueError('Subregisters are not supported for register type: {0}'.format(type))
    return Register
  elif type == REG_MMX:
    if is_subreg:
      raise ValueError('Subregisters are not supported for register type: {0}'.format(type))
    return MMX_Register
  elif type == REG_SSE:
    if is_subreg:
      return XMM_Subregister
    else:
      return XMM_Register
  elif type == REG_SSE_MASK:
    if is_subreg:
      raise ValueError('Subregisters are not supported for register type: {0}'.format(type))
    return MaskRegister
  raise ValueError('Invalid register type: {0}'.format(type))

RegisterRegister([REGISTERS_64], REG_PC, 'rip', 64, None)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_PC, 'eip', 32, None, 'rip')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_PC, 'ip', 16, None, 'eip')

RegisterRegister([REGISTERS_16, REGISTERS_32], REG_PC, 'gdtr', 48, None, 'gdtr')
RegisterRegister([REGISTERS_16, REGISTERS_32], REG_PC, 'ldtr', 48, None, 'ldtr')
RegisterRegister([REGISTERS_64], REG_PC, 'gdtr', 80, None, 'gdtr')
RegisterRegister([REGISTERS_64], REG_PC, 'ldtr', 80, None, 'ldtr')

RegisterRegister([REGISTERS_64], REG_FLAGS, 'rflags', 64, None)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_FLAGS, 'eflags', 32, None, 'rflags')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_FLAGS, 'flags', 16, None, 'eflags')

RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_SEG, 'cs', 16, None)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_SEG, 'ds', 16, None)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_SEG, 'es', 16, None)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_SEG, 'ss', 16, None)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SEG, 'fs', 16, None)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SEG, 'gs', 16, None)

RegisterRegister([REGISTERS_64], REG_GPR, 'rax', 64, 0)
RegisterRegister([REGISTERS_64], REG_GPR, 'rcx', 64, 1)
RegisterRegister([REGISTERS_64], REG_GPR, 'rdx', 64, 2)
RegisterRegister([REGISTERS_64], REG_GPR, 'rbx', 64, 3)
RegisterRegister([REGISTERS_64], REG_GPR, 'rsp', 64, 4)
RegisterRegister([REGISTERS_64], REG_GPR, 'rbp', 64, 5)
RegisterRegister([REGISTERS_64], REG_GPR, 'rsi', 64, 6)
RegisterRegister([REGISTERS_64], REG_GPR, 'rdi', 64, 7)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_GPR, 'eax', 32, 0, 'rax')
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_GPR, 'ecx', 32, 1, 'rcx')
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_GPR, 'edx', 32, 2, 'rdx')
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_GPR, 'ebx', 32, 3, 'rbx')
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_GPR, 'esp', 32, 4, 'rsp')
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_GPR, 'ebp', 32, 5, 'rbp')
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_GPR, 'esi', 32, 6, 'rsi')
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_GPR, 'edi', 32, 7, 'rdi')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'ax', 16, 0, 'eax')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'cx', 16, 1, 'ecx')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'dx', 16, 2, 'edx')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'bx', 16, 3, 'ebx')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'sp', 16, 4, 'esp')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'bp', 16, 5, 'ebp')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'si', 16, 6, 'esi')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'di', 16, 7, 'edi')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'al', 8, 0, 'ax')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'ah', 8, 1, 'ax')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'cl', 8, 2, 'cx')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'ch', 8, 3, 'cx')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'dl', 8, 4, 'dx')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'dh', 8, 5, 'dx')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'bl', 8, 6, 'bx')
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_GPR, 'bh', 8, 7, 'bx')

RegisterRegister([REGISTERS_64], REG_GPR, 'r8',  64,  8)
RegisterRegister([REGISTERS_64], REG_GPR, 'r9',  64,  9)
RegisterRegister([REGISTERS_64], REG_GPR, 'r10', 64, 10)
RegisterRegister([REGISTERS_64], REG_GPR, 'r11', 64, 11)
RegisterRegister([REGISTERS_64], REG_GPR, 'r12', 64, 12)
RegisterRegister([REGISTERS_64], REG_GPR, 'r13', 64, 13)
RegisterRegister([REGISTERS_64], REG_GPR, 'r14', 64, 14)
RegisterRegister([REGISTERS_64], REG_GPR, 'r15', 64, 15)
RegisterRegister([REGISTERS_64], REG_GPR, 'r8d',  32,  8, 'r8')
RegisterRegister([REGISTERS_64], REG_GPR, 'r9d',  32,  9, 'r9')
RegisterRegister([REGISTERS_64], REG_GPR, 'r10d', 32, 10, 'r10')
RegisterRegister([REGISTERS_64], REG_GPR, 'r11d', 32, 11, 'r11')
RegisterRegister([REGISTERS_64], REG_GPR, 'r12d', 32, 12, 'r12')
RegisterRegister([REGISTERS_64], REG_GPR, 'r13d', 32, 13, 'r13')
RegisterRegister([REGISTERS_64], REG_GPR, 'r14d', 32, 14, 'r14')
RegisterRegister([REGISTERS_64], REG_GPR, 'r15d', 32, 15, 'r15')
RegisterRegister([REGISTERS_64], REG_GPR, 'r8w',  16,  8, 'r8d')
RegisterRegister([REGISTERS_64], REG_GPR, 'r9w',  16,  9, 'r9d')
RegisterRegister([REGISTERS_64], REG_GPR, 'r10w', 16, 10, 'r10d')
RegisterRegister([REGISTERS_64], REG_GPR, 'r11w', 16, 11, 'r11d')
RegisterRegister([REGISTERS_64], REG_GPR, 'r12w', 16, 12, 'r12d')
RegisterRegister([REGISTERS_64], REG_GPR, 'r13w', 16, 13, 'r13d')
RegisterRegister([REGISTERS_64], REG_GPR, 'r14w', 16, 14, 'r14d')
RegisterRegister([REGISTERS_64], REG_GPR, 'r15w', 16, 15, 'r15d')
RegisterRegister([REGISTERS_64], REG_GPR, 'r8b',  8,  8, 'r8w')
RegisterRegister([REGISTERS_64], REG_GPR, 'r9b',  8,  9, 'r9w')
RegisterRegister([REGISTERS_64], REG_GPR, 'r10b', 8, 10, 'r10w')
RegisterRegister([REGISTERS_64], REG_GPR, 'r11b', 8, 11, 'r11w')
RegisterRegister([REGISTERS_64], REG_GPR, 'r12b', 8, 12, 'r12w')
RegisterRegister([REGISTERS_64], REG_GPR, 'r13b', 8, 13, 'r13w')
RegisterRegister([REGISTERS_64], REG_GPR, 'r14b', 8, 14, 'r14w')
RegisterRegister([REGISTERS_64], REG_GPR, 'r15b', 8, 15, 'r15w')
RegisterRegister([REGISTERS_64], REG_GPR, 'spl', 8, 4, 'sp')
RegisterRegister([REGISTERS_64], REG_GPR, 'bpl', 8, 5, 'bp')
RegisterRegister([REGISTERS_64], REG_GPR, 'sil', 8, 6, 'si')
RegisterRegister([REGISTERS_64], REG_GPR, 'dil', 8, 7, 'di')

RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_FP, 'st0', 80, 0)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_FP, 'st1', 80, 1)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_FP, 'st2', 80, 2)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_FP, 'st3', 80, 3)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_FP, 'st4', 80, 4)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_FP, 'st5', 80, 5)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_FP, 'st6', 80, 6)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_FP, 'st7', 80, 7)

RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_MMX, 'mm0', 64, 0)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_MMX, 'mm1', 64, 1)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_MMX, 'mm2', 64, 2)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_MMX, 'mm3', 64, 3)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_MMX, 'mm4', 64, 4)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_MMX, 'mm5', 64, 5)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_MMX, 'mm6', 64, 6)
RegisterRegister([REGISTERS_16, REGISTERS_32, REGISTERS_64], REG_MMX, 'mm7', 64, 7)

RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'ymm0', 256, 0)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'ymm1', 256, 1)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'ymm2', 256, 2)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'ymm3', 256, 3)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'ymm4', 256, 4)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'ymm5', 256, 5)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'ymm6', 256, 6)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'ymm7', 256, 7)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm8',  256,  8)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm9',  256,  9)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm10', 256, 10)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm11', 256, 11)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm12', 256, 12)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm13', 256, 13)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm14', 256, 14)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm15', 256, 15)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm15', 256, 15)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm16', 256, 16)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm17', 256, 17)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm18', 256, 18)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm19', 256, 19)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm20', 256, 20)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm21', 256, 21)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm22', 256, 22)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm23', 256, 23)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm24', 256, 24)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm25', 256, 25)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm26', 256, 26)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm27', 256, 27)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm28', 256, 28)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm29', 256, 29)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm30', 256, 30)
RegisterRegister([REGISTERS_64], REG_SSE, 'ymm31', 256, 31)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'xmm0', 128, 0, 'ymm0')
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'xmm1', 128, 1, 'ymm1')
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'xmm2', 128, 2, 'ymm2')
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'xmm3', 128, 3, 'ymm3')
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'xmm4', 128, 4, 'ymm4')
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'xmm5', 128, 5, 'ymm5')
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'xmm6', 128, 6, 'ymm6')
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE, 'xmm7', 128, 7, 'ymm7')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm8',  128,  8, 'ymm8')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm9',  128,  9, 'ymm9')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm10', 128, 10, 'ymm10')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm11', 128, 11, 'ymm11')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm12', 128, 12, 'ymm12')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm13', 128, 13, 'ymm13')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm14', 128, 14, 'ymm14')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm15', 128, 15, 'ymm15')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm16', 128, 16, 'ymm16')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm17', 128, 17, 'ymm17')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm18', 128, 18, 'ymm18')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm19', 128, 19, 'ymm19')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm20', 128, 20, 'ymm20')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm21', 128, 21, 'ymm21')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm22', 128, 22, 'ymm22')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm23', 128, 23, 'ymm23')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm24', 128, 24, 'ymm24')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm26', 128, 25, 'ymm25')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm25', 128, 26, 'ymm26')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm27', 128, 27, 'ymm27')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm28', 128, 28, 'ymm28')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm29', 128, 29, 'ymm29')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm30', 128, 30, 'ymm30')
RegisterRegister([REGISTERS_64], REG_SSE, 'xmm31', 128, 31, 'ymm31')

RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE_MASK, 'k0', 64, 0)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE_MASK, 'k1', 64, 1)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE_MASK, 'k2', 64, 2)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE_MASK, 'k3', 64, 3)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE_MASK, 'k4', 64, 4)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE_MASK, 'k5', 64, 5)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE_MASK, 'k6', 64, 6)
RegisterRegister([REGISTERS_32, REGISTERS_64], REG_SSE_MASK, 'k7', 64, 7)
