get_default_code_segment = myprew.get_default_code_segment
enum_nondefault_code_segments = myprew.enum_nondefault_code_segments
get_segment_start = myprew.get_segment_start
analyze_function_prologue = myprew.analyze_function_prologue
is_code_segment = myprew.is_code_segment

def enum_static_branching_from_default_to_other_code_segments(alignment_from=4, alignment_to=1):
  for ea, target_ea in enum_potional_static_branching_from_default_to_other_code_segments(alignment_from, alignment_to):
    yield ea, target_ea

def enum_potional_static_branching_from_default_to_other_code_segments(alignment_from=4, alignment_to=1):
  if alignment_from <= 0:
    raise ValueError('alignment_from')
  if alignment_to <= 0:
    raise ValueError('alignment_to')
  
  default_code_seg = get_default_code_segment()
  if default_code_seg.start_ea >= default_code_seg.end_ea:
    return
  nondefault_code_seg_starts = [segm.start_ea for segm in enum_nondefault_code_segments()]
  size = default_code_seg.end_ea - default_code_seg.start_ea
  
  for ea in nondefault_code_seg_starts:
    if ea <= default_code_seg.start_ea:
      raise Exception('The default code segment must be the first code segment!')
  
  # Note: we decrease by 1, because the last byte can never be a valid JMP/CALL
  for ea in range(default_code_seg.start_ea, default_code_seg.end_ea - 1):
    if ea % alignment_from != 0:
      continue
    b = idaapi.get_original_byte(ea)
    if b == 0xEB:
      d = idaapi.get_original_byte(ea + 1)
      if d < 0x80: # Note: this assumes that the Non-Default Code Segment is always before the others
        target_ea = ea + d + 2
        if target_ea % alignment_to == 0:
          target_seg_start = get_segment_start(target_ea)
          if target_seg_start != default_code_seg.start_ea:
            # Note: this is unlikely to happen, but let's double check
            if target_seg_start in nondefault_code_seg_starts:
              yield ea, target_ea
    elif (b == 0xE8 or b == 0xE9) and ea + 5 < default_code_seg.end_ea:
      d = idaapi.get_original_dword(ea + 1)
      if d < 0x80000000: # Note: this assumes that the Non-Default Code Segment is always before the others
        target_ea = ea + d + 5
        if target_ea % alignment_to == 0:
          target_seg_start = get_segment_start(target_ea)
          if target_seg_start in nondefault_code_seg_starts:
            yield ea, target_ea

i, j = 0, 0
for source_ea, target_ea in enum_potional_static_branching_from_default_to_other_code_segments():
  if source_ea % 16 != 0 and 1:
    continue
  if target_ea % 16 != 0 and 0:
    continue
  
  source_func = idaapi.get_func(source_ea)
  source_func_ea = source_func.start_ea if source_func != None else BADADDR
  source_item_ea = idaapi.get_item_head(source_ea)
  source_item_flags = idaapi.get_flags(source_item_ea) if source_item_ea != BADADDR else 0
  
  result = None
  if source_func_ea != BADADDR: # TODO: the function has got to have a real stack frame or has to have cross references
    if idaapi.is_code(source_item_flags) and source_item_ea != source_ea:
      result = 'Source is in known function code'
    if idaapi.is_data(source_item_flags) and (idaapi.get_name(source_item_ea) or '').startswith('jpt_'):
      result = 'Source is in known jump table'
  
  if not result:
    result = analyze_function_prologue(target_ea, is_executable_ea=is_code_segment)
  
  if result == 'Protected register destroyed':
    if idaapi.get_dword(target_ea) == 0x24748B48 and idaapi.get_dword(target_ea + 5) == 0x247C8B48 and idaapi.get_word(target_ea + 11) == 0xC483 and idaapi.get_word(target_ea + 14) == 0xC35D:
      result = 'Unwind'
  if result == 'Valid':
    if idaapi.get_word(target_ea) == 0x8B48 and idaapi.get_dword(target_ea + 8) == 0xE8E13148:
      result = 'Unwind'
    if idaapi.get_word(target_ea) == 0x8148 and idaapi.get_byte(target_ea + 7) == 0xC3:
      result = 'Unwind'
  if result == 'Valid':
    if source_ea % 16 == 0 and 1:
      ea_bytes = idaapi.get_bytes(target_ea, 0x100)
      try:
        func = list(myprew.decode_block(ea_bytes, None, target_ea))
        extra =  ', '.join([i.Text for i in func][:5])
      except Exception as ex:
        extra = str(ex)
      print(hex(source_ea), '->', hex(target_ea), result, myprew.get_func_cmt(source_ea, 0) if myprew.get_func_cmt(source_ea, 0) else myprew.get_func_cmt(source_ea, 1), extra)
    j += 1
  elif result == 'Unwind':
    pass
  elif result == '' or 'XXX' in result:
    print(hex(source_ea), '->', hex(target_ea), result)
  i += 1

print(j, '/', i)
