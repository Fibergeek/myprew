from .registers import Register, SegmentRegister

OPACTION_NONE   = 0
OPACTION_READ   = 1 << 0
OPACTION_WRITE  = 1 << 1 
OPACTION_CREAD  = 1 << 2
OPACTION_CWRITE = 1 << 3

OPVISIBILITY_HIDDEN   = 0
OPVISIBILITY_IMPLICIT = 1
OPVISIBILITY_EXPLICIT = 2

class Operand:
  def __init__(self, action, visibility):
    if visibility != OPVISIBILITY_HIDDEN and visibility != OPVISIBILITY_IMPLICIT and visibility != OPVISIBILITY_EXPLICIT:
      raise ValueError('Unsupported visibility!')
    self._flags = (visibility << 8) + action
  
  @property
  def Text(self):
    return ''
   
  @property
  def Bitness(self):
    return self.Size * 8
  
  @property
  def Size(self):
    return 0
  
  @property
  def IsMem(self):
    return False
  
  @property
  def IsReg(self):
    return False
  
  @property
  def IsImm(self):
    return False
  
  @property
  def Action(self):
    return self._flags & 0xFF
  
  @property
  def ActionText(self):
    action = self.Action
    if action == OPACTION_NONE:
      return ''
    if action == OPACTION_READ:
      return 'Read'
    if action == OPACTION_WRITE:
      return 'Write'
    if action == OPACTION_READ + OPACTION_WRITE:
      return 'ReadWrite'
    if action == OPACTION_READ + OPACTION_CREAD:
      return 'CondRead'
    if action == OPACTION_WRITE + OPACTION_CWRITE:
      return 'CondWrite'
    if action == OPACTION_READ + OPACTION_CWRITE + OPACTION_WRITE:
      return 'ReadContWrite'
    if action == OPACTION_WRITE + OPACTION_READ + OPACTION_CREAD:
      return 'WriteCondRead'
    raise NotImplementedError('Unknown action: {0}'.format(action))
  
  @property
  def Visibility(self):
    return self._flags >> 8
  
  @property
  def VisibilityText(self):
    visibility = self.Visibility
    if visibility == OPVISIBILITY_HIDDEN:
      return 'Hidden'
    if visibility == OPVISIBILITY_IMPLICIT:
      return 'Implicit'
    if visibility == OPVISIBILITY_EXPLICIT:
      return 'Explicit'
    raise NotImplementedError('Unknown visibility: {0}'.format(visibility))

class NullOperand(Operand):
  def __init__(self):
    Operand.__init__(self, OPACTION_NONE, OPVISIBILITY_HIDDEN)
  
  @property
  def Text(self):
    return None

class RegisterOperand(Operand):
  def __init__(self, reg, action, visibility):
    if not isinstance(reg, Register):
      raise ValueError('A Register type is required!')
    Operand.__init__(self, action, visibility)
    self._reg = reg
  
  @property
  def Text(self):
    return self._reg.Name
  
  @property
  def Size(self):
    return self._reg.Size
  
  @property
  def Reg(self):
    return self._reg
  
  @property
  def IsReg(self):
    return True

class ImmediateOperand(Operand):
  def __init__(self, size, rawsize, value, action, visibility):
    Operand.__init__(self, action, visibility)
    self._value = value
    self._size = (rawsize << 16) + size
  
  @property
  def Text(self):
    return hex(self.RawValue)
  
  @property
  def Size(self):
    return self._size & 0xFFFF
  
  @property
  def Value(self):
    return self._value
  
  @property
  def RawSize(self):
    return self._size >> 16
  
  @property
  def RawValue(self):
    return self._value & ((1 << self.RawSize) - 1)
  
  @property
  def IsImm(self):
    return True

class MemoryOperand(Operand):
  def __init__(self, size, segment_reg, base_reg, index_reg, scale, displacement, action, visibility):
    if not isinstance(segment_reg, SegmentRegister):
      raise ValueError('A Register type is required!')
    if base_reg != None and not isinstance(base_reg, Register):
      raise ValueError('A Register type is required!')
    if index_reg != None and not isinstance(index_reg, Register):
      raise ValueError('A Register type is required!')
    Operand.__init__(self, action, visibility)
    self._segment_reg = segment_reg
    self._base_reg = base_reg
    self._index_reg = index_reg
    self._scale = scale
    self._disp = displacement
    self._size = size
  
  @property
  def Text(self):
    return ''
  
  @property
  def Bitness(self):
    return self._size * 8
  
  @property
  def Size(self):
    return self._size
  
  @property
  def IsMem(self):
    return True
  
  @property
  def HasBaseReg(self):
    return self._base_reg != None
  
  @property
  def BaseReg(self):
    return self._base_reg
  
  @property
  def HasIndexReg(self):
    return self._index_reg != None
  
  @property
  def IndexReg(self):
    return self._index_reg
  
  @property
  def Disp(self):
    return self._disp
  
  @property
  def SegmentReg(self):
    return self._segment_reg

class Instruction:
  def __init__(self, address=0):
    self._addr = address
    self._bytes = None
    self._mnem = None
    self._text = None
    self._is_privileged = False
    self._length = 0
    self._operands = []
  
  @property
  def Bitness(self):
    return 64 # TODO: this should be platform dependant!
  
  @property
  def Addr(self):
    return self._addr
    
  @property
  def Bytes(self):
    return self._bytes
  
  @property
  def Mnem(self):
    return self._mnem
  
  @property
  def Text(self):
    return self._text
  
  @property
  def Length(self):
    return self._length
  
  @property
  def IsPrivileged(self):
    return self._is_privileged
  
  @property
  def Operands(self):
    return [op for op in self._operands]
  
  @property
  def TextOperands(self):
    return [op for op in self._operands if op.Text]
