Opt("WinTitleMatchMode", 2)

; We activate the x64dbg window
WinActivate('[TITLE:x64dbg;CLASS:Qt5QWindowIcon]')
WinWaitActive('[TITLE:x64dbg;CLASS:Qt5QWindowIcon]')
Sleep(100)

; Let's go to the Log tab
Send("!l")
Sleep(200)

; We clear the log
MouseClick( 'left', 15, 600, 1, 1 )
Send('^{ENTER}')
Send('ClearLog{ENTER}')
Sleep(500)
Send('ScriptLoad c:\temp\scripttest.txt{ENTER}')

; We run the EXE until the breakpoint
Send('!{F2}')
Sleep(500)
Send('{F9}')
Sleep(500)

; We load our script
Send("!s")
Sleep(200)
Send('^{ENTER}')
Sleep(100)

; We run the script
Send('+{TAB}{SPACE}')
