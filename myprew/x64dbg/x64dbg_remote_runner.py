"""
This script contains a single function which can run a x64dbg script on a remote system using dispy.

Requirements:
  Python installed in c:\Python39
  Modules installed : autoit, dispy, psutil
  x64dbg installed in c:\Temp\x64
  Plugins required : dbgexit, setmemrights, system
  Files that will be edited by this script:
    c:\Temp\*.dump
    c:\Temp\script.txt
    c:\Temp\x64\x64dbg.ini

  On the remote, run dispy like this:
     \Python39\python.exe \Python39\Scripts\dispynode.py -d -i 192.168.168.XXX -c 1 -p 3323 --zombie_interval=10

  Also usefull:
     python test.py | wsl grep -e 'Registers:' -e 'RAX:' -e 'R8:' -e 'CAPTURED INSTRUCTION' -e 'CAPTURED MEMORY'
"""

PARALLELISM = ['192.168.168.*']

def execute_x64dbg_script(args):
 SCRIPT_LOCATION = r'C:\Temp\script.txt'
 
 def configure_ini_file():
  from configparser import ConfigParser
  
  INI_FILE_PATH = r'c:\Temp\x64\x64dbg.ini'
  
  try:
    parser = ConfigParser()
    parser.optionxform = str
    parser.read(INI_FILE_PATH)
    parser.set('Engine', 'InitializeScript', SCRIPT_LOCATION)
    parser.set('Engine', 'NoScriptTimeout', '1')
    parser.set('Misc', 'Utf16LogRedirect', '1')
    parser.set('Shortcuts', 'ActionRedirectLog', 'Alt+J')
    parser.set('Exceptions', 'IgnoreRange', '')
    parser.set('Events', 'AttachBreakpoint', '0')
    parser.set('Events', 'DebugStrings', '0')
    parser.set('Events', 'DllEntry', '0')
    parser.set('Events', 'DllEntrySystem', '0')
    parser.set('Events', 'DllLoad', '0')
    parser.set('Events', 'DllLoadSystem', '0')
    parser.set('Events', 'DllUnload', '0')
    parser.set('Events', 'DllUnloadSystem', '0')
    parser.set('Events', 'EntryBreakpoint', '0')
    parser.set('Events', 'NtTerminateProcess', '0')
    parser.set('Events', 'SystemBreakpoint', '1')
    parser.set('Events', 'ThreadEnd', '0')
    parser.set('Events', 'ThreadEntry', '0')
    parser.set('Events', 'ThreadStart', '0')
    parser.set('Events', 'TlsCallbacks', '0')
    parser.set('Events', 'TlsCallbacksSystem', '0')
  finally:
    with open(INI_FILE_PATH, 'wt') as configfile:
      parser.write(configfile)
 
 def create_python_script():
  content = """
def redirect_x64dbg_log():
  import autoit
  from time import sleep
  
  autoit.win_activate('x64dbg')
  sleep(0.3)
  autoit.send('!l')
  sleep(0.3)
  autoit.mouse_click('right', 30, 200)
  sleep(0.1)
  autoit.send('!j')
  sleep(0.3)
  autoit.send('^a')
  autoit.send(r'C:/Temp/x64dbg.log')
  autoit.send('{TAB}')
  autoit.send('!o')

redirect_x64dbg_log()
  """
  
  with open(r'c:\Temp\Redirect_Log.py', 'wt') as f:
    f.write(content)
 
 def create_x64dbg_script(content):
  with open(SCRIPT_LOCATION, 'wt') as f:
    f.write('system "start "" \python39\pythonw.exe c:/Temp/Redirect_Log.py"\n')
    f.write('zzz .3000\n')
    f.write('\n')
    f.write(content)
 
 def delete_dump_files():
  from glob import glob
  import os
  
  for f in glob(r'c:\Temp\*.dump'):
    try:
      os.remove(f) 
    except:
      pass
  
  if os.path.isfile(r'C:\Temp\x64dbg.log'):
    os.remove(r'C:\Temp\x64dbg.log')
 
 def get_dump_files():
  from glob import glob
  import os
  
  for fn in glob(r'c:\Temp\*.dump'):
    try:
      with open(fn, 'rb') as f:
        yield fn[8:-5], f.read()
    except:
      pass 
  
  if os.path.isfile(r'C:\Temp\x64dbg.log'):
    with open(r'C:\Temp\x64dbg.log', 'rb') as f:
      yield 'x64dbg-log', f.read()[2:].decode('utf-16').replace('\r', '')
 
 def kill_x64dbg():
  import psutil
  import signal
  
  def find_procs_by_name(name):
    for p in psutil.process_iter(['name']):
      if p.info['name'] == name:
        yield p
  
  processes = list(find_procs_by_name('x64dbg.exe'))
  for p in processes:
    p.send_signal(signal.SIGTERM)
  
  if processes:
    from time import sleep
    sleep(3)
 
 def run_x64dbg_on_target(target):
  from os import system
  from time import sleep
  
  system(r'start "" c:\Temp\x64\x64dbg.exe "{}"'.format(target))
  sleep(3)
 
 def keep_running_until_x64dbg_exists(timeout_in_seconds=120):  
  def find_procs_by_name(name):
    import psutil
    for p in psutil.process_iter(['name']):
      if p.info['name'] == name:
        yield p
  
  from time import sleep
  
  for i in range(int(timeout_in_seconds)):
    processes = list(find_procs_by_name('x64dbg.exe'))
    if not processes:
      break
    
    sleep(1)
 
 try:
   target, script = args
   kill_x64dbg()
   delete_dump_files()
   configure_ini_file()
   create_python_script()
   create_x64dbg_script(script)
   run_x64dbg_on_target(target)
   keep_running_until_x64dbg_exists()
   kill_x64dbg()
   return list(get_dump_files())
 except Exception as ex:
   return ex

def run_remote_x64dbg_script(args):
  yield from run_remote_x64dbg_script_internal(args)
  return
  import multiprocessing as mp

  #mp.set_start_method('spawn')

  q = mp.Queue()
  p = mp.Process(target=run_remote_x64dbg_script_helper, args=(q, args))
  p.start()
  p.join()
  return q.get()

def run_remote_x64dbg_script_helper(q, args):
  results = run_remote_x64dbg_script_internal(args)
  q.put(results)

def run_remote_x64dbg_script_internal(args):
  import dispy
  from time import sleep
  
  RETRIES = 5
  
  cluster = None
  try:
    for retry_counter in range(RETRIES):
      if cluster == None:
        cluster = dispy.JobCluster(execute_x64dbg_script, loglevel=dispy.logger.DEBUG, nodes=PARALLELISM, dispy_port=3323)
      job = cluster.submit(args)
      for i in range(120):
        cluster.wait(1)
        if job.status in [dispy.DispyJob.Running, dispy.DispyJob.Cancelled, dispy.DispyJob.Terminated, dispy.DispyJob.Finished]:
          result = job()
          break
      else:
        print('Cancelling after 2 minutes...')
        cluster.cancel(job)
        if retry_counter == 1:
          print('Recreating cluster...')
          cluster.close(timeout=5, terminate=True)
          print('Old cluster was closed...')
          cluster.shutdown()
          print('Old cluster was shutdowned...')
          cluster = None
        result = None
      if result != None:
        if isinstance(result, Exception):
          yield args, result
        else:
          yield args, dict(result)
        break
      else:
        cluster.cancel(job)
        print('Retrying...')
        sleep(3)
  except Exception as ex:
    yield args, ex
  finally:
    if cluster != None:
      print('Closing cluster...')
      cluster.close(timeout=5, terminate=True)
      print('Shutting down cluster...')
      #cluster.shutdown()
      #print('Cluster was shutdowned...')
