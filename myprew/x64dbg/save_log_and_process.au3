Opt("WinTitleMatchMode", 2)

; We activate the x64dbg window
WinActivate('[TITLE:x64dbg;CLASS:Qt5QWindowIcon]')
WinWaitActive('[TITLE:x64dbg;CLASS:Qt5QWindowIcon]')
Sleep(100)

; Let's go to the Log tab
Send("!l")
Sleep(200)

; We save the log
MouseClick( 'menu', 10, 500, 1, 2 )
Sleep(100)
MouseClick( 'left', 15, 600, 1, 1 )
Sleep(100)
