def run_nasm(code, exe_path=None, ram_drive=None):
  from subprocess import Popen, PIPE
  from os import unlink, path
  import tempfile
  
  if not exe_path:
    dirname = path.dirname(path.realpath(__file__))
    exe_path = path.join(dirname, 'nasm.exe')
    if not path.isfile(exe_path):
      exe_path = 'nasm.exe'
  
  with tempfile.NamedTemporaryFile(mode='wt', delete=False, dir=ram_drive, suffix='.asm') as f:
    f.write(code)
  with tempfile.NamedTemporaryFile(mode='wt', delete=False, dir=ram_drive, suffix='.lst') as lst:
    pass
  with tempfile.NamedTemporaryFile(mode='wt', delete=False, dir=ram_drive, suffix='.bin') as bin:
    pass
  
  try:
    p = Popen(exe_path + ' ' + ' '.join( ['--keep-all', '-l' + lst.name, '-Ox', '-o' + bin.name, f.name]), shell=True, stdin=None, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    if stderr:
      try:
        raise Exception(stderr.decode('ascii'))
      except UnicodeDecodeError as ex:
        raise Exception(repr(stderr))
    elif stdout.strip():
      try:
        raise Exception(stdout.decode('ascii'))
      except UnicodeDecodeError as ex:
        raise Exception(repr(stdout))
    else:
      output = read_output_file(bin.name)
      labels = list(handle_listing_file(lst.name))
      return output, labels
  finally:
    unlink(lst.name)
    unlink(bin.name)
    unlink(f.name)

def read_output_file(fn):
  with open(fn, 'rb') as f:
    return f.read()

def handle_listing_file(file):
    #01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678
    #33                              <1>  %endif
    #40 000001B6 662E0F1F8400000000- <1>  ;;;  db 0x66, 0x2e, 0x0f, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
    #40 000001BF 00                  <1>
    #51                                  loc_4222233:
    #52 00000010 65488B042500000000      mov rax, [abs gs:0]              ; mov rax, [moffset]
    #53 00000019 48A1-                   mov rax, [abs qword data_sssss]  ; mov rax, [moffset]
    #53 0000001B [0000000000000000]
    base_offset, file_offset = 0, 0
    with open(file, 'rt') as g:
      for l in g.readlines():
        l = l.strip()
        if ' ' in l:
          _, _, l = l.partition(' ')
        if not l.strip():
          continue
        if l[0] == ' ':
          label, memory_offset = extract_label(l, base_offset + file_offset)
          if label == 'start':
            base_offset = memory_offset
          if label:
            yield label, memory_offset, file_offset
          continue
        elif l[0] != '0':
          continue
        
        l = l[:29].strip().replace('-', '').replace('[', '').replace(']', '').replace('(', '').replace(')', '')
        new_file_offset, _, l = l.partition(' ')
        new_file_offset = int(new_file_offset, 16)
        if len(l) % 2 or ' ' in l:
          raise Exception('Unexpected listing data! Custom verification is required to fix this script! :-(')
        if new_file_offset != file_offset:
          raise Exception('Offset mismatch! Custom verification is required to fix this script! :-( ' + hex(new_file_offset) + ' != ' + hex(file_offset))
        file_offset = new_file_offset + (len(l) >> 1)

def extract_label(l, memory_offset):
  l = l[34:]
  if l[0] != ' ':
    return '', -1
  if ';' in l:
    l, _, _ = l.partition(';')
  l = l.strip()
  if not l or l.startswith('%'):
    return '', -1
  if l.endswith(':'):
    name = l[:-1]
    return name.strip(), memory_offset
  ll = l.lower() # lower-case is for comparison only!
  if ll.startswith('labelat '):
    name, _, addr = l[8:].partition(',')
    addr = addr.strip()
    if addr.startswith('0x'):
      addr = int(addr[2:], 16)
    else:
      addr = int(addr, 10)
    return name.strip(), addr
  if ll.startswith('label '):
    name = l[6:]
    return name.strip(), memory_offset
  return '', -1

def assemble_x64(asm_code, addr=0, exe_path=None, ram_drive=None):
  return run_nasm(with_template_x64(asm_code, addr), exe_path, ram_drive)

def with_template_x64(code, addr=0):
  return """
BITS 64 
DEFAULT REL

%define nop1  nop                                                     ; just a nop, included for completeness
%define nop2  db 0x66, 0x90                                           ; 66 NOP
%define nop3  db 0x0F, 0x1F, 0x00                                     ;    NOP DWORD ptr [EAX]
%define nop4  db 0x0F, 0x1F, 0x40, 0x00                               ;    NOP DWORD ptr [EAX + 00H]
%define nop5  db 0x0F, 0x1F, 0x44, 0x00, 0x00                         ;    NOP DWORD ptr [EAX + EAX*1 + 00H]
%define nop6  db 0x66, 0x0F, 0x1F, 0x44, 0x00, 0x00                   ; 66 NOP DWORD ptr [EAX + EAX*1 + 00H]
%define nop7  db 0x0F, 0x1F, 0x80, 0x00, 0x00, 0x00, 0x00             ;    NOP DWORD ptr [EAX + 00000000H]
%define nop8  db 0x0F, 0x1F, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00       ;    NOP DWORD ptr [EAX + EAX*1 + 00000000H]
%define nop9  db 0x66, 0x0F, 0x1F, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00 ; 66 NOP DWORD ptr [EAX + EAX*1 + 00000000H]
%define nop10 db 0x66, 0x2e, 0x0f, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00                               ; data16; nopw %cs:0L(%[re]ax,%[re]ax,1)
%define nop11 db 0x66, 0x66, 0x2e, 0x0f, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00                         ; data16; nopw %cs:0L(%[re]ax,%[re]ax,1)
%define nop12 db 0x66, 0x66, 0x66, 0x2e, 0x0f, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00                   ; data16; data16; nopw %cs:0L(%[re]ax,%[re]ax,1)
%define nop13 db 0x66, 0x66, 0x66, 0x66, 0x2e, 0x0f, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00             ; data16; data16; data16; nopw %cs:0L(%[re]ax,%[re]ax,1)
%define nop14 db 0x66, 0x66, 0x66, 0x66, 0x66, 0x2e, 0x0f, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00       ; data16; data16; data16; data16; nopw %cs:0L(%[re]ax,%[re]ax,1)
%define nop15 db 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x2e, 0x0f, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00 ; data16; data16; data16; data16; data16; nopw %cs:0L(%[re]ax,%[re]ax,1)

%macro nopalign 1
  %if %1 >= 2
    %if __PASS__ == 0 || __PASS__ == 3
      times (((%1) - (($-$$) % (%1))) % (%1)) nop
    %else
      %push
        %assign %$pad (((%1) - (($-$$) % (%1))) % (%1))
        %if %$pad > 0
          nop%$pad
        %endif
      %pop
    %endif
  %endif
%endmacro
%macro nopalign 0
  msvcalign 16
%endmacro

%macro labelat 2
section %1 align=1 vstart=%2
%1:
%endmacro

labelat start,0x{1:X}
{0}
  """.format(tasm2nasm(code), addr)

def tasm2nasm(code):
  """" NOTE: this function is not to be used by 16-bit applications"""
  code = code.replace("cs:[", "[")
  code = code.replace("ds:[", "[")
  code = code.replace("es:[", "[")
  code = code.replace("ss:[", "[")
  code = code.replace("gs:[", "[gs:")
  code = code.replace("fs:[", "[fs:")
  code = code.replace("[gs:abs qword", "[abs qword gs:")
  code = code.replace("[gs:abs ", "[abs gs:")
  code = code.replace("[offset ", "[")
  code = code.replace(" offset ", " ")
  code = code.replace("[0x", "[abs 0x")
  code = code.replace("prefetch byte ", "prefetch ")
  code = code.replace("prefetchw byte ", "prefetchw ")
  code = code.replace("prefetchnta byte ", "prefetchnta ")
  code = code.replace("prefetcht0 byte ", "prefetcht0 ")
  code = code.replace("prefetcht1 byte ", "prefetcht1 ")
  code = code.replace("prefetcht2 byte ", "prefetcht2 ")
  return code

def test_nasm(code=None, exe_path=None):
  default_code = """
push rbx
nop
lea rax, [data_sssss]
lea rax, [loc_4222233]
nopalign 16
loc_4222233:
mov rax, gs:[abs 0]              ; mov rax, [moffset]
mov rax, [abs qword data_sssss]  ; mov rax, [moffset]

labelat data_sssss,0x14400003344
"""
  try:
    output, labels = assemble_x64(code if code else default_code, addr=0x1400000000, exe_path=exe_path)
    from binascii import hexlify
    print('==>', hexlify(output).decode('ascii'))
    for label, mem_offset, f_offset in labels:
      print('-->', label, hex(mem_offset), hex(f_offset))
  except Exception as ex:
    print(ex)
