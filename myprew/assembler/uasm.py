def run_uasm(code, exe_path=None, ram_drive=None):
  from subprocess import Popen, PIPE
  from os import unlink
  import tempfile
  
  if not exe_path:
    exe_path = 'uasm64.exe'
  if not ram_drive:    
    input_file = '{0}:\\test.asm'.format(str(ram_drive)[0])
    temp_file  = '{0}:\\test.lst'.format(str(ram_drive)[0])
  else:
    input_file = 'test.asm'
    temp_file  = 'test.lst'
  
  with tempfile.NamedTemporaryFile(mode='wt', delete=False) as f:
    f.write(code)
  with tempfile.NamedTemporaryFile(mode='wt', delete=False, suffix='.lst') as lst:
    pass
  
  try:
    p = Popen(exe_path + ' ' + ' '.join( ['-bin', '-Fl=' + lst.name, f.name]), shell=True, stdin=None, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    if stderr:
      try:
        print(stderr.decode('ASCII'))
      except Exception as ex:
        print(stderr)
    elif not b', 0 warnings, 0 errors\r\n' in stdout:
      try:
        print(stdout.decode('ASCII'))
      except Exception as ex:
        print(stdout)
    else:
      handle_listing_file(lst.name)
  finally:
    unlink(f.name)
    unlink(lst.name)

def handle_listing_file(file):
    with open(file, 'rt') as g:
      for l in g.readlines():
        l = l.strip()
        if not l:
          continue
        if l.startswith('Binary Map:'):
          break
        print(l)

def run_uasm_x64(asm_code, addr=0, exe_path=None, ram_drive=None):
  code = """
.X64
X64 SEGMENT
ORG 0x{1:x}
{0}
X64 ENDS
  """.format(asm_code, addr)
  return run_uasm(code, exe_path, ram_drive)

code = """
_test:
  lea rax, gs:[_demo+0x1234]
  lea rax, gs:[rip+_demo+0x1234]
  lea rax, gs:[$+(_demo-_test)+0x1234]
  add dword ptr gs:[_demo+0x1234], 0x11223344
  add dword ptr gs:[rip+_demo+0x1234], 0x11223344
  add dword ptr gs:[$+(_demo-_test)+0x1234], 0x11223344
  add dword ptr gs:[rax*8+rbx+0x12345678], 0x11223344
  lock add dword ptr gs:[rax*8+rbx+0x12345678], 0x11223344
  jz _test

ORG 0x1400001000
_demo:
"""
run_uasm_x64(code, addr=0x1400000000, exe_path=r'd:\Tools\UASM\uasm64.exe')

