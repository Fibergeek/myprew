import eel
import webbrowser

import jfx_bridge_ida

bridge_original = jfx_bridge_ida.IDABridge(connect_to_port=19855, response_timeout=10)
bridge_original.remote_exec('import myprew')

bridge_target = jfx_bridge_ida.IDABridge(connect_to_port=19856, response_timeout=10)
bridge_target.remote_exec('import myprew')

@eel.expose
def say_hello_py(x):
    query = '[{"addr": addr, "name": get_name(addr)} for addr in myprew.get_workflow_funcs()]'
    data1 = bridge_original.remote_eval(query)
    data2 = bridge_target.remote_eval(query)
    return data1 + data2

if __name__ == '__main__':
    eel.init('ida_eel')

    eel.start('index.htm', mode=None, block=False)
    url = 'http://localhost:8000/'
    print('Running server on', url)
    webbrowser.open(url)

    try:
       while True:
           eel.sleep(1.0)
    except KeyboardInterrupt:
       pass
