import pydle
import asyncio
import os
import psutil

PREFIX = 'PLEASE PM ME FOR FUNCTION '
EXTRAFIX = ' WITH '

def main():
  args = parse_args()
  if not os.environ.get('TARGET', '').strip() or not os.path.isfile(os.environ['TARGET']):
    print('Unable to locate the target <...> (SET TARGET=) to run!')
    return 1
  if not os.environ.get('__PLUGIN_WINMAIN', '').strip():
    print('__PLUGIN_WINMAIN is not set!')
    return 2
  if not args.cmd.strip() or not os.path.isfile(args.cmd):
    print('Unable to locate the command <{0}> (--cmd) to run!'.format(args.cmd))
    return 3
  if not os.environ.get('__PLUGIN_IRCLOGTARGET', '').strip():
    print('WARNING! __PLUGIN_IRCLOGTARGET is not set!')
  
  return run_controller(args)

def parse_args():
  import argparse
  
  parser = argparse.ArgumentParser(description='Run a x64dbg IRC Controller.')
  parser.add_argument('--host',    type=str, default='localhost')
  parser.add_argument('--channel', type=str, default='#Control')
  parser.add_argument('--nick',    type=str, default='Controller')
  parser.add_argument('--cmd',     type=str, default='run_x64dbg.cmd')
  
  return parser.parse_args()

def run_controller(args):
  class MyOwnBot(pydle.Client):
    async def on_connect(self):
         await self.join(args.channel)
   
    async def on_join(self, channel, user):
         if user == self.nickname:
             print('Joined')
         if user.startswith('x64dbg_'):
             self.x64dbg_joined = True
             print('x64dbg joined')
    
    async def on_topic_change(self,channel,message,by):
         if message.startswith(PREFIX):
             self.x64dbg_joined = False
             topic = message[len(PREFIX):]
             if EXTRAFIX in topic:
                 topic, _, extra = topic.rpartition(EXTRAFIX)
             else:
                 extra = ''
             print('Running task', 'FUNCTION', topic)
             await self.set_topic(args.channel, 'WORKING ON ' + topic)
             env = {
                 **os.environ,
                 '__PLUGIN_WINMAINREDIRECTION': topic.split(' ')[0],
             }
             env.update(get_verification_commands_as_dictionary(extra))
             while True:
                proc = await asyncio.create_subprocess_shell(args.cmd, env=env)
                await asyncio.sleep(5)
                if proc.returncode == None:
                    if not self.x64dbg_joined:
                        print('x64dbg did not join! Retrying...')
                        kill_children(proc.pid)
                        continue
                break
    
    async def on_kill(self,channel,target,by,reason):
         if target == self.nickname:
             print('Killed')
             await self.disconnect()
    
    async def on_kick(self,channel,target,by,reason):
         if target == self.nickname:
             print('Kicked')
             await self.disconnect()
  
  client = MyOwnBot(args.nick, realname='x64dbg Controller')
  connect = client.connect(args.host, tls=False, reconnect=False)
  loop = asyncio.get_event_loop()
  asyncio.ensure_future(connect, loop=loop)
  loop.run_forever()
  return 0

def kill_children(parent_pid):
  parent = psutil.Process(parent_pid)
  for child in parent.children(recursive=True):  # or parent.children() for recursive=False
    child.kill()

def get_verification_commands_as_dictionary(extra):
  env = {}
  current_key, key_value = None, None
  lines = extra.replace(':', ':;').replace(':;[', ':[').split(';')
  for line in lines:
    #print(line)
    if line.upper() == 'START:':
      current_key, key_value = flush_key_into_env(env, 'VERIFICATION_ONSTART', current_key, key_value)
    elif line.upper() == 'EXIT:':
      current_key, key_value = flush_key_into_env(env, 'VERIFICATION_ONEXIT', current_key, key_value)
    elif line.endswith(':'):
      current_key, key_value = flush_key_into_env(env, 'VERIFICATION_ONEXEC_' + line[:-1], current_key, key_value)
    elif line:
      if key_value:
        key_value += ';'
      else:
        key_value = ''
      key_value += line
  flush_key_into_env(env, None, current_key, key_value)
  return env

def flush_key_into_env(env, new_key, old_key, value, default_key='VERIFICATION_ONSTART'):
  if value:
    if not old_key:
      old_key = default_key
    env[old_key] = value
  return new_key, ''

if __name__ == '__main__':
  exit(main())