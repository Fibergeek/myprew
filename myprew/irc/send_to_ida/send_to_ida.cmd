@echo off
IF "%1" EQU "" GOTO :end
IF "%2" EQU "" GOTO :go_1
IF "%3" NEQ "" GOTO :end

:go_2
WHERE python.exe
python.exe "%~DP0send_to_ida.py" %1 %2
GOTO :end

:go_1
WHERE python.exe
python.exe "%~DP0send_to_ida.py" RELOCATE_XREF %1
GOTO :end

:end