import sys
import jfx_bridge_ida
b = jfx_bridge_ida.IDABridge(connect_to_port=27238)

if sys.argv[1] == 'JUMP_TO':
  cmd = 'myprew.idc.jumpto({0})'.format(sys.argv[2])
  print(cmd)
  b.remote_exec(cmd)
elif sys.argv[1] == 'RELOCATE_XREF':
  cmd = 'myprew.ida.gui.retarget_similar_absolute_targets_in_function({0}, True, True)'.format(sys.argv[2])
  print(cmd)
  b.remote_exec('print(hex(get_screen_ea()), "-->", hex({0}))'.format(sys.argv[2]))
  b.remote_exec(cmd)
