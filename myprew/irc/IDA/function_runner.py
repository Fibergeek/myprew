import pydle
import asyncio
from time import sleep

def test(task):
  # Simple echo bot.
  class MyOwnBot(pydle.Client):
    async def on_connect(self):
         await self.join('#control')
   
    async def on_join(self, channel, user):
         if user == self.nickname:
             print('Joined')
             await self.set_topic('#control', task)
    
    async def on_kill(self,channel,target,by,reason):
         if target == self.nickname:
             print('Killed')
             await self.disconnect()
    
    async def on_kick(self,channel,target,by,reason):
         if target == self.nickname:
             print('Kicked')
             await self.disconnect()
    
    async def on_message(self, target, source, message):
         # don't respond to our own messages, as this leads to a positive feedback loop
         if source.startswith('x64dbg'):
            if message.startswith('Finished the WinMain script'):
              print('Exited') 
              await self.disconnect()
         if source.startswith('Controller'):
            if message == 'Done':
                pass
  
  client = MyOwnBot('IDA', realname='IDA Pro')
  connect = client.connect('localhost', tls=False, reconnect=False)
  loop = asyncio.get_event_loop()
  asyncio.ensure_future(connect, loop=loop)
  loop.run_forever()

def get_cleaned_commands(extra_commands):
      # Turn newlines into semicolons
      extra_commands = extra_commands.strip().replace('\n', ';')

      # We remove spaces at the start and end of each line
      extra_commands = ';'.join([l.strip() for l in extra_commands.split(';')])

      # Get rid of double semicolons and/or spaces
      extra_commands = extra_commands.replace(':;', ':')
      while ';;' in extra_commands or '  ' in extra_commands:
        extra_commands = extra_commands.replace(';;', ';').replace('  ', ' ')

      # A comma, followed by a space? We remove the space!
      extra_commands = extra_commands.replace(', ', ',')

      # Starting with START is optional, so we remove it
      if extra_commands.upper().startswith('START:'):
        extra_commands = extra_commands[6:]

      # Done
      return extra_commands

def auto_inspect_functions(all=False):
  count, funcs = 0, myprew.get_workflow_funcs() if all else myprew.get_copied_funcs()
  for func_ea in funcs:
    idc.jumpto(func_ea)
    auto_inspect_current_function()
    count += 1
    sleep(2)
  
  print('Done, inspected {0} {1}!'.format(count, 'function' if count == 1 else 'functions'))

def auto_inspect_current_function():
  cmt = get_func_cmt(get_screen_ea(), 0)
  if 'VERIFICATION.' in cmt:
    wrong_count = 0
    for i in range(1000):
      if 'VERIFICATION.{0}'.format(i) in cmt:
        inspect_current_function(i)
        wrong_count = 0
        continue
      elif wrong_count < 2:
        wrong_count += 1	  
        continue
      break
  elif 'VERIFICATION' in cmt:
    inspect_current_function()
  else:
    run_current_function('')
    run_current_function('RevokedAllocInto rcx')
    run_current_function('RevokedAllocInto rdx')
    run_current_function('RevokedAllocInto rcx\nRevokedAllocInto rdx')

def inspect_current_function(verification_number=0):
  cmt = myprew.get_func_cmt(get_screen_ea(), 0)
  if verification_number:
      verification_keyword = 'VERIFICATION.{0}:'.format(verification_number)
  else:
      verification_keyword = 'VERIFICATION:'
  if 'VERIFICATION.ALL:' in cmt:
    raise NotImplementedError('VERIFICATION.ALL')
  if not verification_keyword in cmt:
    if verification_number:
      print('ERROR: No {0} comment found.'.format(verification_keyword[:-1]))
      return
    print('NOTE: No {0} comment found.'.format(verification_keyword[:-1]))
    left, _, right = cmt, '', ''
  else:
    left, _, right = cmt.partition(verification_keyword)
  right = right.strip()
  if right.startswith('"""'):
     right = right[3:].split('"""')[0]
  else:
     right = right.split('\n')[0]

  run_current_function(right.replace('"""', ''))

def run_current_function(extra_commands=None):
  extra_commands = (extra_commands or '').strip()
  if '<SEGMENT:.data>' in extra_commands:
    right = right.replace('<SEGMENT:.data>', hex(myprew.get_segm_by_name('.data').start_ea))

  md5 = myprew.get_input_file_md5_as_str()
  ea = myprew.get_func_start_ea(get_screen_ea())
  if ea != BADADDR:
    if extra_commands:
      extra_commands = get_cleaned_commands(extra_commands)
      test('PLEASE PM ME FOR FUNCTION ' + hex(ea) + ' ON TARGET ' + md5 + ' WITH ' + extra_commands)
    else:
      test('PLEASE PM ME FOR FUNCTION ' + hex(ea) + ' ON TARGET ' + md5)
