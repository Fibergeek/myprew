from unicorn import *
from unicorn.x86_const import *

import idc

from ..ida.functions import get_func_pages, get_func_start_ea
from ..ida.memory import get_page_bytes, PAGE_SIZE

def add_funcs_to_emulation(mu, functions):
  for function in functions:
    add_func_to_emulation(mu, function)

def add_func_to_emulation(mu, function):
  all_pages = set()
  pages = get_func_pages(function)
  all_pages.update(pages)
  
  for page in all_pages:
    mu.mem_map(page, PAGE_SIZE)
    mu.mem_write(page, get_page_bytes(page))

def get_mock_value(base_value):
  return (1 << 62) + (base_value << 32)

def write_mocked_register(mu, reg_index):
  mock_value = get_mock_value(reg_index)
  mu.reg_write(reg_index, mock_value)

def write_mocked_GPRs(mu):
  write_mocked_register(mu, UC_X86_REG_RAX)
  write_mocked_register(mu, UC_X86_REG_RCX)
  write_mocked_register(mu, UC_X86_REG_RDX)
  write_mocked_register(mu, UC_X86_REG_RBX)
  write_mocked_register(mu, UC_X86_REG_RBP)
  write_mocked_register(mu, UC_X86_REG_RSP)
  write_mocked_register(mu, UC_X86_REG_RSI)
  write_mocked_register(mu, UC_X86_REG_RDI)
  write_mocked_register(mu, UC_X86_REG_R8)
  write_mocked_register(mu, UC_X86_REG_R9)
  write_mocked_register(mu, UC_X86_REG_R10)
  write_mocked_register(mu, UC_X86_REG_R11)
  write_mocked_register(mu, UC_X86_REG_R12)
  write_mocked_register(mu, UC_X86_REG_R13)
  write_mocked_register(mu, UC_X86_REG_R14)
  write_mocked_register(mu, UC_X86_REG_R15)

def hook_code(uc, addr, size, user_data):
    print(hex(addr), hex(uc.reg_read(UC_X86_REG_RSP)), size, repr(uc.mem_read(addr, size)))

    # If the address isn't in the list of valid addresses, we have to revoke the rights
    emulated_func_addresses = []
    if addr in emulated_func_addresses:
        user_data['revoked_prot'] = get_protection_of_address(uc, addr)
        uc.mem_protect(addr & ~(PAGE_SIZE - 1), PAGE_SIZE, UC_PROT_NONE)

    idc.add_bpt(addr)

def get_protection_of_address(uc, address):
  for start, end, prot in uc.mem_regions():
    if address >= start and address < end:
      return prot
  return None

def clear_breakpoints():
  while idc.get_bpt_qty():
    idc.del_bpt(idc.get_bpt_ea(0))

def hook_mem_invalid(uc, access, address, size, value, user_data):
        """For Debugging Use Only"""
        cip = uc.reg_read(UC_X86_REG_RIP)
        user_data['mem_invalid'] = cip, access, address, size, value
        if access == UC_MEM_WRITE:
            print("invalid WRITE of 0x%x at 0x%X, data size = %u, data value = 0x%x" % (address, cip, size, value))
        if access == UC_MEM_READ:
            print("invalid READ of 0x%x at 0x%X, data size = %u" % (address, cip, size))
        if access == UC_MEM_FETCH:
            print("UC_MEM_FETCH of 0x%x at 0x%X, data size = %u" % (address, cip, size))
        if access == UC_MEM_READ_UNMAPPED:
            print("UC_MEM_READ_UNMAPPED of 0x%x at 0x%X, data size = %u" % (address, cip, size))
        if access == UC_MEM_WRITE_UNMAPPED:
            print("UC_MEM_WRITE_UNMAPPED of 0x%x at 0x%X, data size = %u" % (address, cip, size))
        if access == UC_MEM_FETCH_UNMAPPED:
            print("UC_MEM_FETCH_UNMAPPED of 0x%x at 0x%X, data size = %u" % (address, cip, size))
        if access == UC_MEM_WRITE_PROT:
            print("UC_MEM_WRITE_PROT of 0x%x at 0x%X, data size = %u" % (address, cip, size))
        if access == UC_MEM_FETCH_PROT:
            print("UC_MEM_FETCH_PROT of 0x%x at 0x%X, data size = %u" % (address, cip, size))
        if access == UC_MEM_FETCH_PROT:
            print("UC_MEM_FETCH_PROT of 0x%x at 0x%X, data size = %u" % (address, cip, size))
        if access == UC_MEM_READ_AFTER:
            print("UC_MEM_READ_AFTER of 0x%x at 0x%X, data size = %u" % (address, cip, size))
        return False

def hook_mem_access(uc, access, address, size, value, extra):
    for page_name, page_addr in extra['vars'].items():
      if address >= page_addr and address < page_addr + 0x10000:
         break
    else:
        raise NotImplementedError('Memory address not found! : ' + hex(address))

    rel_addr = address - page_addr 

    if access == UC_MEM_WRITE:
        print(">>> %s[0x%X] >>> Memory is being WRITE at 0x%x, data size = %u, data value = 0x%x" % (page_name, rel_addr, address, size, value))
    else:   # READ
        print(">>> %s[0x%X] >>> Memory is being READ at 0x%x, data size = %u" % (page_name, rel_addr, address, size))

class MockMemoryAllocator:
  def __init__(self, uc):
    self.base = 0x800000
    self.uc = uc  

  def allocate_page(self, page_count=1):
    page_base = self.base
    self.base += 0x100000
    self.uc.mem_map(page_base, PAGE_SIZE * page_count)
    return page_base

import idc

from ..func_input_mocker import func_input_mocker

def run_emulator(definitions=None):
  if definitions == None:
      definitions = []
  while run_emulator_using_definitions(definitions):
      pass
  return definitions

import struct

def fill_memory(mu, addr, size):
  i = 0
  while i < size:
    abs_addr = addr + i
    data = struct.pack('<Q', get_mock_value((addr + i)))
    mu.mem_write(abs_addr, data)
    i += len(data)

def write_B(mu, addr, value):
  data = struct.pack('<B', value)
  mu.mem_write(addr, data)

def write_W(mu, addr, value):
  data = struct.pack('<H', value)
  mu.mem_write(addr, data)

def write_D(mu, addr, value):
  data = struct.pack('<L', value)
  mu.mem_write(addr, data)

def write_Q(mu, addr, value):
  data = struct.pack('<Q', value)
  mu.mem_write(addr, data)

class unicorn_state_modifier:
  WRITES = {'B': write_B, 'W': write_W, 'D': write_D, 'Q': write_Q}

  @staticmethod
  def get_mocked_return_as_output(number, userdata):
    pass#return 'mov byte:[$rip_for_ret + {0}], 0xC3\nbp $rip_for_ret + {0}\n'.format(hex(int(number)))

  @staticmethod
  def get_allocation_as_output(name, extra):
    mu = extra['mu']
    last_alloc = extra['allocator'].allocate_page()
    extra['vars'][name] = last_alloc
    fill_memory(mu, last_alloc, PAGE_SIZE)

  @staticmethod
  def get_assignment_as_output(target, source, userdata):
    mu = userdata['mu']

    if source.startswith('RET.'):
      number = source[4:]
      source = '$rip_for_ret + ' + hex(int(number))
      raise NotImplementedException('0')
    elif source == 'RET':
      source = userdata['ret_page']
      if not target.startswith('m'):
        raise NotImplementedException('1')
    elif source.startswith('0x'):
      source = int(source[2:], 16)
    elif source[0] in '0123456789':
      source = int(source)
    elif '.' in source:
        size = x64dbg_outputter.SIZES[source[-1]]
        source = size + '($' + source.split('.')[0] + ' + 0x' + source.split('.')[1] + ')'
        raise NotImplementedException('2')
    else:
      source = userdata['vars'][source]
    
    if target.startswith('m') or target.startswith('TLS.'):
      write_function = unicorn_state_modifier.WRITES[target[-1]]
      name = target.split('.')[0]
      base = userdata['vars'][name]
      rel = int(target.split('.')[1], 16)
      write_function(mu, base + rel, source)
      print('Writing', source, 'into', base + rel, '/', hex(source), 'into', hex(base + rel))
    else:
      if target == 'rcx':
         mu.reg_write(UC_X86_REG_RCX, source)
         print('Writing', hex(source), 'into', target)
      elif target == 'rdx':
         mu.reg_write(UC_X86_REG_RDX, source)
         print('Writing', hex(source), 'into', target)
      elif target == 'r8' or  target == 'r8':
         mu.reg_write(UC_X86_REG_R8, source)
         print('Writing', hex(source), 'into r8')
      elif target == 'r9d' or  target == 'r9':
         mu.reg_write(UC_X86_REG_R9, source)
         print('Writing', hex(source), 'into r9')
      else:
         #return 'mov {0}, {1}'.format(target, source)
         raise NotImplementedException('4')

  @staticmethod
  def get_memory_action_as_output(address, action, size, userdata):
    mu = userdata['mu']

    address = address & ~(PAGE_SIZE - 1)
    size = PAGE_SIZE

    if get_protection_of_address(mu, address) == None:
      mu.mem_map(address, size)
      userdata['vars']['PAGE.%X' % address] = address

def create_selector(idx, flags):
    to_ret = flags
    to_ret |= idx << 3
    return to_ret

def create_gdt_entry(base, limit, access, flags):
    to_ret = limit & 0xffff;
    to_ret |= (base & 0xffffff) << 16;
    to_ret |= (access & 0xff) << 40;
    to_ret |= ((limit >> 16) & 0xf) << 48;
    to_ret |= (flags & 0xff) << 52;
    to_ret |= ((base >> 24) & 0xff) << 56;
    return pack('Q',to_ret)

def write_gdt(uc, gdt, mem):
    for idx, value in enumerate(gdt):
        offset = idx * GDT_ENTRY_SIZE
        uc.mem_write(mem + offset, value)

F_GRANULARITY = 0x8	# If set block=4KiB otherwise block=1B
F_PROT_32 = 0x4		# Protected Mode 32 bit
F_LONG = 0x2		# Long Mode
F_AVAILABLE = 0x1 	# Free Use
A_PRESENT = 0x80	# Segment active
A_PRIV_3 = 0x60		# Ring 3 Privs
A_PRIV_2 = 0x40		# Ring 2 Privs
A_PRIV_1 = 0x20		# Ring 1 Privs
A_PRIV_0 = 0x0		# Ring 0 Privs
A_CODE = 0x10		# Code Segment
A_DATA = 0x10		# Data Segment
A_TSS = 0x0		# TSS
A_GATE = 0x0		# GATE
A_EXEC = 0x8		# Executable
A_DATA_WRITABLE = 0x2
A_CODE_READABLE = 0x2
A_DIR_CON_BIT = 0x4
S_GDT = 0x0		# Index points to GDT
S_LDT = 0x4		# Index points to LDT
S_PRIV_3 = 0x3		# Ring 3 Privs
S_PRIV_2 = 0x2		# Ring 2 Privs
S_PRIV_1 = 0x1		# Ring 1 Privs
S_PRIV_0 = 0x0		# Ring 0 Privs

GDT_ENTRY_SIZE = 0x8

from struct import pack

def run_emulator_using_definitions(definitions):
  func_start_ea = get_func_start_ea(idc.get_screen_ea())

  definition = func_input_mocker.parse_string('\n'.join(definitions))
  print('Doing test run with:\n  ' + '\n  '.join(definitions + ['run']))
  mu = Uc(UC_ARCH_X86, UC_MODE_64)
  allocator = MockMemoryAllocator(mu)
  ret_page = allocator.allocate_page()
  mu.mem_write(ret_page, b'\xC3')
  mu.mem_protect(ret_page, PAGE_SIZE, UC_PROT_EXEC)
  extra = {'definitions': definitions, 'ret_page': ret_page, 'vars': {}, 'mu': mu, 'allocator': allocator}
  write_mocked_GPRs(mu)
  add_funcs_to_emulation(mu, [func_start_ea])

  tls_page = allocator.allocate_page(1)
  fill_memory(mu, tls_page, PAGE_SIZE)
  tib_page = allocator.allocate_page(1)
  gdt_page = allocator.allocate_page(1)
  stack_page = allocator.allocate_page() 
  rsp = stack_page + PAGE_SIZE - 32 - 8
  write_Q(mu, rsp, get_mock_value(UC_X86_REG_RIP))
  mu.reg_write(UC_X86_REG_RSP, rsp)
  mu.reg_write(UC_X86_REG_RIP, func_start_ea)

  extra['vars']['TLS'] = tls_page
  extra['vars']['TIB'] = tib_page
  extra['vars']['GDT'] = gdt_page
  extra['vars']['Stack'] = stack_page

  uc=mu

  # Create the GDT entries
  gdt = [create_gdt_entry(0,0,0,0) for i in range(31)]
  gdt[15] = create_gdt_entry(tib_page, PAGE_SIZE, A_PRESENT | A_DATA | A_DATA_WRITABLE | A_PRIV_3 | A_DIR_CON_BIT, F_PROT_32)
  gdt[16] = create_gdt_entry(0, 0xfffff000 , A_PRESENT | A_DATA | A_DATA_WRITABLE | A_PRIV_3 | A_DIR_CON_BIT, F_PROT_32)  # Data Segment
  gdt[17] = create_gdt_entry(0, 0xfffff000 , A_PRESENT | A_CODE | A_CODE_READABLE | A_EXEC | A_PRIV_3 | A_DIR_CON_BIT, F_PROT_32)  # Code Segment
  gdt[18] = create_gdt_entry(0, 0xfffff000 , A_PRESENT | A_DATA | A_DATA_WRITABLE | A_PRIV_0 | A_DIR_CON_BIT, F_PROT_32)  # Stack Segment
  write_gdt(uc, gdt, gdt_page)

  # Fill the GDTR register
  uc.reg_write(UC_X86_REG_GDTR, (0, gdt_page, len(gdt)*GDT_ENTRY_SIZE-1, 0x0))

  # Set the selector
  selector = create_selector(15, S_GDT | S_PRIV_3)
  uc.reg_write(UC_X86_REG_GS, selector)
  selector = create_selector(16, S_GDT | S_PRIV_3)
  uc.reg_write(UC_X86_REG_DS, selector)
  selector = create_selector(17, S_GDT | S_PRIV_3)
  uc.reg_write(UC_X86_REG_CS, selector)
  selector = create_selector(18, S_GDT | S_PRIV_0)
  uc.reg_write(UC_X86_REG_SS, selector)

  write_Q(uc, tib_page + 0x58, tls_page)

  for consume_output in func_input_mocker.outputDefinitions(definition, unicorn_state_modifier, extra):
    pass
  
  #    mu.reg_write(UC_X86_REG_RCX, extra['vars'][ass.value])

  # Now that the state is configured, we implement the hooking
  mu.hook_add(UC_HOOK_CODE, hook_code, extra)
  mu.hook_add(UC_HOOK_MEM_READ + UC_HOOK_MEM_WRITE, hook_mem_access, extra)
  mu.hook_add(UC_HOOK_MEM_INVALID, hook_mem_invalid, extra)

  current_hash = hash(''.join(definitions))
  while run_emulation(mu, extra):
    pass

  print('-------')
  new_hash = hash(''.join(definitions))#hash(repr(definition.memories)+repr(definition.actions))

  return current_hash != new_hash

def is_tracked_value(value):
  return value & (1 << 62)

def get_tracked_value(value):
  if not is_tracked_value(value):
    return None
  return ((value & ~(1 << 62)) >> 32)

def get_nontracked_value(value):
  if not is_tracked_value(value):
    return value
  return value & ((1 << 32) - 1)

def get_free_mem_name(extra, rel_val=1):
   var_names = sorted([name for name in extra['vars'] if name.startswith('m')])
   if var_names:
     last_varname = var_names[-1]
     if last_varname == 'mZ':
       raise NotImplementedExcepton
     return 'm' + chr(ord(last_varname[-1]) + rel_val)
   return 'm' + chr(64 + rel_val)

def run_emulation(mu, extra):
  try:
    print('Starting emulation of {0}...'.format(hex(mu.reg_read(UC_X86_REG_RIP))))
    mu.emu_start(mu.reg_read(UC_X86_REG_RIP), 0)
  except UcError as ex:
    #print(extra)
    error_code = extra['mem_invalid'][1]
    error_address = extra['mem_invalid'][2]

    if error_code == UC_MEM_FETCH_UNMAPPED:
      if is_tracked_value(error_address):
        print('Trying to execute unknown function at ' + hex(extra['mem_invalid'][0]) + ', skipping...')
        del extra['mem_invalid']

        if get_tracked_value(error_address) == UC_X86_REG_RIP:
          print('Final Return RIP is reached! Cleanly exiting...')
          return False
        
        tracked_value = get_tracked_value(error_address)
        for page_name, page_addr in extra['vars'].items():
          if tracked_value >= page_addr and tracked_value < page_addr + 0x10000:
            break
        else:
          raise NotImplementedError('tracked_address not found!')

        tracked_rel_addr = tracked_value - page_addr 

        extra['definitions'] += [page_name + '.' + hex(tracked_rel_addr)[2:].upper() + '.Q = RET']
      
      mu.reg_write(UC_X86_REG_RIP, extra['ret_page'])
      return True
    if error_code in (UC_MEM_READ_UNMAPPED, UC_MEM_WRITE_UNMAPPED):
      if is_tracked_value(error_address ):
        non_tracked = hex(get_nontracked_value(error_address))
        print('tracked', hex(get_tracked_value(error_address)))
        print('nontracked', non_tracked)

        if get_tracked_value(error_address) == UC_X86_REG_RCX:
          #tracked_reg = get_tracked_register(extra['mem_invalid'][2])
          print('Trying to read using tracked register ' + hex(error_address ) + ' at ' + hex(extra['mem_invalid'][0]))
          del extra['mem_invalid']
          extra['definitions'] += ['rcx = ' + get_free_mem_name(extra)]
          #extra['definitions'] += [get_free_mem_name(extra, 1) + '.' + non_tracked[2:].upper() + '.Q = ?']
          return False
        elif get_tracked_value(error_address) == UC_X86_REG_RDX:
          #tracked_reg = get_tracked_register(error_address)
          print('Trying to read using tracked register ' + hex(extra['mem_invalid'][2]) + ' at ' + hex(extra['mem_invalid'][0]))
          del extra['mem_invalid']
          extra['definitions'] += ['rdx = ' + get_free_mem_name(extra)]
          #extra['definitions'] += [get_free_mem_name(extra, 1) + '.' + non_tracked[2:].upper() + '.Q = ?']
          return False
        else:
          tracked_value = get_tracked_value(extra['mem_invalid'][2])
          for page_name, page_addr in extra['vars'].items():
            if tracked_value >= page_addr and tracked_value < page_addr + 0x10000:
              break
          else:
            raise NotImplementedError('tracked_address not found!')

          tracked_rel_addr = tracked_value  - page_addr 

          #tracked_page = get_tracked_page(tracked_address)
          print('Trying to read using tracked address ' + hex(extra['mem_invalid'][2]) + ' at ' + hex(extra['mem_invalid'][0]))
          del extra['mem_invalid']
          extra['definitions'] += [page_name + '.' + hex(tracked_rel_addr)[2:].upper() + '.Q = ' + get_free_mem_name(extra)]
          return False
      else:
        for page_name, page_addr in extra['vars'].items():
          if error_address >= page_addr and error_address < page_addr + 0x10000:
            break
        else:
          extra['definitions'] += ['0x' + hex(error_address)[2:].upper() + ' += R']

        print('Trying to read from untracked address ' + hex(extra['mem_invalid'][2]) + ' at ' + hex(extra['mem_invalid'][0]))
        del extra['mem_invalid']
        mu.reg_write(UC_X86_REG_RIP, extra['ret_page'])
        return False
  
  print('Emulation stopped: RIP = ' + hex(mu.reg_read(UC_X86_REG_RIP)))
  return False

def generate_definitions(definitions=None):
  clear_breakpoints()
  cip = hex(get_func_start_ea(idc.get_screen_ea()))
  definitions = definitions or ''
  definitions = [s.strip() for s in definitions.replace('\n', ';').split(';') if s.strip()]
  definitions = run_emulator(definitions)
  definition = func_input_mocker.parse_string('\n'.join(definitions))
  print('Your definition one-liner:\n  ' + ';'.join(definitions))
  print('Your definitions:\n  ' + '\n  '.join(definitions))
  cip_hackish = 'mov $rip_under_test, ' + cip + '\n  \n  '
  print('Your x64dbg script:\n  ' + cip_hackish + '\n  '.join(func_input_mocker.output_as_x64dbg(definition).split('\n') + ['ret']))
  return definitions


#from myprew.emulator import emulator
#from imp import reload
#reload(myprew.emulator.emulator)

PAGE_SIZE = 4096

def page_start(addr):
    remainder = addr % PAGE_SIZE
    return addr - remainder

class BaseEmulator:
    def __init__(self, platform):
        if platform == 'x64':
            self.uc = Uc(UC_ARCH_X86, UC_MODE_64)
        elif platform == 'x32':
            self.uc = Uc(UC_ARCH_X86, UC_MODE_64)
        else:
            raise Exception('Platform not supported!')

class SingleInstructionEmulator(BaseEmulator):
    def __init__(self, platform):
        BaseEmulator.__init__(self, platform)
        self.uc.mem_map(0, PAGE_SIZE)
        self.uc.mem_protect(0, PAGE_SIZE, UC_PROT_EXEC)
    
    def emulate(self, instr):
        if isinstance(instr, bytes):
            raise Exception('Not yet implemented')
        # TODO: raise an Exception if the instruction is RIP-relative
        self.uc.mem_write(0, instr.Bytes)
        self.uc.emu_start(0, instr.Length, count=1)
