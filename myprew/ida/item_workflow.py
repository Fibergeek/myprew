import idaapi

from .utils import is_workflow_name

def set_item_prefix_to(item_ea, new_prefix):
  """
  Set the prefix of an item.
  If the item is unnamed, it will be given a default name.
  If the item name contains an underscore, the first part is replaced.
  If the item name does not contain an underscore, one is added.
  """ 
  name = idaapi.get_name(item_ea)
  if not name:
    raise NotImplementedError('No name at {0}? Local name maybe?'.format(hex(item_ea) if isinstance(item_ea, int) else item_ea))
    name = '%x' % item_ea
  if '_' in name:
    _, _, name = name.partition('_')
  new_name = new_prefix + '_' + name
  return idaapi.set_name(item_ea, new_name)

def is_safe_item_ea_related_to_workflow(item_ea):
  func_name = idaapi.get_func_name(item_ea)
  if not is_workflow_name(func_name):
    return True
  return func_name.startswith('RESTORED_') or func_name.startswith('VERIFIED_') or func_name.startswith('ORIGINAL_')
