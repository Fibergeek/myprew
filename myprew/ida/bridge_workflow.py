from collections import defaultdict

from .bridge import ensure_bridge_to_original, jump_to_bridged_item
from .item_analysis import update_item_as_instr_with_retargetted_absolute_target
from .xref_analysis import get_surrounding_distances_to_related_xref
from .function_workflow import get_original_item_ea
from .item_workflow import is_safe_item_ea_related_to_workflow
from .utils import get_absolute_ea_from_distances
from .item_coloring import mark_item_as_orange

from .function_workflow import get_workflow_funcs
from .function_grouping import get_funcs_by_max_size

from idc import get_func_cmt, set_func_cmt

import idautils

def jump_to_address_in_remote(item_ea):
  bridge = ensure_bridge_to_original()
  return jump_to_bridged_item(bridge, item_ea)

def jump_to_item_in_original(item_ea):
  original_ea = get_original_item_ea(item_ea)
  bridge = ensure_bridge_to_original()
  return jump_to_bridged_item(bridge, original_ea)

def get_matching_xrefs_to_related_xref(bridge, center_ea, range=6):
  most_likely_incorrect_xrefs = list(set([xref.to for xref in idautils.XrefsFrom(center_ea, 1)]))
  if not most_likely_incorrect_xrefs:
    return {}
  #print(list(map(hex, most_likely_incorrect_xrefs)))
  original_ea = get_original_item_ea(center_ea)
  #print('list(myprew.get_surrounding_distances_to_related_xref({0}, {1}))'.format(hex(original_ea), range))
  result = bridge.remote_eval('list(myprew.get_surrounding_distances_to_related_xref({0}, {1}))'.format(hex(original_ea), range))
  #print(result)
  counts_per_xref = defaultdict(lambda : 0)
  for ea in get_absolute_ea_from_distances(center_ea, result):
    if is_safe_item_ea_related_to_workflow(ea):
     potential_xrefs_to = list(set([xref.to for xref in idautils.XrefsFrom(ea, 1)]))
     #print(hex(ea), ea - center_ea, list(map(hex, potential_xrefs_to)))
     for xref in potential_xrefs_to:
      counts_per_xref[xref] += 1
  return counts_per_xref

def resolve_xref_using_original(bridge_to_original, item_ea):
  results = get_matching_xrefs_to_related_xref(bridge_to_original, item_ea)
  #print(dict([(hex(key), value) for key, value in results.items()]))
  results = sorted(results.items(), key=lambda kv: kv[1], reverse=True)
  if len(results) == 1:
    if results[0][1] > 3:
      update_item_as_instr_with_retargetted_absolute_target(results[0][0], item_ea)
      mark_item_as_orange(item_ea)
      return True
  else:
    pass
  return False

def set_SEE_ORIGINAL_comments_for_an_update(bridge_old):
   bridge_old.remote_exec('import myprew')
   md5 = bridge_old.remote_eval('myprew.get_input_file_md5_as_str()')
   
   old_funcs = bridge_old.remote_eval('myprew.get_workflow_funcs()')
   new_funcs = get_workflow_funcs()
   old_sizes_and_funcs = bridge_old.remote_eval('myprew.get_funcs_by_max_size({0})'.format(old_funcs))
   new_sizes_and_funcs = get_funcs_by_max_size(new_funcs)
   
   for max_size, new_funcs_for_size in sorted(new_sizes_and_funcs.items()):
     old_funcs_for_size = old_sizes_and_funcs.get(max_size, [])
     if len(new_funcs_for_size) == len(old_funcs_for_size):
       old_funcs_for_size = sorted(old_funcs_for_size)
       new_funcs_for_size = sorted(new_funcs_for_size)
       for new_ea, old_ea in zip(new_funcs_for_size, old_funcs_for_size):
         cur_cmt = get_func_cmt(new_ea, 0) or ''
         if not 'SEE ORIGINAL ' in cur_cmt and not 'DISTANCE ' in cur_cmt:
           cmt = 'SEE ORIGINAL {0}.0x{1:X}\nDISTANCE {2}0x{3:X}'.format(md5, old_ea, '+' if new_ea > old_ea else '-', abs(new_ea - old_ea))
           new_cmt = (cur_cmt + '\n' + cmt) if cur_cmt else (cmt)
           print(hex(new_ea), cmt.replace('\n', ' '))
           set_func_cmt(new_ea, new_cmt, 0)
     else:
       print('Unable to match the workflow functions with max size {0}'.format(max_size))


def set_SEE_ORIGINAL_comments_for_an_update(bridge_old):
   bridge_old.remote_exec('import myprew')
   md5 = bridge_old.remote_eval('myprew.get_input_file_md5_as_str()')
   
   old_funcs = bridge_old.remote_eval('myprew.get_workflow_funcs()')
   new_funcs = get_workflow_funcs()
   old_sizes_and_funcs = bridge_old.remote_eval('myprew.get_funcs_by_max_size({0})'.format(old_funcs))
   new_sizes_and_funcs = get_funcs_by_max_size(new_funcs)
   
   for max_size, new_funcs_for_size in sorted(new_sizes_and_funcs.items()):
     old_funcs_for_size = old_sizes_and_funcs.get(max_size, [])
     if len(new_funcs_for_size) == len(old_funcs_for_size):
       old_funcs_for_size = sorted(old_funcs_for_size)
       new_funcs_for_size = sorted(new_funcs_for_size)
       for new_ea, old_ea in zip(new_funcs_for_size, old_funcs_for_size):
         cur_cmt = get_func_cmt(new_ea, 0) or ''
         if not 'SEE ORIGINAL ' in cur_cmt and not 'DISTANCE ' in cur_cmt:
           cmt = 'SEE ORIGINAL {0}.0x{1:X}\nDISTANCE {2}0x{3:X}'.format(md5, old_ea, '+' if new_ea > old_ea else '-', abs(new_ea - old_ea))
           new_cmt = (cur_cmt + '\n' + cmt) if cur_cmt else (cmt)
           print(hex(new_ea), cmt.replace('\n', ' '))
           set_func_cmt(new_ea, new_cmt, 0)
     else:
       print('Unable to match the workflow functions with max size {0}'.format(max_size))

def copy_comments_for_an_update(bridge_old, print_ignored=False):
   bridge_old.remote_exec('import myprew')
   md5 = bridge_old.remote_eval('myprew.get_input_file_md5_as_str()')
   
   old_funcs = bridge_old.remote_eval('myprew.get_workflow_funcs()')
   new_funcs = get_workflow_funcs()
   old_sizes_and_funcs = bridge_old.remote_eval('myprew.get_funcs_by_max_size({0})'.format(old_funcs))
   new_sizes_and_funcs = get_funcs_by_max_size(new_funcs)
   
   for max_size, new_funcs_for_size in sorted(new_sizes_and_funcs.items()):
     old_funcs_for_size = old_sizes_and_funcs.get(max_size, [])
     if len(new_funcs_for_size) == len(old_funcs_for_size):
       old_funcs_for_size = sorted(old_funcs_for_size)
       new_funcs_for_size = sorted(new_funcs_for_size)
       for new_ea, old_ea in zip(new_funcs_for_size, old_funcs_for_size):
         if not get_func_cmt(new_ea, 0):
           old_cmt = bridge_old.remote_eval('get_func_cmt({0}, 0)'.format(hex(old_ea))) or ''
           print(hex(new_ea), 'copied from', hex(old_ea), 'distance:', hex(new_ea - old_ea))
           set_func_cmt(new_ea, old_cmt, 0)
         elif print_ignored:
           print(hex(new_ea), 'ignored')
     else:
       print('Unable to match the workflow functions with max size {0}'.format(max_size))
