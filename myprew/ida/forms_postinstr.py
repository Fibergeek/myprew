import myprew
import sip # If this import will not work, then the code will not work! Update your IDA/Plugin support!
from idaapi import PluginForm
from PyQt5 import QtCore, QtGui, QtWidgets

class CrippledPostInstructionsFormClass(PluginForm):
    def OnCreate(self, form):
        """
        Called when the widget is created
        """

        # Get parent widget
        self.parent = self.FormToPyQtWidget(form)
        self.PopulateForm()

    def PopulateForm(self):
        # Create layout
        layout = QtWidgets.QVBoxLayout()

        # create an empty list
        self.list = QtWidgets.QListWidget()
        self.list.currentItemChanged.connect(self.on_item_clicked)

        # Add a button which can be used to refresh the listbox
        self.button = QtWidgets.QPushButton("Refresh")
        self.button.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        self.button.clicked.connect(self.refresh_items)
        self.button.resize(150, 50)
        layout.addWidget(self.button)
        layout.addWidget(self.list)

        # populate the listbox
        self.add_items()

        # make our created layout the dialogs layout
        self.parent.setLayout(layout)

    def refresh_items(self):
        self.list.clear()
        self.add_items()

    def add_items(self):
        for c, func_ea in enumerate(myprew.get_crippled_funcs()):
            if c > 0:
              self.add_item('')
            
            item_ea = idc.next_head(func_ea)
            self.add_item(hex(func_ea) + ' ' + myprew.get_func_name(func_ea) + '  - ' + str(c + 1))
            i = 0
            while i < 3:
              item_ea = idc.next_head(item_ea)
              if myprew.is_item_code(item_ea):
                self.add_item('  ' + hex(item_ea)[2:] + ' ' + idc.GetDisasm(item_ea))
                i += 1

    def add_item(self, item):
        self.list.addItem(item)
    
    def on_item_clicked(self):
        if not self.list.currentItem():
          return
        
        if self.list.currentItem().text().startswith('  '):
          addr, _, name = self.list.currentItem().text().strip().partition(' ')
          jumpto(int(addr, 16))
        elif self.list.currentItem().text():
          addr, _, name = self.list.currentItem().text().partition(' ')
          jumpto(int(addr[2:], 16))

def show_next_of_crippled_funcs():
    plg = CrippledPostInstructionsFormClass()
    plg.Show("Crippled / Next Instructions")
