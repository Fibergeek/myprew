from io import BytesIO
from myprew.emulator import emulator

from idautils import DataRefsFrom, CodeRefsTo

class Failure(Exception):
  pass

def generate_deobfuscated_ida_function_as_instr(ea, is_protection_addr=None, print_blocks=False):
  for block in generate_deobfuscated_ida_function_as_blocks(ea, is_protection_addr=is_protection_addr, print_blocks=print_blocks):
    for instr in block:
      yield instr

def generate_deobfuscated_ida_function_as_blocks(ea, is_protection_addr=None, print_blocks=False):
  if isinstance(is_protection_addr, int):
    is_protection_addr = lambda ea: ea >= is_protection_addr
  
  previous_block = None
  for block in decode_ida_function_into_blocks(ea, print_blocks=print_blocks):
    if has_hidden_cpuid(block):
      to_emulate = list(extract_instructions_to_emulate_hidden_cpuid(block))
      if len(to_emulate) != 2:
        print('WARNING! Hidden-CPUID: Strange: {0} instructions instead of 2 in block at {1}'.format(len(to_emulate), hex(block[0].Addr)))
      new_instr = get_emulated_instr_value_of_hidden_cpuid(to_emulate)
      block = list(recreate_block_without_hidden_cpuid(block, to_emulate, new_instr))
    
    if is_protection_addr != None and has_ida_dref_to_protection(block, is_protection_addr):
      block = list(recreate_block_without_ida_dref_to_protection(block, is_protection_addr, previous_block))

    yield list(recreate_block_without_manual_removals(block))
    previous_block = block

def has_hidden_cpuid(block):
  return get_register_used_for_hidden_cpuid(block) != None

def get_register_used_for_hidden_cpuid(block):
  for instr in block:
    if 'C3A20F' in instr.Text.upper():
      if len(instr.Operands) == 2 and instr.Operands[0].IsReg:
	    # TODO: verify the instruction (mov and/or lea)
        return instr.Operands[0].Reg
      elif not is_instr_marked_for_removal(instr):
        print('WARNING! Hidden-CPUID: Ignoring {0} {1}'.format(hex(instr.Addr), instr.Text))
  return None

def extract_instructions_to_emulate_hidden_cpuid(block):
  reg = get_register_used_for_hidden_cpuid(block)
  if reg == None:
    return
  
  for instr in block:
    #print(instr.Text, reg.Name) # 
    if instr.Mnem == 'PUSH' or instr.Mnem == 'POP': # TODO: is this really always safe to ignore?
      continue
    if myprew.decoder.analysis.accesses_any_of_register([instr], reg):
      if not myprew.decoder.analysis.modifies_any_of_register([instr], reg):
        print('WARNING! Hidden-CPUID: Ignoring {0} {1} (Is the register being used?)'.format(hex(instr.Addr), instr.Text))
      elif instr.Mnem != 'MOV' and instr.Mnem != 'LEA':
        raise Failure('WARNING! Hidden-CPUID: Ignoring {0} {1} (unhandled instruction)'.format(hex(instr.Addr), instr.Text))
      else:
        yield instr

def get_emulated_instr_value_of_hidden_cpuid(instructions_to_emulate):
  new_addr = instructions_to_emulate[0].Addr
  new_value = get_emulated_target_value_of_hidden_cpuid(instructions_to_emulate)
  new_instr = 'mov ' + get_register_used_for_hidden_cpuid(instructions_to_emulate).Name + ', ' + hex(new_value)
  new_bytes = myprew.ida.assembler.asm_instruction(myprew.ida.assembler.create_ks_x86_64(), new_instr, new_addr)
  return myprew.decoder.zydis.decode_instruction(new_bytes, 'x64', new_addr)

def get_emulated_target_value_of_hidden_cpuid(instructions_to_emulate):
  reg = get_register_used_for_hidden_cpuid(instructions_to_emulate)
  if reg == None:
    raise Exception('Invalid usage')

  emu = emulator.SingleInstructionEmulator('x64')
  for instr in instructions_to_emulate:
    emu.emulate(instr)

  # PUT THE TARGET IN 'RAX'
  if reg.Size == 32:
    new_instr = 'mov eax, ' + reg.Name
  elif reg.Size == 64:
    new_instr = 'mov rax, ' + reg.Name
  else:
    new_instr = 'movzx rax, ' + reg.Name
  new_bytes = myprew.ida.assembler.asm_instruction(myprew.ida.assembler.create_ks_x86_64(), new_instr)
  new_instr = myprew.decoder.zydis.decode_instruction(new_bytes, 'x64')
  
  emu.emulate(new_instr)
  
  # TODO: check that the first instruction is full register write
  from unicorn.x86_const import UC_X86_REG_RAX
  return emu.uc.reg_read(UC_X86_REG_RAX)

def recreate_block_without_hidden_cpuid(block, instructions_to_remove, new_instr):
  ids_to_remove = [id(instr) for instr in instructions_to_remove]
  for instr in block:
    if id(instr) in ids_to_remove:
      print('Removing HCRI {0} {1}'.format(hex(instr.Addr), instr.Text))
      if id(instr) == ids_to_remove[0] and new_instr != None:
        print('Inserting HCOI {0} {1}'.format(hex(new_instr.Addr), new_instr.Text))
        yield new_instr
    else:
      yield instr

def recreate_block_without_ida_dref_to_protection(block, is_protection_addr, previous_block=None):
  ids_to_remove = [id(instr) for instr in extract_instr_with_ida_dref_to_protection(block, is_protection_addr)]
  for instr in block:      
    if not id(instr) in ids_to_remove:
      yield instr
      continue
    
    should_remove = False
    if is_instr_marked_for_removal(instr):
      # This is a Manually Verified Deleteable/Removalable Instruction
      should_remove = True
    else:
      if len(instr.Operands) >= 2 and (instr.Operands[0].IsMem or (instr.Mnem == 'XCHG' and instr.Operands[1].IsMem)):
        # A value is written to a Protection section, we should remove those writes
        should_remove = True
      elif previous_block != None:
        # If IDA has detected this instruction is not referenced by any instruction (including flow); we can safely remove it!
        if not [xref for xref in CodeRefsTo(instr.Addr, 1)]:
          should_remove = True
        elif previous_block[-1].Mnem == 'CALL':
          if instr.Mnem in ('SUB', 'ADD', 'AND', 'XOR', 'OR') and ('r8, ' in instr.Text or 'r8d, ' in instr.Text or 'r8w, ' in instr.Text or 'r8b, ' in instr.Text or 'r9, ' in instr.Text or 'r9d, ' in instr.Text or 'r9w, ' in instr.Text or 'r9b, ' in instr.Text or 'cx, ' in instr.Text or 'cl, ' in instr.Text or 'dx, ' in instr.Text or 'dl, ' in instr.Text):
            # TODO: we should really check the register isn't used in the succeeding code-flow
            # Once we implemented that we can also include MOV which loads constants
            should_remove = True
    
    if not should_remove:
      raise Failure('Unable to remove DRTP {0} {1}'.format(hex(instr.Addr), instr.Text))
    print('Removing DRTP {0} {1}'.format(hex(instr.Addr), instr.Text))

def recreate_block_without_manual_removals(block):
  for instr in block:
    if is_instr_marked_for_removal(instr):
      print('Removing MMFR {0} {1}'.format(hex(instr.Addr), instr.Text))
    else:
      yield instr

def is_instr_marked_for_removal(instr):
  return get_cmt(instr.Addr, 0) in ('MVDI', 'MVRI', 'MMFR', 'MMFD')

def has_ida_dref_to_protection(block, is_protection_addr):
  for instr in extract_instr_with_ida_dref_to_protection(block, is_protection_addr):
    return True
  return False

def extract_instr_with_ida_dref_to_protection(block, is_protection_addr):
  for instr in block:
    for dref_to in DataRefsFrom(instr.Addr):
      if is_protection_addr(dref_to):
        yield instr
        break

def decode_ida_function_into_blocks(ea, print_blocks=False):
  start_addr = myprew.get_func_start_ea(ea)
  next_addr = start_addr
  data = myprew.get_func_bytes(start_addr)
  while True:
    start_index = next_addr - start_addr
    block = list(myprew.decoder.zydis.decode_block(data[start_index:], 'x64', next_addr, is_ida_branch_target))
    if not block:
      break
    next_addr = block[-1].Addr + block[-1].Length
    if print_blocks:
      first_addr = block[0].Addr
      print('Block', '[', hex(first_addr), '-', hex(next_addr), '[')  
    yield block

def is_ida_branch_target(last_instr):
  # TODO: This function does not take jump tables into account and sadly takes false positives from other sections into account
  if last_instr == None:
    return False
  next_addr = last_instr.Addr + last_instr.Length
  coderefs = list(CodeRefsTo(next_addr, 0))
  if not coderefs:
    return False
  # TODO: filter the coderefs
  return True

#for instr in generate_deobfuscated_ida_function_as_instr(get_screen_ea(), print_blocks=False):
#   print(hex(instr.Addr), instr.Text)
