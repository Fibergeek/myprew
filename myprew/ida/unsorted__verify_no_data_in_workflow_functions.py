
for func_ea in myprew.get_workflow_funcs():
  for item_ea in myprew.get_func_items(func_ea):
    if not myprew.is_item_code(item_ea):
       print(hex(item_ea))
    elif not myprew.is_item_code(item_ea + myprew.get_item_size(item_ea)):
       disasm = GetDisasm(item_ea + myprew.get_item_size(item_ea))
       if not disasm.startswith('align '):
           print(hex(item_ea + myprew.get_item_size(item_ea)), disasm)
                                                 