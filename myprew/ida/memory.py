import idc

# TODO: if the target is 16-bit, we should set this to 16-bytes
PAGE_SIZE = 0x1000

get_bytes = idc.get_bytes

def get_page_bytes(ea):
  """ Return all bytes that are in the page of a specific address """
  ea = ea & ~(PAGE_SIZE - 1)
  return idc.get_bytes(ea, PAGE_SIZE)
