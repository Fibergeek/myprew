import idaapi
import binascii

def generate_disasm_line(ea, n):
    return idaapi.generate_disasm_line(ea, n)

def get_input_file_md5():
    return idaapi.retrieve_input_file_md5()

def get_input_file_md5_as_str():
    return binascii.hexlify(get_input_file_md5()).decode('ascii')

def get_length(iter):
    if '__len__' in dir(iter):
        return len(iter)

    l = 0
    for i in iter:
      l += 1
    return l

def get_absolute_ea_from_distances(center_ea, distances):
    for distance in distances:
        yield center_ea + distance

def is_workflow_name(name):
    """
    Returns True if the name contains a prefix related to our workflow.
    Otherwise False is returned.
    """
    if not isinstance(name, str):
        return False
    nsw = lambda prefix: name.startswith(prefix)
    return nsw('ORIGINAL_') or nsw('COPIED_') or nsw('CRIPPLED_') or nsw('ONGOING_') or nsw('RESTORED_') or nsw('VERIFIED_')

def get_name_as_addr(name):
  if not name:
    return idaapi.BADADDR
  name_ea = idaapi.get_name_ea(idaapi.BADADDR, name)
  if name_ea != idaapi.BADADDR:
    return name_ea
  addr_text = name
  if addr_text.startswith('0x'):
   addr_text = addr_text[1:]
  if addr_text.startswith('x'):
   addr_text = addr_text[1:]
  return int(addr_text, 16)
  
def search_list_in_list(searchee, to_search):
  if not isinstance(searchee, list) and not isinstance(searchee, tuple):
    searchee = list(searchee)
  if not isinstance(to_search, list):
    to_search = list(to_search)
  return [i for i in range(len(searchee)-len(to_search)+1) if searchee[i:i+len(to_search)] == to_search]
