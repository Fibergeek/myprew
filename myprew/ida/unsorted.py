def get_workflow_funcs_with_red():
  for func_ea in myprew.get_workflow_funcs():
    yield func_ea, myprew.get_red_func_items(func_ea)



def adjust_rel_addr(item_ea, rel):
  if item_ea == None:
    item_ea = get_screen_ea()
  target_ea = myprew.get_item_refered_absolute_target(item_ea)
  if target_ea != BADADDR:
    myprew.update_item_as_instr_with_retargetted_absolute_target(target_ea + rel, item_ea)
    myprew.mark_item_as_orange(item_ea)


for func_ea, items in get_workflow_funcs_with_red():
  bridge = myprew.ensure_bridge_to_original()
  items = list(items)
  c = len(items)
  for item_ea in items:
    original_ea = myprew.get_original_item_ea(item_ea)
    if original_ea != BADADDR:
      cmd = 'myprew.get_item_refered_absolute_target({0})'.format(original_ea)
      original_xref_ea = result = bridge.remote_eval(cmd)
      # TODO: fix this : myprew.idaapi.get_name
      cmd = 'myprew.idaapi.get_name({0})'.format(original_xref_ea)
      original_name_xref_ea = bridge.remote_eval(cmd)
      if original_name_xref_ea.startswith('a'):
        new_addr = myprew.idaapi.get_name_ea(BADADDR, original_name_xref_ea)
        if new_addr != BADADDR:
          print(hex(original_ea), hex(original_xref_ea), original_name_xref_ea, '-->', hex(new_addr))
          # TODO: the name it self should not be enough, we have to verify the constant for X bytes atleast
          myprew.update_item_as_instr_with_retargetted_absolute_target(new_addr, item_ea)

for func_ea, items in get_workflow_funcs_with_red():
  items = list(items)
  c = len(items)
  if c:
    print(hex(func_ea), c)





SHOW_OK = False
from itertools import chain
for func_ea in chain(myprew.get_verified_funcs(), myprew.get_restored_funcs()):
  bridge = myprew.ensure_bridge_to_original()  
  for item_ea in myprew.get_func_items(func_ea):
    target_ea = myprew.get_item_refered_absolute_target(item_ea)
    if target_ea != BADADDR and target_ea != None:
      local_name_xref_ea = myprew.idaapi.get_name(target_ea)
      if local_name_xref_ea.startswith('sub_') or local_name_xref_ea.startswith('FUNC_') or local_name_xref_ea.startswith('VERIFIED_') or local_name_xref_ea.startswith('RESTORED_') :
        #print('Researching', hex(item_ea), 'to get content of', local_name_xref_ea)
        original_ea = myprew.get_original_item_ea(item_ea)
        if original_ea != BADADDR and original_ea != None:
          cmd = 'myprew.get_item_refered_absolute_target({0})'.format(original_ea)
          original_xref_ea = result = bridge.remote_eval(cmd)
          if original_xref_ea != BADADDR and original_xref_ea != None:
            # TODO: fix this : myprew.idaapi.get_name
            cmd = 'myprew.idaapi.get_name({0})'.format(original_xref_ea)
            original_name_xref_ea = bridge.remote_eval(cmd)
            if original_name_xref_ea.startswith('sub_') or original_name_xref_ea.startswith('FUNC_') or original_name_xref_ea.startswith('VERIFIED_') or original_name_xref_ea.startswith('RESTORED_'):
              local_data = myprew.get_func_ascii(target_ea)
              cmd = 'myprew.get_func_ascii({0})'.format(original_xref_ea)
              original_data = bridge.remote_eval(cmd)
              if local_data != original_data:
                local_data = myprew.get_func_dotted_ascii(target_ea)
                cmd = 'myprew.get_func_dotted_ascii({0})'.format(original_xref_ea)
                original_data = bridge.remote_eval(cmd)
                if local_data != original_data:
                  print(hex(item_ea), ':', original_name_xref_ea, '->', local_name_xref_ea, ':', 'MISMATCH')
                elif SHOW_OK:
                  print(hex(item_ea), ':', original_name_xref_ea, '->', local_name_xref_ea, ':', 'ACCEPTABLE MATCH')
              elif SHOW_OK:
                  print(hex(item_ea), ':', original_name_xref_ea, '->', local_name_xref_ea, ':', 'EXACT MATCH')
            else:
              print(hex(item_ea), ':', original_name_xref_ea, '->', local_name_xref_ea, ':', 'NAME MISMATCH')



