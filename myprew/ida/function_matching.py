from idc import BADADDR, get_screen_ea

from .functions import get_funcs, get_func_start_ea, get_func_size
from .function_analysis import get_func_dotted_ascii, get_func_guessed_size

def get_matching_functions_by_dotted_ascii(ascii):
    return get_funcs(lambda func_ea: get_func_dotted_ascii(func_ea) == ascii)

def print_matching_functions_by_dotted_ascii(ascii):
   raise NotImplementedError

def get_surrounding_func_guessed_sizes(ea=None, count=3):
  return get_surrounding_func_sizes(ea, count, size_getter=get_func_guessed_size)

def get_surrounding_func_sizes(ea=None, count=3, size_getter=None):
  if ea == None:
    ea = get_screen_ea()
  func_ea = get_func_start_ea(ea)
  if func_ea == BADADDR:
    return [], []
  if size_getter == None:
    size_getter = get_func_size
 
  funcs = list(get_funcs())
  index = funcs.index(func_ea)
  if index == -1:
    return [], []

  sizes_left  = [size_getter(ea) for ea in funcs[index - count:index]]
  sizes_right = [size_getter(ea) for ea in funcs[index+1:index + count + 1]]
  return sizes_left, sizes_right

def print_with_surrounding_func_sizes(ea=None, count=3, printer=None):
  if printer == None:
    printer = print
  if ea == None:
    ea = get_screen_ea()
  
  left, right =  get_surrounding_func_sizes(ea, count)
  printer('{0} + [{2}] + {1}'.format(left, right, get_func_guessed_size(ea)))

def get_with_surrounding_func_guessed_sizes(ea=None, count=3):
  left, right =  get_surrounding_func_guessed_sizes(ea, count)
  return left + [get_func_guessed_size(ea)] + right

def print_crippled_funcs_with_surrounding_func_guessed_sizes():
  for func_ea in get_crippled_funcs():
    print(hex(func_ea), repr(get_with_surrounding_func_guessed_sizes(func_ea)).replace(', ', ','))

def print_with_surrounding_func_guessed_sizes(ea=None, count=3, printer=None):
  if printer == None:
    printer = print
  if ea == None:
    ea = get_screen_ea()
  
  left, right =  get_surrounding_func_guessed_sizes(ea, count)
  printer('{0} + [{2}] + {1}'.format(left, right, get_func_guessed_size(ea)))

def search_list_in_list(searchee, to_search):
  if not isinstance(searchee, list) and not isinstance(searchee, tuple):
    searchee = list(searchee)
  if not isinstance(to_search, list):
    to_search = list(to_search)
  return [i for i in range(len(searchee)-len(to_search)+1) if searchee[i:i+len(to_search)] == to_search]

def search_func_size_pattern(pattern, pattern_b=None, size_getter=None):
  if not isinstance(pattern, list):
    pattern = list(pattern)
  if size_getter == None:
    size_getter = get_func_size
  
  funcs = list(get_funcs())
  sizes = [size_getter(ea) for ea in funcs]
  for i in search_list_in_list(sizes, pattern):
    if pattern_b != None:
      print(hex(funcs[i]), '-->', hex(funcs[i+len(pattern)+1]), search_list_in_list(sizes[i+len(pattern):], pattern_b))
    else:
      print(hex(funcs[i]), '-->', hex(funcs[i+len(pattern)+1]))

def search_func_guessed_size_pattern(pattern, pattern_b=None, size_getter=None):
  search_func_size_pattern(pattern, pattern_b, size_getter=get_func_guessed_size)
