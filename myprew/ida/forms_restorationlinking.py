#import sip # Without this, this will not work!
from idaapi import PluginForm
from PyQt5 import QtCore, QtGui, QtWidgets

class RestorationLinkingFormClass(PluginForm):
    def OnCreate(self, form):
        """
        Called when the widget is created
        """

        # Get parent widget
        self.parent = self.FormToPyQtWidget(form)
        self.PopulateForm()

    def PopulateForm(self):
        # Create layout
        layout = QtWidgets.QVBoxLayout()

        # create an empty text box
        self.hash = QtWidgets.QLineEdit()
        self.hash.textChanged.connect(self.update_item)
        #self.edit = QtWidgets.QPlainTextEdit()

        # create an empty list
        self.list = QtWidgets.QListWidget()
        #self.list.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.list.currentItemChanged.connect(self.print_item)
        self.detail = QtWidgets.QListWidget()
        self.detail.currentItemChanged.connect(self.remote_jump)
        
		# item         
        for func_ea, cmt in myprew.get_origin_comments_for_functions():
            self.add_item(hex(func_ea) + ' ' + get_func_name(func_ea) + ' ' + cmt)
        
        # table 
        #self.table = QtWidgets.QTableWidget()
        #self.table.setRowCount(4)
        #self.table.setColumnCount(25)
        #self.table.setHorizontalHeaderLabels(["Rule File", "Rename", "Comment", "Pattern"])
        #self.table.setItem(0,0, QtWidgets.QTableWidgetItem("Cell (1,1)"))
        #self.table.setItem(0,1, QtWidgets.QTableWidgetItem("Cell (1,2)"))
        #self.table.setItem(1,0, QtWidgets.QTableWidgetItem("Cell (2,1)"))
        #self.table.setItem(1,1, QtWidgets.QTableWidgetItem("Cell (2,2)"))
        #self.table.setItem(2,0, QtWidgets.QTableWidgetItem("Cell (3,1)"))
        
        # create a button and connect it's "clicked" signal to our "add_item" slot
        #self.genbtn = QtWidgets.QPushButton("Generate Skelton From Current Function (Cursor)")
        #self.genbtn.clicked.connect(self.add_item)
        self.addbtn = QtWidgets.QPushButton("Add Rule From Selected Attributes")
        self.addbtn.clicked.connect(lambda : self.add_item(''))
        #layout.addWidget(self.table)
        #layout.addWidget(self.genbtn)
        #layout.addWidget(self.list)
        layout.addWidget(self.addbtn)
        layout.addWidget(self.list)
        layout.addWidget(self.hash)
        layout.addWidget(self.detail)

        # make our created layout the dialogs layout
        self.parent.setLayout(layout)

    def add_item(self, item):
        self.list.addItem(item)
    
    def print_item(self):
        jumpto(self.update_item())
		
    def remote_jump(self):
        parts = self.detail.currentItem().text().split(' ')
        if len(parts) != 6:
            return

        addr = int(parts[3].strip()[2:], 16)
        myprew.jump_to_address_in_remote(addr)
    
    def update_item(self):
        self.detail.clear()
        addr, _, name = self.list.currentItem().text().partition(' ')
        with open('d:\\searchall.log', 'rt') as f:
            keep_adding = ''
            for line in f.readlines():
              if '.[' in line and line.upper().startswith(addr.upper() + '.[') and not line.rstrip().endswith(':') and self.hash.text().upper() in line.upper():
                 self.detail.addItem(line[len(addr)+1:].rstrip())
                 keep_adding, _, _ = line[len(addr)+1:].partition(':')
              elif keep_adding and not '.[' in line and line.startswith('0x'):
                 self.detail.addItem(keep_adding + ' :' + line.rstrip())
              elif keep_adding:
                 keep_adding = ''
        return int(addr[2:], 16)

def show_restoration_linking_comments():
    plg = RestorationLinkingFormClass()
    plg.Show("Demo Search")
