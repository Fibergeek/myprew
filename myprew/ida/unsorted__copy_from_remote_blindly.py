import myprew

bridge = myprew.ensure_bridge_to_original()

for func_ea in myprew.get_crippled_funcs():
  extra = [] 
  if func_ea != myprew.get_func_start_ea(func_ea):
    extra.append('START_EA')
  remote_func_size = bridge.remote_eval('myprew.get_func_size({0})'.format(func_ea))
  remote_funcbytes = bridge.remote_eval('myprew.get_func_bytes({0})'.format(func_ea))
  
  if myprew.get_func_bytes(func_ea) != remote_funcbytes:
    ida_bytes.patch_bytes(func_ea, remote_funcbytes)
  
  if remote_func_size != myprew.get_func_size(func_ea):
    extra.append('FUNC_SIZE')
  if not extra:
    func_bytes = myprew.get_func_bytes(func_ea)
    if remote_funcbytes != func_bytes:
      extra.append('FUNC_BYTES')
  if extra:
    print(hex(func_ea), myprew.get_func_name(func_ea), str(myprew.get_func_size(func_ea)).rjust(4), ', '.join(extra))
  else:
    myprew.set_func_prefix_to_RESTORED(func_ea)
  if 'FUNC_BYTES' in extra:
    for i, (b, r) in enumerate(zip(func_bytes, remote_funcbytes)):
      if b != r:
        print('  ' + hex(func_ea + i))
