from binascii import hexlify

from idaapi import BADADDR

from .utils import get_length
from .items import get_item_bytes, get_item_size, is_item_code
from .item_analysis import get_item_prefixes_and_opcode, is_item_rip_relative
from .functions import get_func_start_ea, get_func_name, get_funcs, get_next_func_start_ea, get_func_items, get_func_bytes

from idaapi import get_byte, get_original_byte

import idc

def is_func_of_max_size(ea, expected_max_size):
    return get_func_max_size(ea) == expected_max_size

def get_func_max_size(item_ea):
    """
    Returns the maximum size in a bytes a function can be.
    This size is calculated by subtracting the start addresses of the next function and this function.
    This calculation will only keep the first chunk in mind.
    """
    start_ea = get_func_start_ea(item_ea)
    next_start_ea = get_next_func_start_ea(item_ea)
    if start_ea == BADADDR or next_start_ea == BADADDR:
        return 0
    return max(next_start_ea - start_ea, 0)

def is_func_of_guessed_size(ea, expected_guessed_size):
    return get_func_max_size(ea) == expected_guessed_size

def get_func_guessed_size(ea, align_byte=0xCC):
    next_ea = get_next_func_start_ea(ea)
    if next_ea == BADADDR or ea == BADADDR:
        return 0
    while get_byte(next_ea - 1) == align_byte:
        next_ea -= 1
    return next_ea - (get_func_start_ea(ea) or ea)

def get_func_original_guessed_size(ea, align_byte=0xCC):
    next_ea = get_next_func_start_ea(ea)
    if next_ea == BADADDR or ea == BADADDR:
        return 0
    while get_original_byte(next_ea - 1) == align_byte:
        next_ea -= 1
    return next_ea - (get_func_start_ea(ea) or ea)

def print_funcs_of_max_size(expected_max_size, printer=None):
    if printer == None:
        printer = print
    
    for func_ea in get_funcs_of_max_size(expected_max_size):
        printer(hex(func_ea), get_func_name(func_ea))

def get_funcs_of_max_size(expected_max_size):
    for func_ea in get_funcs():
        if is_func_of_max_size(func_ea, expected_max_size):
            yield func_ea

def get_func_item_count(item_ea):
    return get_length(get_func_item_count(item_ea))

def get_func_ascii(ea):
  bytes = get_func_bytes(ea)
  return hexlify(bytes).decode('ascii')

def get_func_dotted_ascii(ea):
  ascii = ''
  for item_ea in get_func_items(ea):
    if is_item_rip_relative(item_ea):
      opcodes = get_item_prefixes_and_opcode(item_ea)
      ascii += hexlify(opcodes).decode('ascii')
      ascii += '.'  * 2 * (get_item_size(item_ea) - len(opcodes))
    else:
       ascii += hexlify(get_item_bytes(item_ea)).decode('ascii')
  return ascii

def is_func_followed_by_legit_func(func_ea, printer=None, threshold=2):
  next_func_ea = get_next_func_start_ea(func_ea)
  if next_func_ea == BADADDR:
    return False
  return is_legit_func(next_func_ea, printer, threshold)

def is_legit_func(func_ea, printer=None, threshold=2):
  TESTS =  'is_func_aligned_as_expected', 'func_contains_no_unexpected_mnem', 'func_ends_with_correct_mnem'

  err_count = 0

  if printer == None:
    def printer(s, end=None):
      pass

  # Check alignment
  for test in TESTS:
    printer('Testing ' + test, end='')
    result = eval(test)(func_ea)
    printer(': ' + str(result))
    if not result:
      err_count += 1
      if err_count >= threshold:
        return False

  return True

def is_func_aligned_as_expected(func_ea):
  return not bool(func_ea % 4)

def func_contains_no_unexpected_mnem(func_ea):
  return not func_contains_unexpected_mnem(func_ea)

def func_contains_unexpected_mnem(func_ea):
  for item_ea in get_func_items(func_ea):
    mnem = idc.print_insn_mnem(item_ea)
    if mnem in ['in', 'insb', 'insd', 'out', 'outsb', 'outsd', 'retf', 'iret', 'hlt', 'sti', 'cli']:
      return True
  return False

def func_ends_with_correct_mnem(func_ea):
  last_item_ea = list(get_func_items(func_ea))[-1]
  mnem = idc.print_insn_mnem(last_item_ea)
  return mnem in ['jmp', 'retn']
