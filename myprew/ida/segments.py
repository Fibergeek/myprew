import idaapi

def get_header_segment():
  return get_segment(idaapi.get_imagebase())

def get_default_code_segment():
  # TODO: support other platforms other than Windows
  default_ea = get_header_segment().end_ea
  return get_segment(default_ea)

def enum_nondefault_code_segments():
  default_code_seg = get_default_code_segment()
  if default_code_seg != None:
    is_nondefault_code_segment = lambda segm: segm.start_ea != default_code_seg.start_ea
  else:
    is_nondefault_code_segment = lambda segm: True
  return filter(is_nondefault_code_segment, enum_code_segments())

def enum_code_segments():
  return filter(is_code_segment, enum_segments())

def enum_segments():
  segm = idaapi.get_first_seg()
  while True:
    if segm == None:
      break
    yield segm
    segm = idaapi.get_next_seg(segm.start_ea)

def is_code_segment(seg_or_ea):
  # This function returns True if a segment is executable
  # It will return False otherwise and also if the supplied argument is invalid
  return get_segment_perm(seg_or_ea) & idaapi.SEGPERM_EXEC

def get_segment_name(ea_or_segm):
  # This function returns the name of a segment or an empty string
  # The input can be an address or a segment object
  segm = get_segment(ea_or_segm)
  if segm == None:
    return ''
  return idaapi.get_segm_name(segm, 0)

def get_segment_flags(ea_or_segm):
  # This function returns the flags of a segment or zero
  # The input can be an address or a segment object
  segm = get_segment(ea_or_segm)
  if segm == None:
    return 0
  return segm.flags

def get_segment_perm(ea_or_segm):
  # This function returns the permissions of a segment or zero
  # The input can be an address or a segment object
  segm = get_segment(ea_or_segm)
  if segm == None:
    return 0
  return segm.perm

def get_segment_start(ea_or_segm):
  # This function returns the start address of a segment or zero
  # The input can be an address or a segment object
  segm = get_segment(ea_or_segm)
  if segm == None:
    return 0
  return segm.start_ea
 
def get_segment_end(ea_or_segm):
  # This function returns the end address of a segment or zero
  # The input can be an address or a segment object
  segm = get_segment(ea_or_segm)
  if segm == None:
    return 0
  return segm.end_ea

def get_segment_size(ea_or_segm):
  # This function returns the size of a segment or zero
  # The input can be an address or a segment object
  segm = get_segment(ea_or_segm)
  if segm == None:
    return 0
  return segm.end_ea - segm.start_ea

def get_segment(ea_or_segm):
  # This function returns a segment object
  # The input can be an address or an segment object
  if ea_or_segm == None or isinstance(ea_or_segm, idaapi.segment_t):
    return ea_or_segm
  return idaapi.getseg(ea_or_segm)