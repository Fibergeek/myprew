from binascii import hexlify
from struct import pack
try:
  import pydis
except ImportError:
  pydis = None

# We require the support library for some extra functions
support_lib

def test_reassemble_code(code, cur_addr, new_addr):
  end_addr = cur_addr + len(code)
  as_reassembleable_str = lambda instruction: get_loc_label(instruction) + with_fixed_reladdr(instruction, cur_addr, end_addr, new_addr)
  old_instrs = decode_as_list(cur_addr, code)
  new_text = '\n'.join(map(as_reassembleable_str, old_instrs))
  new_code, count = test_assemble_code(new_addr, new_text)
  if new_code == None:
    raise Exception('Error! Please review the code: """\n' + new_text + '\n"""')
  new_instrs = decode_as_list(new_addr, new_code)
  if len(old_instrs) != len(new_instrs):
    raise Exception('Instruction count changed for {0}'.format(hex(cur_addr)))
  if len(code) != len(new_code):
    raise Exception('Code size changed for {0}'.format(hex(cur_addr)))

def get_relocated_ida_code(cur_addr, new_addr, insert_rex=False):
  # TODO: remove this dependency
  is_protected_addr = lambda ea: eval_idc('{0} >= get_protection_start() && !is_protected_address_exception({0})'.format(ea))
  
  global_labels = {}
  original_code = support_lib.get_func_bytes(cur_addr)
  end_addr = cur_addr + len(original_code) # TODO: fix this for obfuscated code with different length
  try:
    as_reassembleable_str = lambda instruction: get_loc_label(instruction) + with_fixed_reladdr(instruction, cur_addr, end_addr, new_addr, global_labels)
    new_instrs = generate_deobfuscated_ida_function_as_instr(cur_addr, is_protection_addr=is_protected_addr)
    # pydis.decode(code, cur_addr)
    new_text = '\n'.join(map(as_reassembleable_str, new_instrs))
  except Failure as ex:
    print(str(ex))
    return b''
  if insert_rex:
    new_text = 'db 0x40\n' + new_text
  new_text += '\n' + '\n'.join(['labelat ' + data[0] + ',' + hex(data[1]) for data in global_labels.items()])
  #print(new_text)
  new_code, labels = assemble_code(new_addr, new_text)
  if new_code == None:
    raise Exception('Error! Please review the code: """\n' + new_text + '\n"""')
  return new_code

def decode_as_list(addr, code):
  return list(pydis.decode(code, addr))

def get_loc_label(instruction):
    try:
        return 'loc_' + hex(instruction.Addr)[2:] + ': ; ' + hexlify(instruction.Bytes).decode('ascii') + '\n'
    except:
        return 'loc_' + hex(instruction.address)[2:] + ': ; ' + hexlify(instruction.bytes).decode('ascii') + '\n'

def with_fixed_reladdr(instruction, old_addr, end_addr, new_addr, global_labels=None):
  # TODO: fix this in a good stable way!!!
  instruction = list(pydis.decode(instruction.Bytes, instruction.Addr))[0]
  
  # 
  if not instruction.attributes & pydis.InstructionAttribute.Is_Relative:
      return '  ' + instr_as_str(instruction, convert_nop_to_align=True)

  instr_text = instr_as_str(instruction) # WARNING! This seems to be a requirement!
  rewritten = False
  for addr in re.findall('\[0x[A-F0-9]*\]', instr_text):
        addr_int = int(addr[3:-1], 16)
        if addr_int >= old_addr and addr_int < end_addr:
          instr_text = instr_text.replace(addr, '[' + 'loc_' + hex(addr_int)[2:] + ']')
          rewritten = True
        else:
          instr_text = instr_text.replace(addr, '[global_' + hex(addr_int)[2:] + ']')
          if global_labels != None:
              global_labels['global_' + hex(addr_int)[2:]] = addr_int
          rewritten = True
  if not rewritten:
        for addr in re.findall('0x[A-F0-9]*', instr_text):
          addr_int = int(addr[2:], 16)
          if addr_int >= old_addr and addr_int < end_addr:
            instr_text = instr_text.replace(addr, 'loc_' + hex(addr_int)[2:])
            rewritten = True
  if not rewritten:
      return instr_text + ' ; Is_Relative_Rewritten'
  return  instr_text + ' ; Is_Relative'

def instr_as_str(instr, convert_nop_to_align=False):
  s = str(instr)
  if instr.mnemonic == 'nop':
    print('Note, converting NOP to NOP' + ('ALIGN 16' if convert_nop_to_align else str(instr.length)) + ' at ' + hex(instr.address))
    if convert_nop_to_align:
      s = 'nopalign 16' # TODO! We have to see the next address to verify the alignment!!!
    else:
      s = "nop" + str(instr.length)
  if instr.mnemonic == 'movmskpd':
    # Bypass a bug in Zydis: https://github.com/zyantific/zydis/issues/312
    return 'db ' + ', '.join([hex(b) for b in instr.bytes])
  if instr.attributes & pydis.InstructionAttribute.Is_Relative:
    # TODO: When using NASM, do we really need to specify short/near?
    if instr.operands[0].encoding.name == 'JImm_8':
      l, _, r = s.rpartition(' ')
      #s = l + ' short ' + r
    elif instr.operands[0].encoding.name.startswith('JImm_'):
      l, _, r = s.rpartition(' ')
      #s = l + ' near ' + r
  if '[' in s and '+' in s:
    if instr.raw.disp.size == 32: # Prevent NASM/YASM from optimizing!
      s = s.replace('[', '[dword ')
  if ', [' in s and instr.mnemonic in ('cvtsi2sd'):
    s = s.replace(', [', ', qword ptr [')
  if 'inc [' in s or 'dec [' in s or 'not [' in s or 'neg [' in s or 'mul [' in s or 'div [' in s:
    if instr.operands[0].size == 8:
      s = s.replace(' [', ' byte ptr [')
    elif instr.operands[0].size == 16:
      s = s.replace(' [', ' word ptr [')
    elif instr.operands[0].size == 32:
      s = s.replace(' [', ' dword ptr [')
    elif instr.operands[0].size == 64:
      s = s.replace(' [', ' qword ptr [')
  s = s.replace(' ptr [', ' [')
  return s

def assemble_code(addr, code):
  try:
   cleaned_code = get_cleaned_code(code)
   encoding, labels = support_lib.nasm.assemble_x64(cleaned_code, addr=addr, ram_drive='N:\\')
   return encoding, labels
  except Exception as ex:
   print("ERROR: %s" % ex)
   return None, []

def test_assemble_code(addr, code):
  cleaned_code = get_cleaned_code(code)
  encoding, labels = assemble_code(addr, code)
  print("%s\n------>\n%s\n======>\n%s (Number of statements: %u)" % (code, cleaned_code, hexlify(encoding or b''), labels))
  if labels and pydis:
    print("------>")
    instruction = None # in case of data-only without any valid instructions we need to initalize the variable!
    for instruction in pydis.decode(encoding, addr):
      print('{0} ; {1}'.format(instr_as_str(instruction), hexlify(instruction.bytes).decode('ascii')))
    last_addr = (instruction.address + instruction.length) if instruction else addr
    remaining = addr + len(encoding) - last_addr
    if remaining > 0:
      print('db %s' % ', '.join(map(lambda b: '0x%02x' % b, encoding[-remaining:])))
  return encoding, labels

def get_cleaned_code(code):
  return '\n'.join(remove_comments(code.split('\n')))

def remove_comments(lines):
  for line in lines:
    line, _, _ = line.partition(';')
    line = line.strip()
    line = line.replace('\t', ' ')
    line = line.replace(' ptr ', ' ')
    if line:
      yield line

def undefine_range(source_ea, size):
  if size <= 0:
    return
  idaapi.del_items(source_ea, DELIT_SIMPLE, size)

def get_fixable_xrefs(func_ea, ignore_eas=None):
  ignore_eas = ignore_eas or []
  for item in FuncItems(func_ea):
    for xref in XrefsTo(item, 1):
      frm = xref.frm
      if get_func_attr(frm, FUNCATTR_START) == func_ea:
        continue
      if frm in ignore_eas:
        continue
      if is_supported(frm):
        #print(hex(frm), idc.generate_disasm_line(frm, 1))
        if not ' short ' in idc.generate_disasm_line(frm, 0): # Ignore short jumps, those are the result of random inserts
          if not idc.generate_disasm_line(frm, 0).startswith('jmp     loc_'): # Ignore fixed jmp's to local labels
            if not idc.generate_disasm_line(frm, 0).startswith('dq offset loc_'): # Ignored xrefs to local labels
              if not idc.generate_disasm_line(frm, 0).startswith('jrcx'): # Ignore short jumps, those are the result of random inserts
                if not idc.generate_disasm_line(frm, 0).startswith('loop'): # Ignore short jumps, those are the result of random inserts
                  if not idc.generate_disasm_line(frm, 0).startswith('lea') or xref.to == func_ea:
                    yield xref
        continue
      if 'cs:loc_' in idc.generate_disasm_line(frm, 0): # Ignore memory access local labels
        continue
      raise Exception('Unable to handle the xref at ' + hex(frm))

def is_supported(item):
  cmd = idc.generate_disasm_line(item, 0)
  if is_RUNTIME_FUNCTION(cmd) or is_DQ(cmd) or is_DD(cmd):
    return idc.get_item_head(item) == item # TODO: this won't correctly detecting the beginning of an array, only array items with idx>0
  cmd = idc.generate_disasm_line(item, 1)
  return is_MOVOFFSET(cmd) or is_LEA(cmd) or is_JMP(cmd) or is_RUNTIME_FUNCTION(cmd)

def is_MOVOFFSET(cmd):
  return cmd.startswith('mov ') and ', offset ' in cmd

def is_LEA(cmd):
  return cmd.startswith('lea ')

def is_JMP(cmd):
  cmds = 'jmp', 'jrcxz', 'loop', 'loope', 'loopne', 'jo', 'jno', 'jp', 'jnp', 'jz', 'jnz', 'js', 'jns', 'ja', 'jb', 'jbe', 'jnb', 'jl', 'jle', 'jg', 'jge'
  for x in cmds:
    if cmd.startswith(x + ' '):
      return True
    if cmd.startswith('bnd ' + x + ' '):
      return True
  return False

def is_DD(cmd):
  return cmd.startswith('dd rva ')

def is_DQ(cmd):
  return cmd.startswith('dq offset ')

def is_RUNTIME_FUNCTION(cmd):
  return cmd.startswith('RUNTIME_FUNCTION ')

def move_function(source_ea, move_always_if_new_size_matches=False, dump_bytes_on_newsize_mismatch=True):
  if idaapi.get_byte(source_ea) != 0xE9:
    return False
  if idc.get_func_attr(source_ea, FUNCATTR_START) != source_ea:
    print('The source', hex(source_ea), 'is not the start of a function')
    return False
  if idc.next_func_chunk(source_ea, source_ea) != BADADDR:
    print('Multiple chunks detected at', hex(source_ea))
    return False
  target_ea = source_ea + 5 + idaapi.get_dword(source_ea + 1)
  if idc.next_func_chunk(target_ea, target_ea) != BADADDR:
    print('Multiple chunks detected at', hex(target_ea))
    return False
  try:
    fixable_xrefs = list(get_fixable_xrefs(target_ea, [source_ea]))
  except Exception as ex:
    print(hex(source_ea) + ': ' + str(ex))
    return False
  guessed_size = support_lib.get_func_guessed_size(source_ea)
  max_size = support_lib.get_func_max_size(source_ea)
  moved_size = support_lib.get_func_size(target_ea)
  func_bytes = support_lib.get_func_bytes(target_ea)
  if idaapi.get_word(target_ea) in (0x5340, 0x5540, 0x5640, 0x5740):
    new_bytes = get_relocated_ida_code(get_func_attr(target_ea, FUNCATTR_START), source_ea, insert_rex=True)
  else:
    new_bytes = get_relocated_ida_code(get_func_attr(target_ea, FUNCATTR_START), source_ea)
  if len(new_bytes) + 1 == moved_size and new_bytes[0] in (0x53, 0x55, 0x56, 0x57):
    new_bytes = get_relocated_ida_code(get_func_attr(target_ea, FUNCATTR_START), source_ea, insert_rex=True)
  print(hex(source_ea) + ':', 'G', guessed_size, 'M', max_size, 'F', moved_size, 'C', len(func_bytes), end=' ')
  print('N', len(new_bytes))
  if len(new_bytes) == 0:
    return False
  if moved_size == 0 or moved_size > max_size:
    if len(new_bytes) != guessed_size:
      return False
  if guessed_size != len(new_bytes):
    if guessed_size + 1 == len(new_bytes):
      pass # One extra alignment byte we allow always
    elif move_always_if_new_size_matches and len(new_bytes) == moved_size:
      # If the new calculated size matches, we expect atleast a 0xCC alignment byte there
      # NOTE: this trick will work as long as alignment bytes aren't overwritten with random bytes
      end_of_new_function = source_ea + len(new_bytes)
      if end_of_new_function % 16:
        if idaapi.get_byte(end_of_new_function) != 0xCC:
           allowed_jpt_ea = end_of_new_function
           if allowed_jpt_ea % 4 == 3 and idaapi.get_byte(end_of_new_function) == 0x90:
             allowed_jpt_ea += 1 # NOP alignment inside code (normall right before a jpt_XXX)
           elif allowed_jpt_ea % 4 == 2 and idaapi.get_word(end_of_new_function) == 0x9066:
             allowed_jpt_ea += 2 # NOP alignment inside code (normall right before a jpt_XXX)
           elif allowed_jpt_ea % 4 == 1 and (idaapi.get_dword(end_of_new_function) & 0xFFFFFF) == 0x001F0F:
             allowed_jpt_ea += 3 # NOP alignment inside code (normall right before a jpt_XXX)
           if get_name(allowed_jpt_ea)[:4] != 'jpt_':
             if idaapi.get_byte(end_of_new_function) != 0x48 and idaapi.get_word(end_of_new_function + 1) != 0xE981 and idaapi.get_byte(end_of_new_function + 7) != 0xE9: # known MOV ECX, X + JMP
               print('Found unexpected byte 0x%02X at %s' % (idaapi.get_byte(end_of_new_function), hex(end_of_new_function)))
               return False
    elif dump_bytes_on_newsize_mismatch and len(new_bytes) != moved_size:
      print('NewBytes:', hexlify(new_bytes).decode('ascii'))
      return False
    else:
      return False
  for xref in fixable_xrefs:
    if xref.to != target_ea:
      cmd = idc.generate_disasm_line(xref.frm, 0)
      if moved_size != len(new_bytes) or not is_RUNTIME_FUNCTION(cmd) : # We can fix RUNTIME_FUNCTION if the size is the same
        print('Unable to fix the xref ' + hex(xref.frm) + '(' + cmd + ')')
        return False
  undefine_range(source_ea, max_size)
  idaapi.patch_bytes(source_ea, new_bytes)
  idaapi.add_func(source_ea, BADADDR)
  idaapi.auto_wait()
  already_fixed = []
  for xref in fixable_xrefs:
    print('Fixing {0}: '.format(hex(xref.frm)), end='')
    cmd = idc.generate_disasm_line(xref.frm, 0)
    if is_RUNTIME_FUNCTION(cmd):
      if not xref.frm in already_fixed:
        already_fixed.append(xref.frm)
        print(cmd)
        diff = target_ea - source_ea
        #print('<I', idaapi.get_dword(xref.frm) - diff, '<I', idaapi.get_dword(xref.frm + 4) - diff)
        new_xref_bytes = pack('<I', idaapi.get_dword(xref.frm) - diff) + pack('<I', idaapi.get_dword(xref.frm + 4) - diff - moved_size + len(new_bytes))
        idaapi.del_items(xref.frm, 0, get_item_size(xref.frm))
        idaapi.patch_bytes(xref.frm, new_xref_bytes)
        idc.create_struct(xref.frm, -1, 'RUNTIME_FUNCTION')
        idaapi.auto_wait()
      else:
        print('(already fixed)')
    elif is_DD(cmd):
      print(cmd)
      diff = target_ea - source_ea
      idaapi.patch_dword(xref.frm, idaapi.get_dword(xref.frm) - diff)
      idaapi.set_cmt(xref.frm, 'ORIGINAL:' + cmd, 0)
      idaapi.auto_wait()
    elif is_DQ(cmd):
      print(cmd)
      diff = target_ea - source_ea
      new_xref_bytes = pack('<Q', get_qword(xref.frm) - diff)
      idaapi.del_items(xref.frm, 0, get_item_size(xref.frm))
      idaapi.patch_bytes(xref.frm, new_xref_bytes)
      idaapi.set_cmt(xref.frm, 'ORIGINAL:' + cmd, 0)
      idc.create_qword(xref.frm)
      idaapi.auto_wait()
    elif is_MOVOFFSET(cmd):
      print(cmd)
      diff = target_ea - source_ea
      in_instr = xref.frm + get_item_size(xref.frm) - 8
      idaapi.patch_qword(in_instr, idaapi.get_qword(in_instr) - diff)
      idaapi.set_cmt(xref.frm, 'ORIGINAL:' + cmd, 0)
      idaapi.auto_wait()
    else:
      cmd = idc.generate_disasm_line(xref.frm, 1)
      print(cmd)
      if is_LEA(cmd):
        new_xref_code = cmd.split(',')[0] + ', [global_' + hex(source_ea)[2:] + ']\nlabelat global_' + hex(source_ea)[2:] + ',' + hex(source_ea)
      elif cmd.startswith('bnd '):
        # TODO: does YASM/NASM support bnd???
        # NOTE: KS uses repne instead of bnd
        new_xref_code = 'bnd ' + cmd.split(' ')[1] + ' ' + hex(source_ea)
      else:
        new_xref_code = cmd.split(' ')[0] + ' ' + hex(source_ea)
      new_xref_bytes, _ = assemble_code(xref.frm, new_xref_code)
      if not new_xref_bytes:
        raise Exception('Unable to re-assemble an xref:' + hex(xref.frm))
      idaapi.del_items(xref.frm, 4, get_item_size(xref.frm)) # 4 == DELIT_NOTRUNC
      idaapi.patch_bytes(xref.frm, new_xref_bytes)
      idaapi.create_insn(xref.frm)
      idaapi.auto_wait()
      idaapi.set_cmt(xref.frm, 'ORIGINAL:' + cmd, 0)
  set_func_cmt(source_ea, 'RELOCATED: ' + hex(target_ea), 0)
  return True


move_function(get_screen_ea())
