import idc

COLOR_DEFAULT = 0xFFFFFFFF

def is_item_colorized(item_ea):
  return get_item_color(item_ea) != COLOR_DEFAULT

def is_item_colored_red(item_ea):
  return get_item_color(item_ea) in (0x2020FF, 0x3030FF)

def is_item_colored_green(item_ea):
  return get_item_color(item_ea) == 0x77FF77

def is_item_colored_orange(item_ea):
  return get_item_color(item_ea) == 0x00A5FF

def get_item_color(item_ea):
  return idc.get_color(item_ea, idc.CIC_ITEM)

def mark_item_as_default(item_ea):
  idc.set_color(item_ea, idc.CIC_ITEM, COLOR_DEFAULT)

def mark_item_as_red(item_ea):
  idc.set_color(item_ea, idc.CIC_ITEM, 0x2020FF)

def mark_item_as_green(item_ea):
  idc.set_color(item_ea, idc.CIC_ITEM, 0x77FF77)

def mark_item_as_orange(item_ea):
  idc.set_color(item_ea, idc.CIC_ITEM, 0x00A5FF)

def mark_item_as_white(item_ea):
  idc.set_color(item_ea, idc.CIC_ITEM, 0xFFFFFF)
