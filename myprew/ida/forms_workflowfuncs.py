#import sip # Without this, this will not work!
from idaapi import PluginForm
from PyQt5 import QtCore, QtGui, QtWidgets

class WorkflowFunctionsPluginForm(PluginForm):
    def OnCreate(self, form):
        """
        Called when the widget is created
        """

        # Get parent widget
        self.parent = self.FormToPyQtWidget(form)
        self.PopulateForm()

    def PopulateForm(self):
        # Create layout
        layout = QtWidgets.QVBoxLayout()

        # create an empty list
        self.detail = QtWidgets.QListWidget()
        self.detail.currentItemChanged.connect(self.remote_jump)
        
	    # table 
        self.table = QtWidgets.QTableWidget(selectionBehavior=QtWidgets.QAbstractItemView.SelectRows, selectionMode=QtWidgets.QAbstractItemView.SingleSelection)
        self.table.setColumnCount(2)
        self.table.setHorizontalHeaderLabels(["Prefix", "Count"])
        self.table.itemSelectionChanged.connect(self.table_item_changed)
		
		# init the table
        self.refresh()
        
        # create a button and connect it's "clicked" signal to our "add_item" slot
        #self.genbtn = QtWidgets.QPushButton("Generate Skelton From Current Function (Cursor)")
        #self.genbtn.clicked.connect(self.add_item)
        self.refresh_button = QtWidgets.QPushButton("Refresh")
        self.refresh_button.clicked.connect(self.refresh)
        layout.addWidget(self.refresh_button)
        layout.addWidget(self.table)
        layout.addWidget(self.detail)

        # make our created layout the dialogs layout
        self.parent.setLayout(layout)
		
    def refresh(self):
        prefixes = myprew.get_funcs_by_prefix(myprew.get_workflow_funcs())

		# item         
        self.table.setRowCount(len(prefixes))
        for i, (prefix, funcs) in enumerate(prefixes.items()):
            self.table.setItem(i,0, QtWidgets.QTableWidgetItem(prefix))
            self.table.setItem(i,1, QtWidgets.QTableWidgetItem(str(len(funcs))))

    def table_item_changed(self):
        self.detail.clear()
        items = self.table.selectedItems()
        prefix = items[0].text()
        prefixes = myprew.get_funcs_by_prefix(myprew.get_workflow_funcs())
        for func_ea in prefixes.get(prefix, []):
            self.detail.addItem(hex(func_ea) + ' '+ myprew.get_func_name(func_ea))
    	
    def remote_jump(self):
        parts = self.detail.currentItem().text().split(' ')
        if len(parts) != 2:
            return

        addr = int(parts[0].strip()[2:], 16)
        idc.jumpto(addr)

def show_workflow_funcs():
    plg = WorkflowFunctionsPluginForm()
    plg.Show("Workflow Functions")
