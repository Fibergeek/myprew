import idautils
import ida_funcs
from idaapi import BADADDR

from .items import get_item_size
from .memory import PAGE_SIZE, get_bytes

def get_func_start_ea(item_ea):
    """
    Returns the start address of a function.
    If the given address is not part of a function then BADADDR is returned.
    """
    func = ida_funcs.get_func(item_ea)
    return BADADDR if func == None else func.start_ea

def get_func_end_ea(item_ea):
    """
    Returns the end address of a function.
    If the given address is not part of a function then BADADDR is returned.
    """
    func = ida_funcs.get_func(item_ea)
    return BADADDR if func == None else func.end_ea

def get_prev_func_start_ea(item_ea):
    func = ida_funcs.get_prev_func(item_ea)
    return BADADDR if func == None else func.start_ea

def get_next_func_start_ea(item_ea):
    func = ida_funcs.get_next_func(item_ea)
    return BADADDR if func == None else func.start_ea

def get_func_prefix(item_ea):
    """
    """
    name = get_func_name(item_ea) 
    if '_' in name:
        prefix, _, _ = name.partition('_')
    else:
        prefix = ''
    return prefix

def get_func_name(item_ea):
    """
    Returns the name of a function.
    If the given address is not part of a function then '' is returned.
    """
    name = ida_funcs.get_func_name(item_ea)
    return name if name else ''

def get_func_items(item_ea):
    """
    Returns all the items of a function
    """
    return idautils.FuncItems(item_ea)

def get_funcs(filter_func=None):
    """
    Returns all functions
    """
    return filter(filter_func, idautils.Functions())

def get_func_pages(ea):
  """ Return all pages which are part of function code."""
  pages = set()

  # NOTE: we use FuncItems instead of start_ea/end_ea
  #               so we can support chunked functions
  # TODO: Use Chunks instead
  #               see: https://reverseengineering.stackexchange.com/questions/14725/using-ida-python-iterate-through-all-functions-and-their-instructions
  for item in get_func_items(ea):
    page_of_start = item & ~(PAGE_SIZE - 1)
    pages.add(page_of_start)
    page_of_end = (item + get_item_size(item) - 1) & ~(PAGE_SIZE - 1)
    pages.add(page_of_end)
  
  return pages

def get_func_chunks_bytes(ea):
  """ Return all bytes per chunk of a function """
  for start_ea, end_ea in idautils.Chunks(ea):
    yield start_ea, get_bytes(start_ea, end_ea - start_ea)

def get_func_bytes(ea):
  """ Return all function chunks as merged bytes """
  bytes = [bytes for ea, bytes in get_func_chunks_bytes(ea)]
  return b''.join(bytes)

def get_func_size(ea):
  f = ida_funcs.get_func(ea)
  if f == None:
    return 0
  return f.size()

def recreate_chunked_func(ea_to_redefine=None):
  if ea_to_redefine == None:
    ea_to_redefine = get_screen_ea()
  
  # TODO: verify that the EA is a function chunk  
  
  blocks_to_undefine = list(CodeRefsTo(ea_to_redefine, 0))
  funcs_to_undefine = [myprew.get_func_start_ea(ea) for ea in blocks_to_undefine if get_func_start_ea(ea) != BADADDR]
  for func_ea in funcs_to_undefine:
    for item in list(get_func_items(func_ea))[::-1]:
      del_items(func_ea, DELIT_SIMPLE)
  del_items(ea_to_redefine, DELIT_SIMPLE)
  add_func(ea_to_redefine)
  for func_ea in funcs_to_undefine:
    create_insn(func_ea)
    add_func(func_ea, BADADDR)
