import idc

def get_item_bytes(item_ea):
    return idc.get_bytes(item_ea, get_item_size(item_ea))

def get_item_size(item_ea):
    return idc.get_item_size(item_ea)

def is_item_code(ea=None):
  if ea == None:
    ea = idc.get_screen_ea()
  return idc.is_code(idc.get_full_flags(ea))

def get_item_ea(ea=None):
  # TODO: what is the correct name get_item_ea or get_item_start_ea or get_head ???
  return get_head(ea)

def get_head(ea):
  if ea == idc.BADADDR:
    return idc.BADADDR
  flags = idc.get_full_flags(ea)
  while not idc.is_head(flags):
    ea -= 1
    flags = idc.get_full_flags(ea)
  return ea
