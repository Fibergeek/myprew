import idaapi
import idautils
import ida_kernwin

from .utils import get_name_as_addr
from .item_analysis import get_item_refered_absolute_target, update_item_as_instr_with_retargetted_absolute_target
from .function_workflow import set_func_prefix_to_ORIGINAL, set_func_prefix_to_CRIPPLED, set_func_prefix_to_COPIED
from .function_workflow import set_func_prefix_to_ONGOING, set_func_prefix_to_RESTORED, set_func_prefix_to_VERIFIED
from .item_coloring import mark_item_as_default, mark_item_as_orange, mark_item_as_red, mark_item_as_green, mark_item_as_white
from .bridge_workflow import jump_to_item_in_original, jump_to_address_in_remote
from .items import get_item_bytes

import myprew
print_func_names_by_max_size = myprew.print_func_names_by_max_size
print_counts_by_prefix = myprew.print_counts_by_prefix
get_workflow_funcs = myprew.get_workflow_funcs

class IDACtxEntry(idaapi.action_handler_t):
    """
    A minimal context menu entry class to utilize IDA's action handlers.
    """

    def __init__(self, action_function, should_enable_function=None):
        idaapi.action_handler_t.__init__(self)
        self.action_function = action_function
        self.should_enable_function = should_enable_function

    def activate(self, ctx):
        """
        Execute the embedded action_function when this context menu is invoked.
        """
        self.action_function()
        return 1

    def update(self, ctx):
        """
        Ensure the context menu is always available in IDA.
        """
        #print('update',  self.should_enable_function,  self.should_enable_function(ctx) if self.should_enable_function != None else None)
        if self.should_enable_function != None:
            return idaapi.AST_ENABLE if self.should_enable_function(ctx) else idaapi.AST_DISABLE
        return idaapi.AST_ENABLE_ALWAYS

def register_actions():
  register_action('my:make_original', 'Change prefix to ORIGINAL', lambda : set_func_prefix_to_ORIGINAL(idaapi.get_screen_ea()))
  register_action('my:make_crippled', 'Change prefix to CRIPPLED', lambda : set_func_prefix_to_CRIPPLED(idaapi.get_screen_ea()))
  register_action('my:make_copied',   'Change prefix to COPIED',   lambda : set_func_prefix_to_COPIED(idaapi.get_screen_ea()))
  register_action('my:make_ongoing',  'Change prefix to ONGOING',  lambda : set_func_prefix_to_ONGOING(idaapi.get_screen_ea()))
  register_action('my:make_restored', 'Change prefix to RESTORED', lambda : set_func_prefix_to_RESTORED(idaapi.get_screen_ea()))
  register_action('my:make_verified', 'Change prefix to VERIFIED', lambda : set_func_prefix_to_VERIFIED(idaapi.get_screen_ea()))

  register_action('my:ask_address_and_retarget_absolute_target',                      'Retarget Relative Targetted Absolute Address', lambda : ask_address_and_retarget_absolute_target())
  register_action('my:ask_address_and_retarget_similar_absolute_targets_in_function', 'Retarget Similar Relative Targetted Absolute Addresses in Function', lambda : ask_address_and_retarget_similar_absolute_targets_in_function())
  register_action('my:ask_address_and_retarget_similar_absolute_targets_xrefs',       'Retarget Similar Relative Targetted Absolute Addresses (Using X-RefS)', lambda : ask_address_and_retarget_similar_absolute_targets_xrefs())
  register_action('my:ask_address_and_retarget_absolute_target_then_mark',                      'Retarget Relative Targetted Absolute Address + Mark as orange', lambda : ask_address_and_retarget_absolute_target(True))
  register_action('my:ask_address_and_retarget_similar_absolute_targets_in_function_then_mark', 'Retarget Similar Relative Targetted Absolute Addresses in Function + Mark as orange', lambda : ask_address_and_retarget_similar_absolute_targets_in_function(True))
  register_action('my:ask_address_and_retarget_similar_absolute_targets_xrefs_then_mark',       'Retarget Similar Relative Targetted Absolute Addresses (Using X-RefS) + Mark as orange', lambda : ask_address_and_retarget_similar_absolute_targets_xrefs(True))
  register_action('my:ask_address_and_retarget_absolute_target_then_confirm',                      'Retarget Relative Targetted Absolute Address + Mark as green', lambda : ask_address_and_retarget_absolute_target(True, True))
  register_action('my:ask_address_and_retarget_similar_absolute_targets_in_function_then_confirm', 'Retarget Similar Relative Targetted Absolute Addresses in Function + Mark as green', lambda : ask_address_and_retarget_similar_absolute_targets_in_function(True, True))

  register_action('my:print_counts_by_prefix',       'Print counts by prefix',  lambda : print_counts_by_prefix(get_workflow_funcs()))
  register_action('my:print_func_names_by_max_size', 'Print names by Max Size', lambda : print_func_names_by_max_size(get_workflow_funcs()))

  register_action('my:mark_as_default', 'Unmark Item',         lambda : mark_item_as_default(idaapi.get_screen_ea()))
  register_action('my:mark_as_orange',  'Mark Item as Orange', lambda : mark_item_as_orange(idaapi.get_screen_ea()))
  register_action('my:mark_as_red',     'Mark Item as Red',    lambda : mark_item_as_red(idaapi.get_screen_ea()))
  register_action('my:mark_as_green',   'Mark Item as Green',  lambda : mark_item_as_green(idaapi.get_screen_ea()))
  register_action('my:mark_as_white',   'Mark Item as White',  lambda : mark_item_as_white(idaapi.get_screen_ea()))

  register_action('my:jump_to_original', 'Jump to original',          lambda : jump_to_item_in_original(idaapi.get_screen_ea()))
  register_action('my:jump_to_address',  'Jump to address in remote', lambda : jump_to_address_in_remote(idaapi.get_screen_ea()))

def register_action(id, text, action_function, should_enable_function=None):
  action_desc = idaapi.action_desc_t(id, text, IDACtxEntry(action_function, should_enable_function))
  idaapi.register_action(action_desc)

def ask_address_and_retarget_absolute_target(mark_as_orange=False, mark_as_green=False):
  screen_ea = idaapi.get_screen_ea()
  abs_target = get_item_refered_absolute_target(screen_ea)
  if abs_target == None:
    return None

  new_target = get_name_as_addr(ida_kernwin.ask_str('', 0, 'Enter the new target address'))
  if new_target == idaapi.BADADDR:
    return None

  old = get_item_bytes(screen_ea) if mark_as_orange else b''
  update_item_as_instr_with_retargetted_absolute_target(new_target, screen_ea)
  if mark_as_orange and old != get_item_bytes(screen_ea):
    mark_item_as_orange(screen_ea)
  if mark_as_green and old != get_item_bytes(screen_ea):
    mark_item_as_green(screen_ea)

def ask_address_and_retarget_similar_absolute_targets_in_function(mark_as_orange=False, mark_as_green=False):
  func_ea = idaapi.get_screen_ea()
  abs_target = get_item_refered_absolute_target(func_ea)
  if abs_target == None:
    return None

  new_target = get_name_as_addr(ida_kernwin.ask_str('', 0, 'Enter the new target address'))
  if new_target == idaapi.BADADDR:
    return None

  retarget_similar_absolute_targets_in_function(new_target, mark_as_orange, mark_as_green)

def retarget_similar_absolute_targets_in_function(new_target, mark_as_orange=False, mark_as_green=False):
  if new_target == idaapi.BADADDR:
    return None

  func_ea = idaapi.get_screen_ea()
  abs_target = get_item_refered_absolute_target(func_ea)
  if abs_target == None:
    return None

  for ea in idautils.FuncItems(func_ea):
    if get_item_refered_absolute_target(ea) == abs_target:
      old = get_item_bytes(ea) if mark_as_orange else b''
      update_item_as_instr_with_retargetted_absolute_target(new_target, ea)
      # The commented code below ensure the functions are always marked
      if mark_as_orange: # and old != get_item_bytes(ea):
        mark_item_as_orange(ea)
      if mark_as_green: # and old != get_item_bytes(ea):
        mark_item_as_green(ea)

def ask_address_and_retarget_similar_absolute_targets_xrefs(mark_as_orange=False):
  func_ea = idaapi.get_screen_ea()
  abs_target = get_item_refered_absolute_target(func_ea)
  if abs_target == None:
    return None
  
  new_target = get_name_as_addr(ida_kernwin.ask_str('', 0, 'Enter the new target address'))
  if new_target == idaapi.BADADDR:
    return None

  all_xrefs = [ref.frm for ref in idautils.XrefsTo(abs_target)]
  for ea in all_xrefs:
    if get_item_refered_absolute_target(ea) == abs_target:
      old = get_item_bytes(ea) if mark_as_orange else b''
      update_item_as_instr_with_retargetted_absolute_target(new_target, ea)
      if mark_as_orange and old != get_item_bytes(ea):
        mark_item_as_orange(ea)

class AllHooks(idaapi.UI_Hooks):
    def populating_widget_popup(self, form, popup):
        pass
    
    def finish_populating_widget_popup(self, form, popup):
        if idaapi.get_widget_type(form) != idaapi.BWN_DISASM:
          return
        add_line(form, popup)
        idaapi.attach_action_to_popup(form, popup, "my:make_original", 'Restoration Workflow/')
        idaapi.attach_action_to_popup(form, popup, "my:make_crippled", 'Restoration Workflow/')
        idaapi.attach_action_to_popup(form, popup, "my:make_copied",   'Restoration Workflow/')
        idaapi.attach_action_to_popup(form, popup, "my:make_ongoing",  'Restoration Workflow/')
        idaapi.attach_action_to_popup(form, popup, "my:make_restored", 'Restoration Workflow/')
        idaapi.attach_action_to_popup(form, popup, "my:make_verified", 'Restoration Workflow/')
        add_line(form, popup, 'Restoration Workflow/')
        idaapi.attach_action_to_popup(form, popup, "my:mark_as_default", 'Restoration Workflow/')
        idaapi.attach_action_to_popup(form, popup, "my:mark_as_green",   'Restoration Workflow/')
        idaapi.attach_action_to_popup(form, popup, "my:mark_as_orange",  'Restoration Workflow/')
        idaapi.attach_action_to_popup(form, popup, "my:mark_as_red",     'Restoration Workflow/')
        idaapi.attach_action_to_popup(form, popup, "my:mark_as_white",   'Restoration Workflow/')
        add_line(form, popup, 'Restoration Workflow/')
        idaapi.attach_action_to_popup(form, popup, "my:print_func_names_by_max_size", 'Restoration Workflow/')
        idaapi.attach_action_to_popup(form, popup, "my:print_counts_by_prefix",       'Restoration Workflow/')
        
        idaapi.attach_action_to_popup(form, popup, "my:ask_address_and_retarget_absolute_target",                      'Code Rewriting/')
        idaapi.attach_action_to_popup(form, popup, "my:ask_address_and_retarget_similar_absolute_targets_in_function", 'Code Rewriting/')
        idaapi.attach_action_to_popup(form, popup, "my:ask_address_and_retarget_similar_absolute_targets_xrefs",       'Code Rewriting/')
        add_line(form, popup, 'Code Rewriting/')
        idaapi.attach_action_to_popup(form, popup, "my:ask_address_and_retarget_absolute_target_then_mark",                      'Code Rewriting/')
        idaapi.attach_action_to_popup(form, popup, "my:ask_address_and_retarget_similar_absolute_targets_in_function_then_mark", 'Code Rewriting/')
        idaapi.attach_action_to_popup(form, popup, "my:ask_address_and_retarget_similar_absolute_targets_xrefs_then_mark",       'Code Rewriting/')
        add_line(form, popup, 'Code Rewriting/')
        idaapi.attach_action_to_popup(form, popup, "my:ask_address_and_retarget_absolute_target_then_confirm",                      'Code Rewriting/')
        idaapi.attach_action_to_popup(form, popup, "my:ask_address_and_retarget_similar_absolute_targets_in_function_then_confirm", 'Code Rewriting/')
        
        idaapi.attach_action_to_popup(form, popup, "my:jump_to_original", 'Bridge Workflow/Jump to original')
        idaapi.attach_action_to_popup(form, popup, "my:jump_to_address", 'Bridge Workflow/Jump to address in remote')
        add_line(form, popup)

def add_line(form, popup, extra=None):
    idaapi.attach_action_to_popup(form, popup, None, extra)

hooks = AllHooks()
hooks.unhook()
register_actions()
hooks.hook()

