from collections import defaultdict

from .functions import get_func_prefix, get_func_name
from .function_analysis import get_func_max_size

def print_funcs_by_max_size(funcs, printer=None):
    if printer == None:
       printer = print
    for max_size, func_eas in sorted(get_funcs_by_max_size(funcs).items()):
       printer(max_size, ':', ', '.join(map(hex, func_eas)))

def print_func_names_by_max_size(funcs, printer=None):
    if printer == None:
       printer = print
    for max_size, func_eas in sorted(get_funcs_by_max_size(funcs).items()):
       printer(max_size, ':', ', '.join(map(get_func_name, func_eas)))

def print_counts_by_max_size(funcs, printer=None):
    if printer == None:
       printer = print
    for max_size, func_eas in sorted(get_funcs_by_max_size(funcs).items()):
       printer(max_size, ':', len(func_eas))

def print_count_and_names_by_max_size(funcs, max_names=4, printer=None):
    if printer == None:
       printer = print
    for max_size, func_eas in sorted(get_funcs_by_max_size(funcs).items()):
       if len(func_eas) > max_names:
         printer(max_size, ':', len(func_eas), ',', ', '.join(map(get_func_name, func_eas[:max_names])) + ', ...')
       else:
         printer(max_size, ':', len(func_eas), ',', ', '.join(map(get_func_name, func_eas)))

def print_funcs_by_prefix(funcs, printer=None):
    if printer == None:
       printer = print
    for prefix, func_eas in sorted(get_funcs_by_prefix(funcs).items()):
       printer(prefix, ':', ', '.join(map(hex, func_eas)))

def print_counts_by_prefix(funcs, printer=None):
    if printer == None:
       printer = print
    for prefix, func_eas in sorted(get_funcs_by_prefix(funcs).items()):
       printer(prefix, ':', len(func_eas))

def get_funcs_by_max_size(funcs):
    grouped = defaultdict(list)
    for func_ea in funcs:
      grouped[get_func_max_size(func_ea)].append(func_ea)
    return grouped

def get_funcs_by_prefix(funcs):
    grouped = defaultdict(list)
    for func_ea in funcs:
      grouped[get_func_prefix(func_ea)].append(func_ea)
    return grouped
