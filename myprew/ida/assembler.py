from binascii import hexlify
from keystone import *

def create_ks_x86(mode):
  return Ks(KS_ARCH_X86, mode)

def create_ks_x86_64():
  return create_ks_x86(KS_MODE_64)

def create_ks_x86_32():
  return create_ks_x86(KS_MODE_32)

def asm_instruction(ks, instr, addr=0):
  if '[rel ' in instr:
    instr = '_rip:' + instr.replace('[rel ', '[rip+_rip+').replace(']', '-__rip]')
  instr = instr.replace('__rip', str(addr))
  b = ks.asm(instr, addr=addr, as_bytes=True)[0]
  return b
