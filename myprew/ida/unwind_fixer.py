import idaapi

def is_code_structure_at(ea):
  flags = idaapi.get_flags(ea)
  if idaapi.is_struct(flags):
    ti = idaapi.opinfo_t()
    if idaapi.get_opinfo(ti, ea, 0, flags):
      return ti.tid == code_sid
  return False

def fix_unwind_data():
 code_sid = idaapi.get_struc_id('UNWIND_CODE')
 code_struct = idaapi.get_struc(code_sid)
 code_size = idaapi.get_struc_size(code_struct)
 
 sid = idaapi.get_struc_id('UNWIND_INFO_HDR')
 struct = idaapi.get_struc(sid)
 size = idaapi.get_struc_size(struct)
 if size == 4:
  member_flags = idaapi.get_member_by_name(struct, 'Ver3_Flags')
  member_count = idaapi.get_member_by_name(struct, 'CntUnwindCodes')
  for xref in idautils.XrefsTo(sid):
    code_count = idaapi.get_byte(xref.frm + member_count.soff)
    #print(hex(xref.frm), hex(idaapi.get_word(xref.frm + member_flags.soff)), code_count)
    for i in range(code_count):
      code_ea = xref.frm + size + code_size * i
      if not is_code_structure_at(code_ea):
        if idaapi.is_word(idaapi.get_flags(code_ea)):
          print('Converted ' + hex(code_ea))
          idaapi.create_struct(code_ea, code_size, code_sid, True)
        else:
          print('Requires attention at ' + hex(code_ea))
    
    flags = idaapi.get_byte(xref.frm + member_flags.soff)
    data_ea = xref.frm + size + code_size * code_count
    if code_count % code_size > 0:
      data_ea += code_size
    
    if flags == 0x1:
      end_ea = data_ea
    elif flags in [0x9, 0x19]:
      end_ea = data_ea + 4
    elif flags == 0x11:
      end_ea = data_ea + 8
    elif flags == 0x21:
      end_ea = data_ea + 12
    else:
      raise NotImplementedError('Unknown at ' + hex(xref.frm) + ', flags='+hex(flags))
    if flags in [0x9, 0x19]:
      line = idc.GetDisasm(end_ea)
      if ' rva ' in line:
        if not line.startswith('dd rva '):
          print('Unable to fix ' + hex(end_ea) + ', ' + line)
        else:
          rva = idaapi.get_dword(end_ea)
          if get_segment_name(idaapi.get_imagebase() + rva) != '.rsrc':
            if rva < 0x1000:
              if False:
                print('Redefined ' + hex(end_ea) + ', ' + line)
            else:
              print('Redefined suspicious ' + hex(end_ea) + ', ' + line)		  
            idaapi.create_dword(end_ea, 4, True)
