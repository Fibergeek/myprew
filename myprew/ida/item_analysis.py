import pydis
import idaapi

from .items import is_item_code, get_item_bytes, get_item_size
from .assembler import create_ks_x86_64, asm_instruction

def decode_x86_64(buffer, addr):
  return pydis.decode(buffer, address=addr, mode=pydis.MachineMode.Long64)

def get_decoded_item(ea=None):
  if ea == None:
    ea = idaapi.get_screen_ea()
  try:
      return next(decode_x86_64(get_item_bytes(ea), ea))
  except StopIteration:
      raise Exception('Unable to decode {0}-byte instruction at {1}'.format(len(get_item_bytes(ea)), hex(ea)))

def operand_as_text(instr, op_index):
  return pydis.formatter.Formatter().format_operand(instr.underlying_type, op_index)

def is_instr_rip_relative(instr):
  return get_instr_rip_relative_op(instr) != None

def get_instr_rip_relative_op(instr):
  ops = []
  for op in instr.operands:
    if op.type == pydis.OperandType.Memory:
      if op.memory.base == pydis.Register.RIP:
        ops.append(op)
    elif op.type == pydis.OperandType.Immediate:
      if pydis.InstructionAttribute.Is_Relative in instr.attributes:
        ops.append(op)
  if len(ops) > 2:
    raise NotImplementedError("opcount == " + str(len(ops)))
  elif len(ops) == 2:
    if ops[0].type != pydis.OperandType.Memory or ops[1].type != pydis.OperandType.Immediate:
      raise NotImplementedError("opcount == 2")
  return ops[0] if ops else None

def is_item_rip_relative(ea=None):
  return get_item_rip_relative_op(ea) != None

def get_item_rip_relative_op(ea=None):
  # TODO: add support for FixUps
  if not is_item_code(ea):
    return None
  return get_instr_rip_relative_op(get_decoded_item(ea))

def operand_as_text(op):
  return pydis.formatter.Formatter().format_operand(op._instruction, op._index)

def get_instr_refered_absolute_target(instr):
  op = get_instr_rip_relative_op(instr)
  if op == None:
    return None
  addr = operand_as_text(op).replace('[', '').replace(']', '')
  if 'ptr ' in addr:
    _, _, addr = addr.partition('ptr ')
  return int(addr[2:], 16)

def get_item_refered_absolute_target(ea=None):
  if not is_item_rip_relative(ea):
    return None
  return get_instr_refered_absolute_target(get_decoded_item(ea))

def get_instr_with_retargetted_absolute_target(new_target, instr, ks):
  if not isinstance(new_target, int):
    raise ValueError('new_target should be an int')
  op = get_instr_rip_relative_op(instr)
  if op == None:
    return None
  addr_text = operand_as_text(op).replace('[', '').replace(']', '')
  if 'ptr ' in addr_text:
    _, _, addr_text = addr_text.partition('ptr ')
  new_instr_text = str(instr).replace(addr_text, str(new_target)).replace('[', '[rel ')
  new_instr_bytes = asm_instruction(ks, new_instr_text, addr=instr.address)
  return next(pydis.decode(new_instr_bytes, address=instr.address, mode=instr.machine_mode, address_width=instr.address_width))

def get_item_as_instr_with_retargetted_absolute_target(new_target, ea=None, ks=None):
  if ks == None:
    ks = create_ks_x86_64() # TODO : implement for other assemblers
  if not is_item_rip_relative(ea):
    return None
  return get_instr_with_retargetted_absolute_target(new_target, get_decoded_item(ea), ks)

def update_item_as_instr_with_retargetted_absolute_target(new_target, ea=None, ks=None):
  if ea == None:
    ea = idaapi.get_screen_ea()
  new_instr = get_item_as_instr_with_retargetted_absolute_target(new_target, ea, ks)
  if new_instr != None:
    if get_item_size(ea) != len(new_instr.bytes):
      raise NotImplementError('Current Size ({0}) != New Size ({1})'.format(get_item_size(ea), len(new_instr.bytes)))
    idaapi.del_items(ea)
    for i, b in enumerate(new_instr.bytes):
      idaapi.patch_byte(ea + i, b)
    idaapi.create_insn(ea)

def get_item_prefixes_and_opcode(item_ea):
  if not is_item_code(item_ea):
    return b''
  item_bytes = get_item_bytes(item_ea)
  decoded = get_decoded_item(item_ea)
  result = b''
  for b in item_bytes:
    result += bytes([b])
    if b == decoded.opcode:
      break
  return result

def get_item_prefixes(item_ea):
  return get_item_prefixes_and_opcode(item_ea)[:-1]
