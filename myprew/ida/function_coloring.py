from collections import defaultdict

from .functions import get_func_items
from .item_coloring import is_item_colorized, is_item_colored_orange, is_item_colored_red

def get_colorized_func_items(item_ea):
  return filter(is_item_colorized, get_func_items(item_ea))

def get_red_func_items(item_ea):
  for item_ea in get_func_items(item_ea):
    if is_item_colored_red(item_ea):
      yield item_ea

def get_orange_func_items(item_ea):
  for item_ea in get_func_items(item_ea):
    if is_item_colored_orange(item_ea):
      yield item_ea

def get_func_color_stats(item_ea):
  counts = defaultdict(lambda : 0)
  for item_ea in get_func_items(item_ea):
    if is_item_colored_orange(item_ea):
      counts['o'] += 1
    if is_item_colored_red(item_ea):
      counts['r'] += 1
  return counts
