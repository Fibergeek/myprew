import idautils

def get_xrefs_to_related_xref(ea):
  xrefs = list(set([xref.to for xref in idautils.XrefsFrom(ea, 1)]))
  if len(xrefs) != 1:
    return
  xrefs = sorted(set([xref.frm for xref in idautils.XrefsTo(xrefs[0], 1)]))
  for frm in xrefs:
    yield frm

def get_distances_to_related_xref(center_ea):
  for ea in get_xrefs_to_related_xref(center_ea):
    yield ea - center_ea

def get_surrounding_distances_to_related_xref(center_ea, range=3):
  real_list = [ea for ea in get_distances_to_related_xref(center_ea)]
  abs_list = [abs(ea) for ea in real_list]
  if not abs_list:
    return []
  center = abs_list.index(0)
  left  = max(center - range, 0)
  right = center + range + 1
  return real_list[left:right]

def get_surrounding_xrefs_to_related_xref(center_ea, range=3):
  for distance in get_surrounding_distances_to_related_xref(center_ea, range):
    yield center_ea + distance
