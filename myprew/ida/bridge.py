import idaapi
import sys

try:
  import jfx_bridge_ida
except ImportError:
  jfx_bridge_ida = None

def ensure_bridge_to_original(host=None, port=None):
    return ensure_bridge(host, port)

def ensure_bridge(host=None, port=None):
    if jfx_bridge_ida == None:
        raise ImportError
    
    if port == None:
        port = jfx_bridge_ida.jfx_bridge_ida.bridge.DEFAULT_SERVER_PORT
    host, port = host or 'localhost', port or ''
    if host != 'localhost' and host != '127.0.0.1':
        raise NotImplementedError('Only localhost is supported!')
    host_port_pair = host, port
    
    if 'myprew__open_bridges' in dir(sys):
        if host_port_pair in sys.myprew__open_bridges:
            return sys.myprew__open_bridges[host_port_pair]
    else:
        sys.myprew__open_bridges = {}
    
    print('Opening a bridge to {0} {1}'.format(host, port))
    bridge_original = jfx_bridge_ida.IDABridge(connect_to_port=port, response_timeout=10)
    bridge_original.remote_exec('import myprew')
    sys.myprew__open_bridges[host_port_pair] = bridge_original
    return bridge_original

def jump_to_bridged_item(bridge, target_ea):
  if target_ea == idaapi.BADADDR:
    return False
  bridge.remote_exec('jumpto({0})'.format(hex(target_ea)))
  return True
