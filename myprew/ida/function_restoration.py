from ida_bytes import get_original_byte, get_original_dword
from ida_segment import get_segm_by_name, getseg, get_segm_name
from myprew import get_func_start_ea, is_workflow_name, set_func_prefix_to, set_item_prefix_to, get_item_ea, get_item_size
from myprew import get_crippled_funcs, is_func_of_max_size
from .function_analysis import is_func_followed_by_legit_func, get_func_original_guessed_size, get_func_guessed_size
from .function_workflow import get_workflow_funcs
from .memory import get_bytes
from .utils import generate_disasm_line, get_input_file_md5_as_str
from .items import is_item_code, get_item_size

import idc
import idautils

from .functions import get_func_size

from .function_analysis import get_func_max_size
from .function_workflow import set_func_prefix_to_COPIED, get_workflow_funcs

from .item_analysis import decode_x86_64

def get_crippled_funcs_of_max_size(size):
  for ea in get_crippled_funcs(filter=lambda ea: is_func_of_max_size(ea, size)):
    yield ea

def print_crippled_funcs_of_max_size(max_size):
  for func_ea in get_crippled_funcs_of_max_size(max_size):
    prev_func_ea = get_prev_func_start_ea(func_ea)
    next_func_ea = get_next_func_start_ea(func_ea)
    
    prev_max_size = get_func_max_size(prev_func_ea)
    next_max_size = get_func_max_size(next_func_ea)
    
    if prev_max_size == max_size and next_max_size == max_size:
      commons = get_func_common_XrefsFrom(prev_func_ea, next_func_ea)
      commons_report = ' COMMONS: ' + ', '.join([idc.get_name(common_ea) for common_ea in commons])
    else:
      commons = []
      commons_report = ''
   
    prev_report = get_func_name(prev_func_ea) + ': ' + str(prev_max_size).rjust(5)
    next_report = get_func_name(next_func_ea) + ': ' + str(next_max_size).rjust(5)
    #print(get_func_name(func_ea), ':', prev_report, next_report)
    print(prev_report, '<<<', get_func_name(func_ea), '>>>', next_report + commons_report)

def print_intersegment_jumps_or_calls_and_mark_as_SPREWED(name_frm, name_to, alignment_from=16, alignment_to=1, allow_renaming=False, print_protected_only=False, print_in_knowns=False, print_confirmed=False, print_unlikely=False):
    results = get_intersegment_jumps_or_calls(name_frm, name_to, alignment_from, alignment_to, allow_renaming, print_in_knowns, print_unlikely)
    for ea, head_ea, func_addr, func_name, target_ea, reason in results:
        if reason != 'confirmed' or print_confirmed:
            target_bytes = idc.get_bytes(target_ea, 15 * 3) # 3 instructions of each max 15 bytes
            target_decoded = decode_x86_64(target_bytes, target_ea)
            target_instr = []
            target_type = 'Normal'
            for i in range(3):
                try:
                    instr = str(next(target_decoded))
                    target_instr.append(instr)
                    if instr.startswith('iret') or instr.startswith('retf') or instr.startswith('syscall') or instr.startswith('int3'):
                        break # Illigal
                    if instr.startswith('jmp') or instr.startswith('ret'):
                        if instr.startswith('jmp 0x'):
                          target_type = 'Most Likely Protected'
                        break # Legal (short functions)
                    if instr.startswith('lea rsp') or instr.startswith('pushf'):
                        target_type = 'Protected'
                except KeyboardInterrupt:
                    raise
                except: # DecodingError?
                    target_type = 'False Positive'
                    break
            decoded_string = ' / '.join(target_instr)
            
            if target_type.endswith('Protected') if print_protected_only else True:
                print(hex(ea), '-->', func_name, '--> SPREWED', hex(target_ea), 'reason:', reason, '-->', decoded_string, '==>', target_type)
                if (reason == 'confirmed' or reason == 'being renamed') and not print_protected_only:
                    if not idc.get_name(target_ea).startswith('SPREWED_'):
                        set_item_prefix_to(target_ea, 'SPREWED')
                        idc.set_cmt(target_ea, 'SPREW OF ' + hex(ea), 1)
                        idc.set_cmt(ea, 'SPREW AT ' + hex(target_ea), 1)

def get_intersegment_jumps_or_calls(name_frm, name_to, alignment_from=16, alignment_to=1, allow_renaming=False, print_in_knowns=False, print_unlikely=False):
 if isinstance(name_to, str):
  is_seg_match = lambda seg: get_segm_name(seg) == name_to
 else:
  is_seg_match = lambda seg: get_segm_name(seg) in name_to
 if alignment_from <= 0:
  raise ValueError('alignment_from')
 if alignment_to <= 0:
  raise ValueError('alignment_to')
 
 text_seg = get_segm_by_name(name_frm)
 if text_seg != None:
  for ea in range(text_seg.start_ea, text_seg.end_ea):
   b = get_original_byte(ea)
   if b in [0xE8, 0xE9] and ((ea) % alignment_from == 0):
    d = get_original_dword(ea + 1)
    if d < 0x80000000: # TODO: this assumes that ea_frm is always lower then ea_to
      target = ea + d + 5
      seg = getseg(target) if target % alignment_to == 0 else None
      if seg != None:
       if is_seg_match(seg):
        func_name = get_func_name(ea)
        func_addr = get_func_start_ea(ea)
        head_ea = get_item_ea(ea)
        if (idc.get_name(head_ea) or '').split('_')[0] in ['jpt', 'JUMPTABLE']:
          reason = 'in jumptable (?)'
        elif ea != head_ea:
          if not idc.is_code(idc.get_full_flags(head_ea)):
            reason = 'in data'
          elif ea >= head_ea + get_item_size(head_ea):
            reason = 'data'#'post instruction data'
          elif not func_name:
            reason = 'in anonymous instruction'
          elif print_in_knowns and not print_unlikely:
            reason = 'in known instruction'
          else:
            reason = ''
        elif not idc.is_code(idc.get_full_flags(ea)):
          reason = 'data'
        elif not func_name:
          if False: # TODO: check if the previous head is also an instruction which is not JMP/RETN/RETF/IRET
            reason = 'anonymous code'
          else:
            reason = 'in anonymous code'
        elif is_workflow_name(func_name) and func_addr == ea:
          reason = 'confirmed'
        elif is_workflow_name(func_name):
          reason = 'inside a restored branch'
        elif func_addr == ea:
          reason = 'incorrectly named'
          if allow_renaming:
            set_func_prefix_to(func_addr, 'CRIPPLED')
            reason = 'being renamed'
        elif head_ea == ea:
          reason = 'not a function yet'
        else:
          reason = ''
        if reason:
          yield ea, head_ea, func_addr, func_name, target, reason
        elif print_unlikely:
          reason = 'inside code (+' + hex(ea - func_addr) + ' / +' + str(ea - head_ea) + ')'
          print(hex(ea), '-->', func_name, '-->', hex(target), 'reason:', reason, '-->', idautils.GetDisasm(ea))

def print_workflow_funcs_as_c_case():
  """This function is to be used at the end of the workflow."""
  """It will print a C switch statement to test against the SPREWED functions."""
  print('switch(img_va)') 
  print('{')
  targets = {}
  for func in get_workflow_funcs():
    if get_original_byte(func) in [0xE8, 0xE9]:
      targets[func + get_original_dword(func + 1) + 5] = func
  for target, source in sorted(targets.items()):
    print('  case ' + hex(target) + ': // ' + hex(source) + ' ' + get_func_name(source))
  print('} // verified = ' + str(len(get_verified_funcs())) + ' / ' + str(len(targets)) + ' addresses')

def make_crippled(ea):
  if ida_funcs.get_fchunk_num(ea) != -1:
    while ida_funcs.get_fchunk_num(ea) != -1:
      ida_funcs.remove_func_tail(ida_funcs.get_func(ea), ea)
  ida_funcs.add_func(ea, idc.BADADDR)
  set_func_prefix_to_CRIPPLED(ea)

#make_crippled(get_screen_ea())

from .bridge import ensure_bridge_to_original
from .bridge_workflow import get_matching_xrefs_to_related_xref
from .function_analysis import get_func_dotted_ascii
from .function_workflow import get_workflow_funcs, get_copied_funcs, get_ongoing_funcs, update_func_prefix, get_original_item_ea, get_unresolved_func_items
from .functions import get_funcs, get_func_name
from .item_analysis import update_item_as_instr_with_retargetted_absolute_target
from .item_coloring import mark_item_as_orange

def resolve_xref_using_original(bridge_to_original, item_ea):
  results = get_matching_xrefs_to_related_xref(bridge_to_original, item_ea)
  print(dict([(hex(key), value) for key, value in results.items()]))
  results = sorted(results.items(), key=lambda kv: kv[1], reverse=True)

  results_other_than_one = [r for r in results if r[1] != 1]
  if results_other_than_one:
    results = results_other_than_one

  if len(results) == 1:
    # More than 3, very good, we assume this is correct
    if results[0][1] > 3:
      retarget_item = True
    else:
      original_ea = get_original_item_ea(item_ea)
      original_xref_to, = bridge_to_original.remote_eval('[xref.to for xref in XrefsFrom({0}, 1)]'.format(original_ea)) # We require 1 result !
      #print('xref_to original =', hex(original_xref_to), 'versus local =' , hex(results[0][0]))
      original_dotted_ascii = bridge_to_original.remote_eval('myprew.get_func_dotted_ascii(myprew.get_func_start_ea({0}))'.format(original_xref_to))
      expected_dotted_ascii = get_func_dotted_ascii(results[0][0])
      retarget_item = original_dotted_ascii == expected_dotted_ascii
    
    if retarget_item:
      update_item_as_instr_with_retargetted_absolute_target(results[0][0], item_ea)
      mark_item_as_orange(item_ea)
      return True
  else:
    pass
  return False

def resolve_unresolved_xrefs_of_func_items(ea, printer=None):
    if printer == None:
       printer = print

    bridge_original = ensure_bridge_to_original()
    for item_ea, original_item_ea in get_unresolved_func_items(ea):
      printer(resolve_xref_using_original(ensure_bridge_to_original(), item_ea))

    update_func_prefix(ea)

def resolve_unresolved_xrefs_workflow_funcs(printer):
  if printer == None:
    printer = print
  resolve_unresolved_xrefs_of_copied_funcs(printer)
  resolve_unresolved_xrefs_of_ongoing_funcs(printer)

def resolve_unresolved_xrefs_of_copied_funcs(printer=None):
  for func_ea in get_copied_funcs():
    if printer:
      printer('Resolving XRefs in', get_func_name(func_ea), 'at', hex(func_ea))
    resolve_unresolved_xrefs_of_func_items(func_ea)  

def resolve_unresolved_xrefs_of_ongoing_funcs(printer=None):
  for func_ea in get_ongoing_funcs():
    if printer:
      printer('Resolving XRefs in', get_func_name(func_ea), 'at', hex(func_ea))
    resolve_unresolved_xrefs_of_func_items(func_ea)

def search_func_dotted_ascii_in_text_segment(to_search):
  for func_ea in filter(lambda func_ea: get_segm_name(func_ea) == '.text', get_funcs()):
    if get_func_dotted_ascii(func_ea) == to_search:
     print('Match', hex(func_ea))
     yield hex(func_ea)

#print(list(search_func_dotted_ascii_in_text_segment(to_search)))

def print_invalid_funcs_following_workflow_funcs(printer=None):
    if printer == None:
       printer = print

    for func_ea in get_workflow_funcs():
        if not is_func_followed_by_legit_func(func_ea):
           printer(hex(func_ea))
        else:
           chunk_end_ea = ida_funcs.get_fchunk(func_ea).end_ea
           while True:
              if ida_funcs.get_fchunk(chunk_end_ea) != None:
                 break
              if is_item_code(chunk_end_ea):
                printer(hex(chunk_end_ea))
                break
              if not get_item_size(chunk_end_ea):
                printer(hex(chunk_end_ea) + ' (item_size == 0?)')
                break
              chunk_end_ea += get_item_size(chunk_end_ea)

def get_copyable_crippled_funcs():
    for func_ea in get_crippled_funcs():
        cmt = idc.get_func_cmt(func_ea, 0)
        if 'SEE ORIGINAL ' in cmt:
            origin = cmt.split('SEE ORIGINAL ')[1].split('\n')[0].strip()
            yield func_ea, origin
        elif 'ORIGINAL:' in cmt:
            origin = cmt.split('ORIGINAL:')[1].split('\n')[0].strip()
            yield func_ea, origin

import ida_funcs
import ida_auto
import idc

def remove_NORET_flag(funcs):
    for ea in funcs:
        flags = idc.get_func_flags(ea)
        idc.set_func_flags(ea, flags & ~ida_funcs.FUNC_NORET)

def has_NORET(ea):
    flags = idc.get_func_flags(ea)
    return bool(flags & ida_funcs.FUNC_NORET)

def copy_crippled_funcs_from_original(*bridges_old):
 if not bridges_old:
   bridges_old = [ensure_bridge_to_original()]

 if len(bridges_old) != 1:
   raise NotImplementedError('Only one bridge is supported for now!')

 b_old = bridges_old[0]

 # raise NotImplementedError('TODO: this function is broken because comments are lost!')

 my_hash = get_input_file_md5_as_str()

 for new_ea, origin in get_copyable_crippled_funcs():
   name = get_func_name(new_ea)
   if '.' in origin and not '.0x'in origin:
     origin = origin.replace('.', '.0x')
   
   if '0x' in origin:
    old_hash = origin.split('0x')[0].replace('.', '')
    if not old_hash.strip():
      raise NotImplementedError('No hash specified! Please upgrade the comment!')
    if old_hash.upper() in ['THIS', 'SELF']:
      print(hex(new_ea), 'ORIGINAL:', origin, 'COPY NOT YET SUPPORTED!')
      continue
    bridged_hash = b_old.remote_eval(' myprew.get_input_file_md5_as_str() ')
    if bridged_hash == old_hash:
     old_ea = int(origin.split('0x')[1], 16)
     old_space = b_old.remote_eval(' myprew.get_func_max_size({0}) '.format(old_ea))
     new_space = get_func_max_size(new_ea)
     
     if old_space == new_space:      
      print(hex(new_ea), 'ORIGINAL:', origin, hex(new_space), '==', hex(old_space))
      
      old_bytes = b_old.remote_eval(' myprew.get_bytes({0}, {1}) '.format(old_ea, old_space)) # idc_old.get_bytes(old_ea, old_space)
      ida_auto.enable_auto(False)
      
      # Store the comments
      cmt0 = idc.get_func_cmt(new_ea, 0)
      cmt1 = idc.get_func_cmt(new_ea, 1)

      # Delete any function and code
      idc.del_func(new_ea)
      idc.del_items(new_ea, new_space)

      # Patch
      for i in range(old_space):
        _ = idc.patch_byte(new_ea + i, old_bytes[i])
        if get_bytes(new_ea + i, 1)[0] != old_bytes[i]:
           ida_auto.enable_auto(True)
           raise Exception('Unable to patch byte at 0x{0}'.format(new_ea + i))

      # Patching can trigger code detection etc. Let's delete anything which got disassembled - to be sure
      idc.del_func(new_ea)
      idc.del_items(new_ea, new_space)

      # Now we create a new function
      # We try 3 times, not sure why, but IDA doesn't always succeed on the first try
      ida_auto.enable_auto(True)
      for i in range(3):
          ida_funcs.add_func(new_ea, idc.BADADDR)
          ida_auto.auto_wait()
          if get_func_start_ea(new_ea) != idc.BADADDR:
              break
      if get_func_start_ea(new_ea) != idc.BADADDR:
          # Restore the comments      
          idc.set_func_cmt(new_ea, cmt0, 0)
          idc.set_func_cmt(new_ea, cmt1, 1)
          set_func_prefix_to_COPIED(new_ea)
          if get_func_original_guessed_size(new_ea) != get_func_guessed_size(new_ea):
            print('WARNING! A mismatch in the guessed size:', get_func_original_guessed_size(new_ea), '(original) != ', get_func_guessed_size(new_ea), '(copied)')
      else:
          print('Auto-function creation failed for ' + hex(new_ea))
     else:
       print(hex(new_ea), 'ORIGINAL:', origin, hex(new_space), '!=', hex(old_space), 'SIZE MISMATCH')
    else:
      pass#print(hex(new_ea), 'ORIGINAL:', origin, 'IGNORED DUE TO HASH MISMATCH!')
   else:
     print(hex(new_ea), 'ORIGINAL:', origin, 'CANNOT BE PARSED!')

def get_verification_comments_for_functions(expected_size=0):
  for func_ea in get_workflow_funcs():
    cmt = idc.get_func_cmt(func_ea, 0)
    if 'VERIFICATION' in cmt:
      if expected_size == 0 or expected_size == get_func_size(func_ea):
        yield func_ea, cmt

def print_verification_comments_for_functions(expected_size=0, ignore_func_eas=None):
  if not ignore_func_eas:
    ignore_func_eas = []
  for func_ea, comments in get_verification_comments_for_functions(expected_size):
    if not func_ea in ignore_func_eas:
      print(hex(func_ea) + ', SIZE=' + str(get_func_size(func_ea)))
      print('  ' + comments.replace('\n', '\n  '))

def print_verification_comments_for_functions_of_current_size():
  current_func_ea = idc.get_screen_ea()
  if current_func_ea == idc.BADADDR:
    return
  
  current_func_ea = get_func_start_ea(current_func_ea)
  if current_func_ea == idc.BADADDR:
    return

  func_size = get_func_size(current_func_ea)
  if func_size != 0:
    print_verification_comments_for_functions(func_size, [current_func_ea])

def print_restoration_hashes(print_details=False, printer=None):
  if printer == None:
    printer = print
  
  data = set()
  for ea in get_workflow_funcs():
    cmt = idc.get_func_cmt(ea, 0)
    lines = [line.strip() for line in cmt.split('\n') if line.startswith('ORIGINAL:')]
    original = lines[0].split(' ')[-1].split('.')[0]
    if print_details:    
      printer(hex(ea), original)
    data.add(original)
  
  for original in sorted(data):
    printer(original)

def get_origin_comments_for_functions():
  for func_ea in get_workflow_funcs():
    cmt = idc.get_func_cmt(func_ea, 0)
    yielded = False
    for line in [line.strip() for line in cmt.split('\n')]:
      if line.startswith('ORIGINAL'):
        yield func_ea, line
        yielded = True
      elif line.strip().startswith('MISMATCH'):
        yield func_ea, line
        yielded = True
      elif line.strip().startswith('ALTERNATIVE'):
        yield func_ea, line
        yielded = True
      elif line.strip().startswith('SEE ') or line.strip().startswith('SEE:'):
        yield func_ea, line
        yielded = True
    if not yielded:
       yield func_ea, ''

def print_origin_comments_for_functions(printer=None):
  if printer == None:
    printer = print
  
  for ea, cmt_line in get_origin_comments_for_functions():
    printer(hex(ea), cmt_line)
