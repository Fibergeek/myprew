import myprew

bridge = myprew.ensure_bridge_to_original()
remote_workflow = bridge.remote_eval('list(myprew.get_workflow_funcs())')

for func_ea in remote_workflow:
  extra = [] 
  if func_ea != myprew.get_func_start_ea(func_ea):
    extra.append('START_EA')
  remote_func_size = bridge.remote_eval('myprew.get_func_size({0})'.format(func_ea))
  if remote_func_size != myprew.get_func_size(func_ea):
    extra.append('FUNC_SIZE')
  if not extra:
    remote_funcbytes = bridge.remote_eval('myprew.get_func_bytes({0})'.format(func_ea))
    func_bytes = myprew.get_func_bytes(func_ea)
    if remote_funcbytes != func_bytes:
      extra.append('FUNC_BYTES')
  if extra:
    print(hex(func_ea), myprew.get_func_name(func_ea), str(myprew.get_func_size(func_ea)).rjust(4), ', '.join(extra))
  if 'FUNC_BYTES' in extra:
    for i, (b, r) in enumerate(zip(func_bytes, remote_funcbytes)):
      if b != r:
        print('  ' + hex(func_ea + i))
