import re

from idc import BADADDR, get_func_cmt

from .item_analysis import get_item_refered_absolute_target
from .item_workflow import set_item_prefix_to
from .functions import get_func_start_ea, get_func_name, get_funcs, get_func_items
from .item_coloring import is_item_colorized, mark_item_as_orange, mark_item_as_red
from .utils import is_workflow_name
from .function_coloring import get_func_color_stats, get_red_func_items

def set_func_prefix_to(item_ea, new_prefix):
    """
    Sets the prefix of a function
    If the function name contains an underscore, the first part is replaced.
    If the function name does not contain an underscore, one is added.
    """
    start_ea = get_func_start_ea(item_ea)
    if start_ea == BADADDR:
        return False
    return set_item_prefix_to(start_ea, new_prefix)

def set_func_prefix_to_ORIGINAL(ea):
    """Change the prefix of a function to ORIGINAL"""
    return set_func_prefix_to(ea, 'ORIGINAL')

def set_func_prefix_to_CRIPPLED(ea):
    """Change the prefix of a function to CRIPPLED"""
    return set_func_prefix_to(ea, 'CRIPPLED')

def set_func_prefix_to_COPIED(ea):
    """Change the prefix of a function COPIED"""
    return set_func_prefix_to(ea, 'COPIED')

def set_func_prefix_to_ONGOING(ea):
    """Change the prefix of a function to ONGOING"""
    return set_func_prefix_to(ea, 'ONGOING')

def set_func_prefix_to_RESTORED(ea):
    """Change the prefix of a function to RESTORED"""
    return set_func_prefix_to(ea, 'RESTORED')

def set_func_prefix_to_VERIFIED(ea):
    """Change the prefix of a function to VERIFIED"""
    return set_func_prefix_to(ea, 'VERIFIED')

def get_original_funcs(filter=None):
    return [ea for ea in get_funcs(filter) if get_func_name(ea).startswith('ORIGINAL_')]

def get_crippled_funcs(filter=None):
    return [ea for ea in get_funcs(filter) if get_func_name(ea).startswith('CRIPPLED_')]

def get_copied_funcs(filter=None):
    return [ea for ea in get_funcs(filter) if get_func_name(ea).startswith('COPIED_')]

def get_ongoing_funcs(filter=None):
    return [ea for ea in get_funcs(filter) if get_func_name(ea).startswith('ONGOING_')]

def get_restored_funcs(filter=None):
    return [ea for ea in get_funcs(filter) if get_func_name(ea).startswith('RESTORED_')]

def get_verified_funcs(filter=None):
    return [ea for ea in get_funcs(filter) if get_func_name(ea).startswith('VERIFIED_')]

def get_workflow_funcs(filter=None):
    """Returns all functions which are related to our workflow as a list."""
    return [ea for ea in get_funcs(filter) if is_workflow_func(ea)]

def is_workflow_func(item_ea): 
    """
    Returns True if the function name contains a prefix related to our workflow.
    Otherwise False is returned.
    """
    return is_workflow_name(get_func_name(item_ea))

def colorize_workflow_functions(printer=None):
  colorize_copied_functions(printer)
  colorize_ongoing_functions(printer)
  colorize_restored_functions(printer) # TODO: do we really need this ???

def colorize_copied_functions(printer=None):
  for function_ea in get_copied_funcs():
   if printer != None:
     printer('Colorizing', hex(function_ea))
   for item_ea in get_func_items(function_ea):
     if not is_item_colorized(item_ea):
       target_ea = get_item_refered_absolute_target(item_ea)
       if target_ea != None and get_func_start_ea(target_ea) != function_ea:
         mark_item_as_red(item_ea)

def colorize_ongoing_functions(printer=None):
  for function_ea in get_ongoing_funcs():
   if printer != None:
     printer('Colorizing', hex(function_ea))
   for item_ea in get_func_items(function_ea):
     if not is_item_colorized(item_ea):
       target_ea = get_item_refered_absolute_target(item_ea)
       if target_ea != None and get_func_start_ea(target_ea) != function_ea:
         mark_item_as_red(item_ea)

def colorize_restored_functions(printer=None):
  for function_ea in get_restored_funcs():
   if printer != None:
     printer('Colorizing', hex(function_ea))
   for item_ea in get_func_items(function_ea):
     if not is_item_colorized(item_ea):
       target_ea = get_item_refered_absolute_target(item_ea)
       if target_ea != None and get_func_start_ea(target_ea) != function_ea:
         mark_item_as_orange(item_ea)

def get_original_func_ea(item_ea):
  start_ea = get_func_start_ea(item_ea)
  if not is_workflow_func(start_ea):
    return BADADDR
  if get_func_name(start_ea).startswith('ORIGINAL_'):
    return start_ea
  comment = get_func_cmt(start_ea , 1) or get_func_cmt(start_ea, 0)

  # Old version of the workflow
  result = re.search('SEE\s*0x([0-9a-fA-F]*)', comment)
  if result != None:
    result = int(result.groups()[0], 16)
    return result

  # New version of the workflow which support hashes
  result = re.search('SEE ORIGINAL .*0x([0-9a-fA-F]*)', comment)
  if result != None:
    result = int(result.groups()[0], 16)
    return result

  # Newest version with 0x of the workflow which support hashes
  result = re.search('ORIGINAL: .*\.0x([0-9a-fA-F]*)', comment)
  if result != None:
    result = int(result.groups()[0], 16)
    return result

  # Newest version without 0x of the workflow which support hashes
  result = re.search('ORIGINAL: .*\.([0-9a-fA-F]*)', comment)
  if result != None:
    result = int(result.groups()[0], 16)
    return result

  return BADADDR

def get_original_item_ea(item_ea):
  original_func_ea = get_original_func_ea(item_ea)
  if original_func_ea == BADADDR:
    return BADADDR
  start_ea = get_func_start_ea(item_ea)
  return original_func_ea + (item_ea - start_ea)

def get_unresolved_func_items(func_ea):
  start_ea = get_func_start_ea(func_ea)
  original_start_ea = get_original_func_ea(func_ea)
  if original_start_ea == BADADDR:
    return
  for item_ea in get_red_func_items(start_ea):
    yield item_ea, get_original_item_ea(item_ea)

def update_func_prefix(ea):
  stats = get_func_color_stats(ea)
  if get_func_name(ea).startswith('COPIED_') or get_func_name(ea).startswith('ONGOING_'):
    if stats['r'] == 0:
      return set_func_prefix_to_RESTORED(ea)
    if stats['o'] > 0:
      return set_func_prefix_to_ONGOING(ea)
  return False
